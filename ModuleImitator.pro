QT       += core gui serialport widgets multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

#QMAKE_CXXFLAGS_DEBUG -= -O1 -02 -O3 -Os
#QMAKE_CXXFLAGS_DEBUG += -O0
QMAKE_CXXFLAGS_MT_DBG += -pg
QMAKE_CXXFLAGS_DEBUG += -pg
QMAKE_LFLAGS_DEBUG += -pg

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $${_PRO_FILE_PWD_}/TableViews/
INCLUDEPATH += $${_PRO_FILE_PWD_}/TableViews/Modules/
INCLUDEPATH += $${_PRO_FILE_PWD_}/TableViews/Modules/M1/
INCLUDEPATH += $${_PRO_FILE_PWD_}/TableViews/Modules/M2/
INCLUDEPATH += $${_PRO_FILE_PWD_}/TableViews/objectModels/
INCLUDEPATH += $${_PRO_FILE_PWD_}/TableViews/SystemTabs/
INCLUDEPATH += $${_PRO_FILE_PWD_}/Moduls/
INCLUDEPATH += $${_PRO_FILE_PWD_}/Additional/
INCLUDEPATH += $${_PRO_FILE_PWD_}/IOfolder/IO/
INCLUDEPATH += $${_PRO_FILE_PWD_}/IOfolder/Contacts/
INCLUDEPATH += $${_PRO_FILE_PWD_}/IOfolder/IOWidgets/
INCLUDEPATH += $${_PRO_FILE_PWD_}/IOfolder/IOWidgets/DI/
INCLUDEPATH += $${_PRO_FILE_PWD_}/IOfolder/IOWidgets/DO/
INCLUDEPATH += $${_PRO_FILE_PWD_}/IOfolder/IOWidgets/AI/
INCLUDEPATH += $${_PRO_FILE_PWD_}/IOfolder/IOWidgets/AO/
INCLUDEPATH += $${_PRO_FILE_PWD_}/IOfolder/IOmini/

SOURCES += \
    Additional/ImageLabel.cpp \
    Additional/MemoryView.cpp \
    Additional/led.cpp \
    Additional/mProgressBar.cpp \
    Additional/static.cpp \
    IOfolder/Contacts/AIContact.cpp \
    IOfolder/Contacts/AOContact.cpp \
    IOfolder/Contacts/Contact.cpp \
    IOfolder/Contacts/DIContact.cpp \
    IOfolder/Contacts/DOContact.cpp \
    IOfolder/IO/AnalogInput.cpp \
    IOfolder/IO/AnalogOutput.cpp \
    IOfolder/IO/DiscreteInput.cpp \
    IOfolder/IO/DiscreteOutput.cpp \
    IOfolder/IO/IOClass.cpp \
    IOfolder/IO/IOConfigurator.cpp \
    IOfolder/IO/IOConfiguratorWindow.cpp \
    IOfolder/IOWidgets/AI/AINone.cpp \
    IOfolder/IOWidgets/AI/AIWLineEdit.cpp \
    IOfolder/IOWidgets/AI/AIWSlider.cpp \
    IOfolder/IOWidgets/AI/AIWidgets.cpp \
    IOfolder/IOWidgets/AO/AOWidgets.cpp \
    IOfolder/IOWidgets/DI/DINone.cpp \
    IOfolder/IOWidgets/DI/DIWButton.cpp \
    IOfolder/IOWidgets/DI/DIWidgets.cpp \
    IOfolder/IOWidgets/DO/DOWidgets.cpp \
    IOfolder/IOWidgets/IOWidget.cpp \
    IOfolder/IOmini/AImini.cpp \
    IOfolder/IOmini/DIMini.cpp \
    IOfolder/IOmini/IOmini.cpp \
    Port.cpp \
    TableViews/Imitation.cpp \
    TableViews/MI_TableView.cpp \
    TableViews/Modules/AddTabM.cpp \
    TableViews/Modules/Custom.cpp \
    TableViews/Modules/M1/AI_12.cpp \
    TableViews/Modules/M2/AIO_31A.cpp \
    TableViews/Modules/M2/AI_31A.cpp \
    TableViews/Modules/M2/AI_32A.cpp \
    TableViews/Modules/M2/AI_33A.cpp \
    TableViews/Modules/M1/AO_11.cpp \
    TableViews/Modules/M2/AO_31A.cpp \
    TableViews/Modules/M1/DIO_11.cpp \
    TableViews/Modules/M1/DI_11.cpp \
    TableViews/Modules/M2/DIA_31A.cpp \
    TableViews/Modules/M2/DIO_32A.cpp \
    TableViews/Modules/M2/DI_32A.cpp \
    TableViews/Modules/M2/DI_33A.cpp \
    TableViews/Modules/M1/DO_11.cpp \
    TableViews/Modules/M2/DO_31A.cpp \
    TableViews/Modules/ERU_ECP.cpp \
    TableViews/Modules/Module.cpp \
    TableViews/Modules/Modules.cpp \
    TableViews/objectModels/AddTabO.cpp \
    TableViews/objectModels/AnalogSetter.cpp \
    TableViews/objectModels/ArithmeticUniversal.cpp \
    TableViews/objectModels/Comparator.cpp \
    TableViews/objectModels/Delay_A.cpp \
    TableViews/objectModels/Delay_D.cpp \
    TableViews/objectModels/Gate.cpp \
    TableViews/objectModels/Limit.cpp \
    TableViews/objectModels/LogicalUniversal.cpp \
    TableViews/objectModels/objectModel.cpp \
    TableViews/objectModels/objects.cpp \
    TableViews/objectModels/Scale.cpp \
    TableViews/objectModels/Splitter_A.cpp \
    TableViews/objectModels/Splitter_D.cpp \
    TableViews/SystemTabs/SystemTabs.cpp \
    TableViews/SystemTabs/TabAllModules.cpp \
    TableViews/SystemTabs/TabCOM.cpp \
    TableViews/SystemTabs/WidgetsTab.cpp \
    main.cpp \
    mainwindow.cpp \
    options.cpp

HEADERS += \
    Additional/CircleImage.h \
    Additional/ImageLabel.h \
    Additional/MemoryView.h \
    Additional/led.h \
    Additional/mProgressBar.h \
    Additional/static.h \
    IOfolder/Contacts/AIContact.h \
    IOfolder/Contacts/AOContact.h \
    IOfolder/Contacts/Contact.h \
    IOfolder/Contacts/Contacts.h \
    IOfolder/Contacts/DIContact.h \
    IOfolder/Contacts/DOContact.h \
    IOfolder/IO/AnalogInput.h \
    IOfolder/IO/AnalogOutput.h \
    IOfolder/IO/DiscreteInput.h \
    IOfolder/IO/DiscreteOutput.h \
    IOfolder/IO/IOClass.h \
    IOfolder/IO/IOConfigurator.h \
    IOfolder/IO/IOConfiguratorWindow.h \
    IOfolder/IO/IOs.h \
    IOfolder/IOWidgets/AI/AINone.h \
    IOfolder/IOWidgets/AI/AIWLineEdit.h \
    IOfolder/IOWidgets/AI/AIWSlider.h \
    IOfolder/IOWidgets/AI/AIWidgets.h \
    IOfolder/IOWidgets/AO/AOWidgets.h \
    IOfolder/IOWidgets/DI/DINone.h \
    IOfolder/IOWidgets/DI/DIWButton.h \
    IOfolder/IOWidgets/DI/DIWidgets.h \
    IOfolder/IOWidgets/DO/DOWidgets.h \
    IOfolder/IOWidgets/IOWidget.h \
    IOfolder/IOWidgets/IOWidgets.h \
    IOfolder/IOmini/AImini.h \
    IOfolder/IOmini/DIMini.h \
    IOfolder/IOmini/IOmini.h \
    Port.h \
    TableViews/Imitation.h \
    TableViews/MI_TableView.h \
    TableViews/Modules/AddTabM.h \
    TableViews/Modules/Custom.h \
    TableViews/Modules/M1/AI_12.h \
    TableViews/Modules/M2/AIO_31A.h \
    TableViews/Modules/M2/AI_31A.h \
    TableViews/Modules/M2/AI_32A.h \
    TableViews/Modules/M2/AI_33A.h \
    TableViews/Modules/M1/AO_11.h \
    TableViews/Modules/M2/AO_31A.h \
    TableViews/Modules/M1/DIO_11.h \
    TableViews/Modules/M1/DI_11.h \
    TableViews/Modules/M2/DIA_31A.h \
    TableViews/Modules/M2/DIO_32A.h \
    TableViews/Modules/M2/DI_32A.h \
    TableViews/Modules/M2/DI_33A.h \
    TableViews/Modules/M1/DO_11.h \
    TableViews/Modules/M2/DO_31A.h \
    TableViews/Modules/ERU_ECP.h \
    TableViews/Modules/Module.h \
    TableViews/Modules/Modules.h \
    TableViews/objectModels/AddTabO.h \
    TableViews/objectModels/AnalogSetter.h \
    TableViews/objectModels/ArithmeticUniversal.h \
    TableViews/objectModels/Comparator.h \
    TableViews/objectModels/Delay_A.h \
    TableViews/objectModels/Delay_D.h \
    TableViews/objectModels/Gate.h \
    TableViews/objectModels/Limit.h \
    TableViews/objectModels/LogicalUniversal.h \
    TableViews/objectModels/ObjectModel.h \
    TableViews/objectModels/Objects.h \
    TableViews/objectModels/Scale.h \
    TableViews/objectModels/Splitter_A.h \
    TableViews/objectModels/Splitter_D.h \
    TableViews/SystemTabs/SystemTabs.h \
    TableViews/SystemTabs/TabAllModules.h \
    TableViews/SystemTabs/TabCOM.h \
    TableViews/SystemTabs/WidgetsTab.h \
    mainwindow.h \
    options.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=
