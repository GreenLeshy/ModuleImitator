#ifndef PORT_H
#define PORT_H

#include <QObject>
#include <QSerialPort>
#include <QTimer>
#include "MI_TableView.h"

#define SEPARATE_MAX_SIZE 50

class Module;
class Port : public QObject
{
    Q_OBJECT
public:
    explicit Port(QObject *parent = nullptr);
    bool isConnected() const;

    void setIsConnected(bool newIsConnected);

    bool autoRestart() const;
    void setAutoRestart(bool newAutoRestart);

    void init();
    bool open();

public slots:
    void connectToPort(QString const);          //Необходимо подключиться
    void disconnectFromPort();                      //Необходимо отключиться
    void newByte();                                          //Пришел новый байт
    void timeout();                                           //Истекло время ожидания следующего байта
    void write(QByteArray const &text);                             //Необходимо отправить сообщение
    void setBaudRate(QString  const text);
    void setDataBits(QString const  text);
    void setParity(QString  const text);
    void setStopBits(QString  const text);
    void setReadTimeout(int val);
    void setWriteTimeout(int val);
    void problem(void);                                     //Вызывается по таймеру, если не приходят сообщения (в многопоточном режиме порт иногда зависал)
    void errorSLOT(QSerialPort::SerialPortError error);

signals:
    void newMessage(QByteArray const &text, bool check);    //Пришло новое сообщение (связан с newMessageSLOT MainWindows)
    void connected(bool const);                                     //Соединение установленно успешно/неуспешно (связан с connectedSLOT MainWindows)
    void disconnected(void);                                           //Соединение разорвано (связан с disconnectedSLOT MainWindows)

private:

QSerialPort *m_pSerialPort;                                            //Ссылка на порт
bool m_isConnected;                                                     //Флаг коннекта
QTimer *m_timer;                                                               //Таймер, отсчитывающий таймаут между посылками при чтении
QTimer *m_problemTimer;                                                 //Таймер, перезапускающий порт при его зависании
int m_readTimeout=3;                                                    //Таймаут между посылками на чтение, мс
int m_writeTimeout=50;                                                  //Таймаут ожидания отправки ответа, мс
int m_problemTime=1000;                                             //Время в мс для problemTimer
bool m_autoRestart=true;                                                //Перезапускать ли порт при зависании
int m_baudRate=115200;
int m_dataBits=8;
int m_parity=3;
int m_stopBits=2;
int m_flowContrtol=0;
QString m_portName;
};

#endif // PORT_H
