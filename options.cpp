#include "options.h"
#include <QDialogButtonBox>
#include "mainwindow.h"

Options::Options(MainWindow *parent) : QDialog(parent)
{

    setWindowTitle("Параметры:");
    setWindowFlags(this->windowFlags()&~Qt::WindowContextHelpButtonHint);

    Main=parent;

    m_mainLayout = new QVBoxLayout;
    setLayout(m_mainLayout);

    QLabel *label;
    QHBoxLayout *L;

    L=new QHBoxLayout;
    label=new QLabel("Многопоточность: ", this);
    L->addWidget(label);
    m_multiThread=new QCheckBox(this);
    m_multiThread->setChecked(Main->multithread());
    L->addStretch(10);
    L->addWidget(m_multiThread);
    m_mainLayout->addLayout(L);

    L=new QHBoxLayout;
    label=new QLabel("Цикл вычислений, мс: ", this);
    L->addWidget(label);
    m_calcCycle=new QLineEdit(this);
    L->addStretch(10);
    L->addWidget(m_calcCycle);
    m_calcCycle->setText(QString::number(Main->calcCycle()));
    m_calcCycle->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    m_calcCycle->setMaximumWidth(40);
    m_mainLayout->addLayout(L);

    L=new QHBoxLayout;
    label=new QLabel("Использование звуков: ", this);
    L->addWidget(label);
    m_sounds=new QCheckBox(this);
    m_sounds->setChecked(Main->sounds());
    L->addStretch(10);
    L->addWidget(m_sounds);
    m_mainLayout->addLayout(L);

    L=new QHBoxLayout;
    label=new QLabel("Автоматический перезапуск порта при зависании: ", this);
    L->addWidget(label);
    m_portAutoRestart=new QCheckBox(this);
    m_portAutoRestart->setChecked(Main->autoRestart());
    L->addStretch(10);
    L->addWidget(m_portAutoRestart);
    m_mainLayout->addLayout(L);

    L=new QHBoxLayout;
    label=new QLabel("Ограничивать количество отображаемых сообщений: ", this);
    L->addWidget(label);
    m_nMessageIsLimited=new QCheckBox(this);
    m_nMessageIsLimited->setChecked(Main->nMessageLimited());
    L->addStretch(10);
    L->addWidget(m_nMessageIsLimited);
    m_mainLayout->addLayout(L);

    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::StandardButton::Ok | QDialogButtonBox::Cancel, this);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &Options::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &Options::reject);
    m_mainLayout->addWidget(buttonBox);
    setMinimumSize(m_mainLayout->minimumSize());
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

void Options::accept()
{
    Main->setMultithread(m_multiThread->isChecked());
    Main->setCalcCycle(m_calcCycle->text().toInt());
    Main->setSounds(m_sounds->isChecked());
    Main->setAutoRestart(m_portAutoRestart->isChecked());
    Main->setNMessageLimited(m_nMessageIsLimited->isChecked());
    QDialog::accept();
}

void Options::reject()
{

    QDialog::reject();
}

