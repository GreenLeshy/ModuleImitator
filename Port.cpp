#include "Port.h"
#include <QDebug>
#include <QThread>
#include "static.h"
#include "mainwindow.h"

Port::Port(QObject *parent) : QObject(parent)
{
    m_pSerialPort = new QSerialPort(this);
    //m_pSerialPort->setBaudRate(QSerialPort::Baud115200);
    //m_pSerialPort->setDataBits(QSerialPort::Data8);
    //m_pSerialPort->setParity(QSerialPort::OddParity);
    //m_pSerialPort->setStopBits(QSerialPort::TwoStop);
    //m_pSerialPort->setFlowControl(QSerialPort::NoFlowControl); //В Windows управление потоком не работает
    //m_pSerialPort->setFlowControl(QSerialPort::SoftwareControl);

    m_timer=new QTimer(this);
    m_problemTimer = new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &Port::timeout);
    connect(m_problemTimer, &QTimer::timeout, this, &Port::problem);
    connect(m_pSerialPort, &QSerialPort::readyRead, this, &Port::newByte);
    connect(m_pSerialPort, &QSerialPort::errorOccurred, this, &Port::errorSLOT);
}

bool Port::isConnected() const
{
    return m_isConnected;
}

void Port::setBaudRate(QString  const text)
{
    if(text=="Baud:1200")
        m_baudRate=int(QSerialPort::Baud1200);
    else if(text=="Baud:2400")
            m_baudRate=int(QSerialPort::Baud2400);
    else if(text=="Baud:4800")
            m_baudRate=int(QSerialPort::Baud4800);
    else if(text=="Baud:9600")
            m_baudRate=int(QSerialPort::Baud9600);
    else if(text=="Baud:19200")
            m_baudRate=int(QSerialPort::Baud19200);
    else if(text=="Baud:38400")
            m_baudRate=int(QSerialPort::Baud38400);
    else if(text=="Baud:57600")
            m_baudRate=int(QSerialPort::Baud57600);
    else if(text=="Baud:115200")
            m_baudRate=int(QSerialPort::Baud115200);

    m_problemTime=(930000/(Static::cut(text, ":", true).toInt())+22)*10;    //Примерно оптимальный интервал для разных скоростей
            //qDebug() << m_problemTime;
    init();
}

void Port::setDataBits(QString  const text)
{
    if(text=="5 data bits")
        m_dataBits=int(QSerialPort::Data5);
    else if(text=="6 data bits")
            m_dataBits=int(QSerialPort::Data6);
    else if(text=="7 data bits")
            m_dataBits=int(QSerialPort::Data7);
    else if(text=="8 data bits")
            m_dataBits=int(QSerialPort::Data8);
    init();
}

void Port::setParity(QString  const text)
{
    if(text=="NoParity")
        m_parity=int(QSerialPort::NoParity);
    else if(text=="EvenParity")
            m_parity=int(QSerialPort::EvenParity);
    else if(text=="OddParity")
            m_parity=int(QSerialPort::OddParity);
    else if(text=="SpaceParity")
            m_parity=int(QSerialPort::SpaceParity);
    else if(text=="MarkParity")
            m_parity=int(QSerialPort::MarkParity);
    init();
}

void Port::setStopBits(const QString text)
{
    if(text=="1 stop bit")
        m_stopBits=int(QSerialPort::OneStop);
    else if(text=="2 stop bits")
        m_stopBits=int(QSerialPort::TwoStop);
    init();
}

void Port::setReadTimeout(int val)
{
    m_readTimeout=val;
}

void Port::setWriteTimeout(int val)
{
    m_writeTimeout=val;
}

void Port::problem()
{
    if(m_autoRestart)
    {
        m_pSerialPort->close();
        open();
        //qDebug() << "Restarted";
        m_problemTimer->start(m_problemTime);
    }
}

void Port::errorSLOT(QSerialPort::SerialPortError error)
{
    //qDebug() << error;
    if(error == QSerialPort::SerialPortError::TimeoutError)
        problem();
}

bool Port::autoRestart() const
{
    return m_autoRestart;
}

void Port::setAutoRestart(bool newAutoRestart)
{
    m_autoRestart = newAutoRestart;
}

void Port::init()
{
    if(m_isConnected)   //Конфигурировать порт можно только после соединения
    {
        //m_pSerialPort->setPortName(m_portName);
        m_pSerialPort->setBaudRate(m_baudRate);
        m_pSerialPort->setDataBits((enum QSerialPort::DataBits)m_dataBits);
        m_pSerialPort->setParity((enum QSerialPort::Parity)m_parity);
        m_pSerialPort->setStopBits((enum QSerialPort::StopBits)m_stopBits);
        m_pSerialPort->setFlowControl(QSerialPort::NoFlowControl); //В Windows управление потоком не работает
    }
    else
    {
        //qDebug() << "Попытка конфигурирования закрытого порта";
    }
}

bool Port::open()
{
    m_pSerialPort->setPortName(m_portName);
    bool buf = m_pSerialPort->open(QSerialPort::ReadWrite);
    if(buf)
    {
        m_isConnected=true;
        m_timer->start(m_readTimeout);
    }
    emit connected(buf);
    init();
    return buf;
}

void Port::newByte()
{
    m_timer->start(m_readTimeout);//Перезапуск таймера
    m_problemTimer->start(m_problemTime);
}

void Port::timeout()
{
    QByteArray buf = m_pSerialPort->readAll();
    if(buf.size()>0)
    {
        if(Static::CRCRight(buf))
            emit newMessage(buf, 1);
        else
        {
            int size = buf.size();
            if(size<SEPARATE_MAX_SIZE)
            {
                QByteArray buf2;
                for(int i=0; i<size; ++i)
                {
                    buf2.append(buf.at(i));
                    if(Static::CRCRight(buf2))
                    {
                        emit newMessage(buf2, 1);
                        buf2.clear();
                    }
                }
                if(buf2.size()>0)
                    emit newMessage(buf2, 0);
              }
            else
                emit newMessage(buf, 0);
        }
    }
}

void Port::write(const QByteArray &text)
{
    m_timer->stop();
    m_problemTimer->stop();
    //m_pSerialPort->setRequestToSend(true); //В Windows управление потоком не работает
    /*//qDebug() << */m_pSerialPort->write(text);

    m_pSerialPort->waitForBytesWritten(m_writeTimeout);
    /*if(m_pSerialPort->waitForBytesWritten(m_writeTimeout))
        //qDebug() << "Запись идет";
    else
        //qDebug() << "Ошибка";*/

    //m_pSerialPort->setRequestToSend(false); //В Windows управление потоком не работает
    m_timer->start(m_readTimeout);
    m_problemTimer->start(m_problemTime);
}

void Port::connectToPort(QString name)
{
    m_portName=name;
    open();
}

void Port::disconnectFromPort()
{
    emit disconnected();
    m_isConnected=false;
    m_pSerialPort->close();
    m_timer->stop();
    m_problemTimer->stop();
}

