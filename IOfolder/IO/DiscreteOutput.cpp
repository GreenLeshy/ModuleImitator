#include "DiscreteOutput.h"
#include "Module.h"
#include "led.h"
#include "DOContact.h"
#include "mainwindow.h"
#include "DOWidgets.h"

DiscreteOutput::DiscreteOutput(QWidget *parent, QString name) : IOClass(parent)
{
    m_name = dynamic_cast<Imitation*>(parent)->name();
    m_name.prepend('<');
    m_name.append('>');
    m_name.append(":").append(name);
    m_contact = new DOContact(this, m_name);

    this->blockSignals(true);
    //m_contact->blockSignals(true);

    m_nameEdit->setText(m_name);
    m_led = new led(this, name);
    m_led->setMinimumSize(Main->elSize(MainWindow::s_ledD), 20);
    m_led->setMaximumSize(Main->elSize(MainWindow::s_ledD), 25);
    m_layout->insertWidget(0, m_led);
    m_layout->insertStretch(1, 1);
    m_widgets.append(DOWidgets::getTypeNames());


    connect(Main, &MainWindow::DIContactsUpdated, this, &IOClass::ContactsUpdated);
    //Main->contactChanged(m_contact);                     //Для обновления списка контактов при инициализации
    this->boundWidget(m_widgets.at(0));
    //m_contact->blockSignals(false);
    this->blockSignals(false);
}

QJsonObject DiscreteOutput::toJson() const
{
    QJsonObject jio=this->IOClass::toJson();
    jio["value"]=m_value;
    return jio;
}

void DiscreteOutput::fromJson(const QJsonObject jsn)
{
    setValue(jsn["value"].toBool());
    this->IOClass::fromJson(jsn);
}

void DiscreteOutput::boundWidget(const QString &conName)
{
    IOWidget *W = DOWidgets::newWidget(this, m_widgets.indexOf(conName));
    m_contact->boundWidget(conName);
    m_widget=W;
}

void DiscreteOutput::setValue(bool val)
{
    m_value=val;
    m_led->setValue(val);
    dynamic_cast<DOContact*>(m_contact)->setValue(val);
}
