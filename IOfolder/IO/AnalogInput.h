#ifndef ANALOGINPUT_H
#define ANALOGINPUT_H

#include "IOClass.h"

class mProgressBar;
class AnalogInput : public IOClass
{
public:
    explicit AnalogInput(QWidget *parent = nullptr, QString name="", double L=0, double H=20, double LE=4, double HE=20, bool isLimited=false);
    ~AnalogInput();

    inline double value() const;
    virtual QJsonObject toJson(void) const override;
    virtual void fromJson(const QJsonObject  jsn) override;
    virtual void boundWidget(const QString &conName) override; //Устанавливает имя виджета в соответ
    virtual void setRegular(bool newRegular) override;

    double lowerLimit() const;
    void setLowerLimit(double newLowerLimit);
    double upperLimit() const;
    void setUpperLimit(double newUpperLimit);
    double lErrorLimit() const;
    void setLErrorLimit(double newLErrorLimit);
    double hErrorLimit() const;
    void setHErrorLimit(double newHErrorLimit);
    void setLimits(double lowerLimit = 0, double upperLimit =20, double lErrorLimit =4, double hErrorLimit =20, bool isLimited=false);

public slots:
    //virtual void connectionChanged(QString);
    void newValue(double val);    //Реализация void newValue(bool/double) с типом int


protected:
    double m_value=0;
    mProgressBar *m_visualValue;
    double m_lowerLimit;
    double m_upperLimit;
    double m_lErrorLimit;
    double m_hErrorLimit;
    bool m_isLimited;
};

double AnalogInput::value() const
{
    return m_value;
}

#endif // ANALOGINPUT_H
