#include "IOConfiguratorWindow.h"
#include "IOConfigurator.h"
#include "QDialogButtonBox"

IOConfiguratorWindow::IOConfiguratorWindow(IOConfigurator *configurator, QWidget *parent) : QDialog(parent)
{
    m_configurator=configurator;
    //this->setWindowFlag(Qt::WindowType::Window);
    setWindowTitle("Параметры контакта:");
    setWindowFlags(this->windowFlags()&~Qt::WindowContextHelpButtonHint);
    m_IOType = new QComboBox(this);
    m_dataType = new QComboBox(this);
    m_bit = new QSpinBox(this);
    m_bitness = new QSpinBox(this);
    m_regAddr = new QSpinBox(this);

    m_IOType->addItems({"Input", "Output"});
    m_dataType->addItems({"Дискретный", "Аналоговый"});
    m_regAddr->setMinimum(INT16_MIN);
    m_regAddr->setMaximum(INT16_MAX);
    m_bit->setMinimum(0);
    m_bit->setMaximum(15);
    m_bitness->setMinimum(1);
    m_bitness->setMaximum(16);
    if(configurator->IOType()==IO_type::Input)
        m_IOType->setCurrentIndex(0);
    else
        m_IOType->setCurrentIndex(1);
    if(configurator->dataType()==data_type::Discrete)
        m_dataType->setCurrentIndex(0);
    else
        m_dataType->setCurrentIndex(1);
    m_regAddr->setValue(configurator->regAddr());
    m_bit->setValue(configurator->bit());
    m_bitness->setValue(configurator->bitness());


    m_mainLayout = new QVBoxLayout;
    setLayout(m_mainLayout);

    QLabel *label;
    QHBoxLayout *L;

    L=new QHBoxLayout;
    L->addWidget(m_IOType);
    L->addWidget(m_dataType);
    m_mainLayout->addLayout(L);

    L=new QHBoxLayout;
    label=new QLabel("Адрес: ", this);
    L->addWidget(label);
    L->addStretch(10);
    L->addWidget(m_regAddr);
    m_mainLayout->addLayout(L);

    L=new QHBoxLayout;
    m_bitLabel=new QLabel("Бит: ", this);
    L->addWidget(m_bitLabel);
    L->addStretch(10);
    L->addWidget(m_bit);
    m_mainLayout->addLayout(L);

    L=new QHBoxLayout;
    m_bitnessLabel=new QLabel("Разрядность: ", this);
    L->addWidget(m_bitnessLabel);
    L->addStretch(10);
    L->addWidget(m_bitness);
    m_mainLayout->addLayout(L);

   connect(m_dataType, &QComboBox::currentTextChanged, this, &IOConfiguratorWindow::rePaint);

    setMinimumSize(m_mainLayout->minimumSize());
    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::StandardButton::Ok | QDialogButtonBox::Cancel, this);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &IOConfiguratorWindow::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &IOConfiguratorWindow::reject);
    m_mainLayout->addWidget(buttonBox);
    rePaint();
}

void IOConfiguratorWindow::accept()
{
    m_configurator->setBit(m_bit->value());
    m_configurator->setBitness(m_bitness->value());
    m_configurator->setRegAddr(m_regAddr->value());
    if(m_IOType->currentIndex()==0)
        m_configurator->setIOType(IO_type::Input);
    else
        m_configurator->setIOType(IO_type::Output);
    if(m_dataType->currentIndex()==0)
        m_configurator->setDataType(data_type::Discrete);
    else
        m_configurator->setDataType(data_type::Analog);
    m_configurator->checkTypes();
    QDialog::accept();
}

void IOConfiguratorWindow::reject()
{
    QDialog::reject();
}

void IOConfiguratorWindow::rePaint(void)
{
    if(m_dataType->currentIndex()==0)
    {
        m_bit->show();
        m_bitLabel->show();
        m_bitness->hide();
        m_bitnessLabel->hide();
    }
    else
    {
        m_bit->hide();
        m_bitLabel->hide();
        m_bitness->show();
        m_bitnessLabel->show();
    }
}
