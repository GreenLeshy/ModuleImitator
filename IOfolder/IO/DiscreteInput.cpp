#include "DiscreteInput.h"
#include "Module.h"
#include "led.h"
#include "DIContact.h"
#include "mainwindow.h"
#include "DIWidgets.h"

DiscreteInput::DiscreteInput(QWidget *parent, QString name) : IOClass(parent)
{
    m_name = dynamic_cast<Imitation*>(parent)->name();
    m_name.prepend('<');
    m_name.append('>');
    m_name.append(":").append(name);
    m_contact = new DIContact(this, m_name);

    this->blockSignals(true);
    //m_contact->blockSignals(true);

    m_nameEdit->setText(m_name);
    m_led = new led(this, name);
    m_led->setMinimumSize(Main->sizeList().at(MainWindow::s_ledD), 20);
    m_led->setMaximumSize(Main->sizeList().at(MainWindow::s_ledD), 25);
    m_layout->insertWidget(0, m_led);
    m_layout->insertStretch(1, 1);
    m_widgets.append(DIWidgets::getTypeNames());

    connect(dynamic_cast<DIContact*>(m_contact), &DIContact::newValue, this, &DiscreteInput::newValue);

    connect(Main, &MainWindow::DOContactsUpdated, this, &IOClass::ContactsUpdated);
    //Main->contactChanged(m_contact);                     //Для обновления списка контактов при инициализации (для TabAllModules)
    this->boundWidget(m_widgets.at(0));
    //m_contact->blockSignals(false);
    this->blockSignals(false);
}

DiscreteInput::~DiscreteInput()
{
}

void DiscreteInput::newValue(bool val)
{
    m_led->setValue(val);
    dynamic_cast<DIContact*>(m_contact)->setValue(val); //Если значение изменилось виджетом
    m_value=val;
    emit inputChanged();
}

QJsonObject DiscreteInput::toJson() const
{
    QJsonObject jio=this->IOClass::toJson();
    jio["value"]=m_value;
    return jio;
}

void DiscreteInput::fromJson(const QJsonObject jsn)
{
    newValue(jsn["value"].toBool());
    this->IOClass::fromJson(jsn);
}

void DiscreteInput::setRegular(bool newSave)
{
    IOClass::setRegular(newSave);
    m_led->layout()->insertSpacing(1, 6);
    //m_led->layout()->insertStretch(1,100);
}

void DiscreteInput::boundWidget(const QString &conName)
{
    if(m_widget!=nullptr)
        delete m_widget;
    m_widget = DIWidgets::newWidget(this, m_widgets.indexOf(conName));
    m_contact->boundWidget(conName, 1);
}
