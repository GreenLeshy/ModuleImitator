#ifndef IOCLASS_H
#define IOCLASS_H

#include <QObject>
#include <QLineEdit>
#include <QComboBox>
#include <QLayout>
#include <QWidget>
#include "static.h"
#include "led.h"


class Contact;
class MainWindow;
class Imitation;
class IOWidget;
class IOClass : public QWidget              //Этот класс является родительским для элементов входа-выхода, которые обеспечивают передачу информации от модулей/объектов..
{                                                              //..к контактам и обратно, осуществляют отображение данных о контактах и предоставляют интерфейс для связывания контактов
    Q_OBJECT
public:
    explicit IOClass(QWidget *parent = nullptr);
    ~IOClass();

    data_type dataType() const;                     //Возвращает тип передаваемых данных
    IO_type IOType() const;                             //Возвращает тип контакта

    QHBoxLayout *layout() const;                            //Возвращает указатель на layout, на котором располагается вся визуальная часть IO
    QString name(void) const;                                  //Возвращает имя
    IOWidget * widget(void);

    virtual QJsonObject toJson(void) const;                       //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject jsn);             //Загружает информацию об элементе из QJsonObject

    virtual void boundWidget(const QString &conName)=0;//Привязать виджет
    void disboundWidget();                                      //Отвязать контакт

    Contact *contact() const;                                      //Возвращает указатель на контакт, принадлежащий этому элементу  
    QLineEdit *nameEdit() const;

    MainWindow *getMain() const;


    bool save() const;
    virtual void setRegular(bool newRegular);

public slots:
    //void outputChanged(bool/int);      !!!!-ИМЕЕТ РАЗНЫЕ ТИПЫ ПАРАМЕТРОВ ДЛЯ РАЗНЫХ ТИПОВ СИГНАЛОВ-!!!!   Необходимо изменить значение выходов
    virtual void connectionChanged(const QString &);       //Необходимо изменить подсоединенный контакт
    void nicknameChanged(const QString &);                     //Изменился псевдоним IO
    //void newValue(bool/int)       !!!!-ИМЕЕТ РАЗНЫЕ ТИПЫ ПАРАМЕТРОВ ДЛЯ РАЗНЫХ ТИПОВ СИГНАЛОВ-!!!! Входные значения изменились (связан с newValue контактов )
    void updateTag(const QString &);                                    //Изменилось имя родительского модуля/объекта
    void ContactsUpdated(const QStringList &list);             //Изменился список свободных кконтактов
    void updateName(QString name);                                  //Переименовать контакт

signals:
    void inputChanged(void);                                   //Изменилось входное значение

protected:
    QLineEdit *m_nameEdit;                              //QLineEdit для редактирования имени элемента
    QString m_name;                                         //Имя элемента
    Contact *m_contact;                                     //Указатель на контакт, принадлежащий этому элементу
    QComboBox *m_connectionChoise;              //Виджет выбора контактов для привязки
    QHBoxLayout *m_layout;                              //layout, на котором располагается вся визуальная часть IO
    IOWidget *m_widget=nullptr;                       //Ссылка на IOWidget
    Imitation *MyParent;                                    //Родительский модуль/объект
    MainWindow *Main;                                    //Главное окно
    QStringList m_widgets;                                 //Список виджетов для привязки
    bool m_regular=true;                                        //Этот IO сохраняется как IO, а не как-то иначе
    led *m_led;
};

#endif // IOCLASS_H
