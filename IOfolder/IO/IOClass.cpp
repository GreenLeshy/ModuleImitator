#include "IOClass.h"
#include "Imitation.h"
#include "Module.h"
#include "mainwindow.h"
#include "Contact.h"
#include "static.h"
#include "DIWidgets.h"

IOClass::IOClass(QWidget *parent) : QWidget(parent)
{

    MyParent = dynamic_cast<Imitation*>(parent);
    Main = MyParent->getMain();

    m_nameEdit = new QLineEdit(this);
    m_nameEdit->setStyleSheet("QLineEdit { background: rgb(250, 240, 220); selection-background-color: rgb(150, 140, 120); }");
    m_connectionChoise = new QComboBox(this);
    m_connectionChoise->setStyleSheet("QComboBox { background: rgb(240, 230, 210); selection-background-color: rgb(150, 140, 120); }");
    m_layout = new QHBoxLayout;
    m_layout->addWidget(this);      //Собственный Layout виджета нельзя использовать, а если виджет остается в нем, то появляется невидимый элемент, перекрывающий виджет выбора типа модуля/объекта

    m_nameEdit->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    m_nameEdit->setMinimumSize(Main->elSize(MainWindow::s_name), 20);
    m_nameEdit->setMaximumSize(Main->elSize(MainWindow::s_name), 25);
    m_connectionChoise->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    m_connectionChoise->setMinimumSize(Main->elSize(MainWindow::s_choise), 20);
    m_connectionChoise->setMaximumSize(Main->elSize(MainWindow::s_choise), 25);

    m_layout->addWidget(m_nameEdit);
    m_layout->addWidget(m_connectionChoise);
    m_layout->setSpacing(3);

    connect(m_nameEdit, &QLineEdit::textChanged, this, &IOClass::nicknameChanged);
    connect(m_connectionChoise, &QComboBox::currentTextChanged, this, &IOClass::connectionChanged);
    connect(this, &IOClass::inputChanged, MyParent, &Imitation::inputsChaneged);
    connect(MyParent, &Module::nameChanged, this, &IOClass::updateTag);
}

IOClass::~IOClass()
{
    if(m_widget!=nullptr)
        delete m_widget;
    delete  m_nameEdit;
    delete m_connectionChoise;
    delete m_led;
}

data_type IOClass::dataType() const
{
    return m_contact->dataType();
}

IO_type IOClass::IOType() const
{
    return m_contact->IOType();
}


void IOClass::connectionChanged(const QString &conName)
{
    //qDebug("ConnChanged");
    if(conName!="")
    {
        if(!m_widgets.contains(conName))
        {
            Contact *pair = Main->contact(conName);
            if(pair!=nullptr)
            {
                m_contact->bound(pair);
                this->boundWidget("NONE");
            }
        }
        else
        {
            m_contact->disbound();
            this->boundWidget(conName);
        }
    }
}

void IOClass::nicknameChanged(const QString &newName)
{
    //qDebug("nicknameChanged");
    m_name=newName;
    if(m_name.toLower()=="резерв")
        m_connectionChoise->setEnabled(false);
    else
        m_connectionChoise->setEnabled(true);
    m_contact->setName(newName);
}

void IOClass::updateTag(const QString &newTag)
{
     //qDebug() <<"IO parentCustomRenamed from "+m_name+" to "+Static::writeTag(m_nameEdit->text(), newTag, '<', '>', ':');
    QString buf;
    buf = Static::writeTag(m_nameEdit->text(), newTag, '<', '>', ':');
    m_name=buf;
    m_nameEdit->setText(buf);
}

void IOClass::ContactsUpdated(const QStringList &list)
{
    //qDebug("ContUpd");
    this->blockSignals(true);
    m_contact->blockSignals(true);
    m_connectionChoise->blockSignals(true);
    m_connectionChoise->clear();
    m_connectionChoise->addItems(m_widgets);
    m_connectionChoise->addItems(list);
    if(m_contact->pair()!=nullptr)
    {
        m_connectionChoise->insertItem(0, m_contact->pair()->name());
        m_connectionChoise->setCurrentIndex(0);
    }
    else
        m_connectionChoise->setCurrentText(m_contact->widgetType());
    this->blockSignals(false);
    m_contact->blockSignals(false);
    m_connectionChoise->blockSignals(false);
}

void IOClass::updateName(QString name)
{
    m_name=name;
    m_nameEdit->setText(name);
    m_nameEdit->textChanged(name);
    m_nameEdit->textEdited(name);
}

bool IOClass::save() const
{
    return m_regular;
}

void IOClass::setRegular(bool newRegular)
{
    m_regular = newRegular;
    if(!m_regular)
    {
        m_led->swap();
        m_led->setMaximumWidth(40);
        m_led->setMinimumWidth(40);
        int i=0;
        QLayoutItem *item= m_layout->itemAt(i);
        while(item!=0)
        {
            if(item->spacerItem())
                m_layout->removeItem(item);
            else
                ++i;
            item= m_layout->itemAt(i);
        }
    }
}

MainWindow *IOClass::getMain() const
{
    return Main;
}

QLineEdit *IOClass::nameEdit() const
{
    return m_nameEdit;
}

Contact *IOClass::contact() const
{
    return m_contact;
}

QHBoxLayout *IOClass::layout() const
{
    return m_layout;
}

QString IOClass::name() const
{
    return m_name;
}

IOWidget* IOClass::widget()
{
    return m_widget;
}

QJsonObject IOClass::toJson() const
{
    QJsonObject jio;
    jio["name"]=m_name;
    Contact *con=m_contact->pair();
    if(con==nullptr)
    {
        jio["pair"]=m_contact->widgetType();
        if(m_widget!=nullptr)
            jio["widget"]=m_widget->toJson();
    }
    else
        jio["pair"]=m_contact->pair()->name();
    return jio;
}

void IOClass::fromJson(const QJsonObject jsn)
{
    this->blockSignals(true);
    //m_contact->blockSignals(true);
    m_connectionChoise->blockSignals(true);
    m_name=jsn["name"].toString();
    m_nameEdit->setText(jsn["name"].toString());
    connectionChanged(jsn["pair"].toString());
    if(m_widget!=nullptr)
        m_widget->fromJson(jsn["widget"].toObject());
    this->blockSignals(false);
    //m_contact->blockSignals(false);
    m_connectionChoise->blockSignals(false);
}

void IOClass::disboundWidget()
{
    if(m_widget!=nullptr)
    {
        m_layout->removeWidget(m_widget);
        delete m_widget;
        m_widget=nullptr;
    }
}
