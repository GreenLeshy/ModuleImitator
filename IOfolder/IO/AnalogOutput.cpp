#include "AnalogOutput.h"
#include "Module.h"
#include "AOContact.h"
#include <QStyle>
#include <QStyleFactory>
#include "led.h"
#include "mainwindow.h"
#include "static.h"
#include "AOWidgets.h"
#include "mProgressBar.h"

AnalogOutput::AnalogOutput(QWidget *parent, QString name, double L, double H, double LE, double HE, bool isLimited) : IOClass(parent)
{

    m_name = dynamic_cast<Imitation*>(parent)->name();
    m_name.prepend('<');
    m_name.append('>');
    m_name.append(":").append(name);
    m_contact = new AOContact(this, m_name);
    m_nameEdit->setText(m_name);

    this->blockSignals(true);
    //m_contact->blockSignals(true);

    m_led = new led(this, name, 0);
    m_layout->insertWidget(0, m_led);
    m_led->setMinimumSize(Main->elSize(MainWindow::s_ledA), 20);
    m_led->setMaximumSize(Main->elSize(MainWindow::s_ledA), 25);

    m_lowerLimit=L;
    m_upperLimit=H;
    m_lErrorLimit=LE;
    m_hErrorLimit=HE;

    m_isLimited=isLimited;
    m_visualValue = new mProgressBar(this, L, H, LE, HE);
    m_visualValue->setMinimumSize(Main->elSize(MainWindow::s_visualA), 20);
    m_visualValue->setMaximumSize(Main->elSize(MainWindow::s_visualA)*2, 25);
    m_layout->insertWidget(1, m_visualValue);
    m_layout->insertStretch(1, 1);
    m_widgets.append(AOWidgets::getTypeNames());

    connect(Main, &MainWindow::AIContactsUpdated, this, &IOClass::ContactsUpdated);

    //Main->contactChanged(m_contact);                     //Для обновления списка контактов при инициализации
    this->boundWidget(m_widgets.at(0));
    //m_contact->blockSignals(false);
    this->blockSignals(false);
}

AnalogOutput::~AnalogOutput()
{
    delete m_visualValue;
}


QJsonObject AnalogOutput::toJson() const
{
    QJsonObject jio=this->IOClass::toJson();
    jio["value"]=m_value;
    return jio;
}

void AnalogOutput::fromJson(const QJsonObject jsn)
{
    setValue(jsn["value"].toDouble());
    this->IOClass::fromJson(jsn);
}

void AnalogOutput::boundWidget(const QString &conName)
{
    IOWidget *W = AOWidgets::newWidget(this, m_widgets.indexOf(conName));
    m_contact->boundWidget(conName);
    m_widget=W;
}

void AnalogOutput::setLimits(double lowerLimit, double upperLimit, double lErrorLimit, double hErrorLimit, bool isLimited)
{
    m_lowerLimit=lowerLimit;
    m_upperLimit=upperLimit;
    m_lErrorLimit=lErrorLimit;
    m_hErrorLimit=hErrorLimit;
    m_isLimited = isLimited;
    m_visualValue->setLimits(lowerLimit, upperLimit, lErrorLimit, hErrorLimit);
}

void AnalogOutput::setRegular(bool newRegular)
{
    IOClass::setRegular(newRegular);
    if(!m_regular)
    {
        m_led->setMaximumWidth(23);
        m_led->setMinimumWidth(22);
    }
}

void AnalogOutput::setValue(double val)
{
    if(m_isLimited)
    {
         val=Static::clamp(val, m_lowerLimit, m_upperLimit);
    }
    m_value=val;
    m_visualValue->setValue(m_value);

    dynamic_cast<AOContact*>(m_contact)->setValue(m_value);
}
