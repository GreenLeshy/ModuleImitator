#include "AnalogInput.h"
#include "Module.h"
#include "led.h"
#include "AIContact.h"
#include "mainwindow.h"
#include "static.h"
#include "AIWidgets.h"
#include "mProgressBar.h"

AnalogInput::AnalogInput(QWidget *parent, QString name, double L, double H, double LE, double HE, bool isLimited) : IOClass(parent)
{
    m_name = dynamic_cast<Imitation*>(parent)->name();
    m_name.prepend('<');
    m_name.append('>');
    m_name.append(":").append(name);
    m_contact = new AIContact(this, m_name);

    this->blockSignals(true);
    //m_contact->blockSignals(true);

    m_nameEdit->setText(m_name);
    m_visualValue = new mProgressBar(this);
    m_visualValue->setMinimumSize(Main->elSize(MainWindow::s_visualA), 20);
    m_visualValue->setMaximumSize(Main->elSize(MainWindow::s_visualA)*2, 25);
    m_layout->insertWidget(0, m_visualValue);
    m_visualValue->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    m_widgets.append(AIWidgets::getTypeNames());

    m_led = new led(this, name, 0);
    m_led->setMinimumSize(Main->elSize(MainWindow::s_ledA), 20);
    m_led->setMaximumSize(Main->elSize(MainWindow::s_ledA), 25);
    m_layout->insertWidget(0, m_led);
    m_layout->insertStretch(1, 1);

    m_lowerLimit=L;
    m_upperLimit=H;
    m_lErrorLimit=LE;
    m_hErrorLimit=HE;

    m_isLimited=isLimited;


    connect(dynamic_cast<AIContact*>(m_contact), &AIContact::newValue, this, &AnalogInput::newValue);
    connect(Main, &MainWindow::AOContactsUpdated, this, &IOClass::ContactsUpdated);
    //Main->contactChanged(m_contact);                     //Для обновления списка контактов при инициализации (для TabAllModules)
    this->boundWidget(m_widgets.at(0));
    //m_contact->blockSignals(false);
    this->blockSignals(false);
}

AnalogInput::~AnalogInput()
{
    delete m_visualValue;
}

void AnalogInput::newValue(double val)
{
    if(m_isLimited)
    {
        val=Static::clamp(val, m_lowerLimit, m_upperLimit);
    }
    m_value=val;

    dynamic_cast<AIContact*>(m_contact)->setValue(val); //Если значение изменилось виджетом
    m_visualValue->setValue(m_value);
    emit inputChanged();
}



QJsonObject AnalogInput::toJson() const
{
    QJsonObject jio=this->IOClass::toJson();
    jio["value"]=m_value;
    return jio;
}

void AnalogInput::fromJson(const QJsonObject jsn)
{
    newValue(jsn["value"].toDouble());
    this->IOClass::fromJson(jsn);
}

void AnalogInput::boundWidget(const QString &conName)
{
    if(m_widget!=nullptr)
        delete m_widget;
    m_widget = AIWidgets::newWidget(this, m_widgets.indexOf(conName));
    m_contact->boundWidget(conName, 1);
    if(m_widget->type()=="NONE" && !m_regular)
        m_widget->hide();
    else
        m_widget->show();
}

void AnalogInput::setRegular(bool newRegular)
{
    IOClass::setRegular(newRegular);
    if(!m_regular)
    {
        m_led->setMaximumWidth(23);
        m_led->setMinimumWidth(22);
        if(m_contact->pair()!=nullptr)
            m_widget->hide();
        repaint();
    }
}

double AnalogInput::lowerLimit() const
{
    return m_lowerLimit;
}

void AnalogInput::setLowerLimit(double newLowerLimit)
{
    m_lowerLimit = newLowerLimit;
    m_visualValue->setLowerLimit(newLowerLimit);
}

double AnalogInput::upperLimit() const
{
    return m_upperLimit;
}

void AnalogInput::setUpperLimit(double newUpperLimit)
{
    m_upperLimit = newUpperLimit;
    m_visualValue->setUpperLimit(newUpperLimit);
}

double AnalogInput::lErrorLimit() const
{
    return m_lErrorLimit;
}

void AnalogInput::setLErrorLimit(double newLErrorLimit)
{
    m_lErrorLimit = newLErrorLimit;
    m_visualValue->setLErrorLimit(newLErrorLimit);
}

double AnalogInput::hErrorLimit() const
{
    return m_hErrorLimit;
}

void AnalogInput::setHErrorLimit(double newHErrorLimit)
{
    m_hErrorLimit = newHErrorLimit;
    m_visualValue->setHErrorLimit(newHErrorLimit);
}

void AnalogInput::setLimits(double lowerLimit, double upperLimit, double lErrorLimit, double hErrorLimit, bool isLimited)
{
    m_lowerLimit=lowerLimit;
    m_upperLimit=upperLimit;
    m_lErrorLimit=lErrorLimit;
    m_hErrorLimit=hErrorLimit;
    m_isLimited = isLimited;
    m_visualValue->setLimits(lowerLimit, upperLimit, lErrorLimit, hErrorLimit);
}
