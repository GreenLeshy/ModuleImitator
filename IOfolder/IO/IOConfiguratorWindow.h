#ifndef IOCONFIGURATORWINDOW_H
#define IOCONFIGURATORWINDOW_H

#include <QDialog>
#include <QLayout>
#include <QComboBox>
//#include <QLineEdit>
#include <QSpinBox>
#include <QLabel>
#include "static.h"

class IOConfigurator;
class IOConfiguratorWindow : public QDialog
{
    Q_OBJECT
public:
    explicit IOConfiguratorWindow(IOConfigurator *configurator, QWidget *parent = nullptr); //Окно настройки контакта для Custom-модуля

public slots:
    void accept();
    void reject();
    void rePaint(void);         //Спрятать/показать виджеты, нужные для дискретных/аналоговых контактов

signals:


protected:
    QVBoxLayout *m_mainLayout;
    IOConfigurator *m_configurator=nullptr;
    QComboBox *m_IOType;
    QComboBox *m_dataType;
    QSpinBox *m_regAddr;
    QSpinBox *m_bit;
    QLabel *m_bitLabel;
    QSpinBox *m_bitness;
    QLabel *m_bitnessLabel;
};

#endif // IOCONFIGURATORWINDOW_H
