#ifndef IOCONFIGURATOR_H
#define IOCONFIGURATOR_H

#include <QObject>
#include "static.h"

class IOClass;
class AnalogInput;
class AnalogOutput;
class DiscreteInput;
class DiscreteOutput;
class Module;
class MainWindow;
class IOConfigurator : public QObject   //Класс, позволяющий создавать и настраивать контакты для custom модулей
{
    Q_OBJECT
public:
    explicit IOConfigurator(QObject *parent);
    ~IOConfigurator();

    IO_type IOType() const;
    void setIOType(IO_type newIOType);

    data_type dataType() const;
    void setDataType(data_type newDataType);
    IOClass * createIO(void);

    int bit() const;
    void setBit(int newBit);

    int regAddr() const;
    void setRegAddr(int newRegAddr);

    int bitness() const;
    void setBitness(int newBitness);

    IOClass *IO() const;

    QJsonObject toJson(void);                       //Возвращает информацию об элементе в виде QJsonObject
    void fromJson(const QJsonObject jsn);             //Загружает информацию об элементе из QJsonObject

public slots:
    void regChaneged(int regAddr);
    void analogInputChaneged(double val);
    void discreteInputChaneged(bool val);
    void checkTypes(void);

signals:
    void IOCreated(IOClass *IO);

protected:


protected:
    Module *m_myModule;
    MainWindow *m_Main;
    IO_type m_IOType = IO_type::Input;
    IO_type m_IOTypeMem;
    data_type m_dataType = data_type::Discrete;
    data_type m_dataTypeMem;
    int m_regAddr=-1;
    int m_bit=0;
    int m_bitness=16;
    int m_bitnessMem=16;                                //Запомненное значение для обновления значения при смене разрядности
    IOClass *m_IO=nullptr;
    AnalogInput *m_analogInput=nullptr;         //Указатель под каждый тип для оптимизации, так как иначе часто будет необходимо приведение типов dynamic cast-ом
    AnalogOutput *m_analogOutput=nullptr;
    DiscreteInput *m_discreteInput=nullptr;
    DiscreteOutput *m_discreteOutput=nullptr;
};

#endif // IOCONFIGURATOR_H
