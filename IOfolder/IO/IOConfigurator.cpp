#include "IOConfigurator.h"
#include "IOs.h"
#include "Contacts.h"
#include "Module.h"
#include "mainwindow.h"

IOConfigurator::IOConfigurator(QObject *parent) : QObject(parent)
{
    m_myModule = dynamic_cast<Module*>(parent);
    m_Main = m_myModule->getMain();
    connect(m_myModule, &Module::registerChanged, this, &IOConfigurator::regChaneged);
}

IOConfigurator::~IOConfigurator()
{
    m_myModule->removeIO(m_IO);
    //m_IO->deleteLater();
    delete m_IO;
}

IO_type IOConfigurator::IOType() const
{
    return m_IOType;
}

void IOConfigurator::setIOType(IO_type newIOType)
{
    m_IOType = newIOType;
}

data_type IOConfigurator::dataType() const
{
    return m_dataType;
}

void IOConfigurator::setDataType(data_type newDataType)
{
    m_dataType = newDataType;
}

void IOConfigurator::regChaneged(int regAddr)
{
    if(regAddr==m_regAddr)
    {
        int buf = m_myModule->getRegister(m_regAddr);
        if(m_IOType==IO_type::Output)
        {
            if(m_dataType==data_type::Discrete)
                m_discreteOutput->setValue(buf & (1<<m_bit));
            else
                m_analogOutput->setValue(Static::scaleToDouble(buf, 0, (1<<(m_bitness))-1, 0, 20, true));
        }
    }
}

void IOConfigurator::analogInputChaneged(double val)
{
    m_myModule->setRegister(m_regAddr, Static::scaleToInt(val, 0, 20, 0, (1<<(m_bitness))-1, true));
}

void IOConfigurator::discreteInputChaneged(bool val)
{
    int buf = m_myModule->getRegister(m_regAddr);
    if(val)
        buf=buf | (1<<m_bit);
    else
        buf=buf & ~(1<<m_bit);
    m_myModule->setRegister(m_regAddr, buf);
}

void IOConfigurator::checkTypes()
{
    if(m_dataTypeMem!=m_dataType || m_IOTypeMem!=m_IOType)
        createIO();
    else
        if((m_bitness!=m_bitnessMem)&&(m_dataType==data_type::Analog)&&(m_IOType==IO_type::Input)) //Обновляем записанное значение в случае изменения разрядности
        {
            analogInputChaneged(Static::scaleToDouble(m_myModule->getRegister(m_regAddr), 0, (1<<(m_bitnessMem))-1, 0, 20, true));
            m_bitnessMem=m_bitness;
        }
}

IOClass *IOConfigurator::IO() const
{
    return m_IO;
}

QJsonObject IOConfigurator::toJson()
{
    QJsonObject IOC;
    IOC["IO_type"] = int(IOType());
    IOC["data_type"] = int(dataType());
    IOC["addr"] = regAddr();
    IOC["bit"] = bit();
    IOC["bitness"] = bitness();
    IOC.insert("contact", IO()->toJson());
    return IOC;
}

void IOConfigurator::fromJson(const QJsonObject jsn)
{
    m_IOType=IO_type(jsn["IO_type"].toInt());
    m_dataType=data_type(jsn["data_type"].toInt());
    m_regAddr=jsn["addr"].toInt();
    m_bit=jsn["bit"].toInt();
    m_bitness=jsn["bitness"].toInt();
    checkTypes();
    m_IO->fromJson(jsn["contact"].toObject());
}

int IOConfigurator::bitness() const
{
    return m_bitness;
}

void IOConfigurator::setBitness(int newBitness)
{
    m_bitness = newBitness;
}

int IOConfigurator::regAddr() const
{
    return m_regAddr;
}

void IOConfigurator::setRegAddr(int newRegAddr)
{
    m_regAddr = newRegAddr;
}

int IOConfigurator::bit() const
{
    return m_bit;
}

void IOConfigurator::setBit(int newBit)
{
    m_bit = newBit;
}

IOClass *IOConfigurator::createIO()
{
    if(m_IO!=nullptr)
    {
        m_myModule->removeIO(m_IO);
        m_IO->deleteLater();
    }
    if(m_dataType==data_type::Discrete)
    {
        if(m_IOType==IO_type::Input)
        {
            m_discreteInput = new DiscreteInput(m_myModule, "DI");
            m_IO=m_discreteInput;
            DIContact *cont = dynamic_cast<DIContact*>(m_discreteInput->contact());
            connect(cont, &DIContact::newValue, this, &IOConfigurator::discreteInputChaneged);
        }
        else
        {
            m_discreteOutput = new DiscreteOutput(m_myModule, "DO");
            m_IO=m_discreteOutput;
        }
    }
    else
    {
        if(m_IOType==IO_type::Input)
        {
            m_analogInput = new AnalogInput(m_myModule, "AI");
            m_IO=m_analogInput;
            AIContact *cont = dynamic_cast<AIContact*>(m_analogInput->contact());
            connect(cont, &AIContact::newValue, this, &IOConfigurator::analogInputChaneged);
        }
        else
        {
            m_analogOutput = new AnalogOutput(m_myModule, "AO");
            m_IO=m_analogOutput;
        }
    }
    m_IO->setRegular(false);
    emit IOCreated(m_IO);
    m_dataTypeMem=m_dataType;
    m_IOTypeMem=m_IOType;
    regChaneged(m_regAddr);
    return m_IO;
}
