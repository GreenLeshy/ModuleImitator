#ifndef DISCRETEOUTPUT_H
#define DISCRETEOUTPUT_H

#include "IOClass.h"

class DiscreteOutput : public IOClass
{
public:
    explicit DiscreteOutput(QWidget *parent = nullptr, QString name="");
    virtual QJsonObject toJson(void) const override;
    virtual void fromJson(const QJsonObject  jsn) override;

    virtual void boundWidget(const QString &conName) override;
    inline bool value() const;

public slots:
    void setValue(bool);                    //РќРµРѕР±С…РѕРґРёРјРѕ РёР·РјРµРЅРёС‚СЊ Р·РЅР°С‡РµРЅРёРµ РІС‹С…РѕРґРѕРІ
    //virtual void connectionChanged(QString);       //РќРµРѕР±С…РѕРґРёРјРѕ РёР·РјРµРЅРёС‚СЊ РїРѕРґСЃРѕРµРґРёРЅРµРЅРЅС‹Р№ РєРѕРЅС‚Р°РєС‚

protected:
    bool m_value=false;
};

bool DiscreteOutput::value() const
{
    return m_value;
}
#endif // DISCRETEOUTPUT_H
