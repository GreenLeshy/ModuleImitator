#ifndef DISCRETEINPUT_H
#define DISCRETEINPUT_H

#include "IOClass.h"
#include <QCheckBox>

class DiscreteInput : public IOClass
{
public:
    explicit DiscreteInput(QWidget *parent = nullptr, QString name="");
    ~DiscreteInput();

    inline bool value() const;
    virtual QJsonObject toJson(void) const override;
    virtual void fromJson(const QJsonObject  jsn) override;
    virtual void setRegular(bool newSave) override;

    virtual void boundWidget(const QString &conName) override;

public slots:
    //virtual void connectionChanged(QString);
    void newValue(bool val);    //Р РµР°Р»РёР·Р°С†РёСЏ void newValue(bool/double) СЃ С‚РёРїРѕРј bool

protected:
    bool m_value=false;
};

bool DiscreteInput::value() const
{
    return m_value;
}

#endif // DISCRETEINPUT_H
