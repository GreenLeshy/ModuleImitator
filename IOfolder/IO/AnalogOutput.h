#ifndef ANALOGOUTPUT_H
#define ANALOGOUTPUT_H

#include "IOClass.h"
#include <QPushButton>

class mProgressBar;
class AnalogOutput : public IOClass
{
public:
    explicit AnalogOutput(QWidget *parent = nullptr, QString name="", double L=0, double H=20, double LE=4, double HE=20, bool isLimited=false);
    ~AnalogOutput();

    inline double value() const;
    virtual QJsonObject toJson(void) const override;
    virtual void fromJson(const QJsonObject  jsn) override;
    virtual void boundWidget(const QString &conName) override;
    void setLimits(double lowerLimit = 0, double upperLimit =20, double lErrorLimit =4, double hErrorLimit =20, bool isLimited=false);
    virtual void setRegular(bool newRegular) override;

public slots:
    void setValue(double);                    //Необходимо изменить значение выходов
    //virtual void connectionChanged(QString);       //Необходимо изменить подсоединенный контакт


protected:
    double m_value=0;
    mProgressBar *m_visualValue;
    double m_lowerLimit;
    double m_upperLimit;
    double m_lErrorLimit;
    double m_hErrorLimit;
    bool m_isLimited;
    QString c_green="QProgressBar {text-align: center; border: 1px solid green} QProgressBar::chunk{background-color: #20DD30}";
    QString c_red="QProgressBar {text-align: center; border: 1px solid red} QProgressBar::chunk{background-color: #FF0000}";


};

double AnalogOutput::value() const
{
    return m_value;
}
#endif // ANALOGOUTPUT_H
