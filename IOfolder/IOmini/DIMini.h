#ifndef DIMINI_H
#define DIMINI_H

#include "IOmini.h"

class led;
class DIMini : public IOmini
{
    Q_OBJECT
public:
    DIMini(IOClass *IO);
    ~DIMini();

protected:
    led *m_led;
    bool m_value;
    virtual void valueChanegedTHIS() override;
    virtual void valueChanegedPARENT() override;
};

#endif // DIMINI_H
