#include "AImini.h"
#include "IOs.h"
#include "AIWidgets.h"
#include "mProgressBar.h"
#include "mainwindow.h"

AIMini::AIMini(IOClass *IO) : IOmini(IO)
{
    p_widget = m_IO->widget()->widget();

    if(m_type==AIWidgets::getTypeName(AIWidgets::LineEdit))
    {
        m_widget=new QLineEdit(this);
        connect(dynamic_cast<QLineEdit *>(m_widget), &QLineEdit::editingFinished, this, &AIMini::valueChanegedTHIS);
        connect(dynamic_cast<QLineEdit *>(p_widget), &QLineEdit::editingFinished, this, &AIMini::valueChanegedPARENT);
    }
    else if(m_type==AIWidgets::getTypeName(AIWidgets::Slider))
    {
        m_widget=new QSlider(Qt::Horizontal, this);
        connect(dynamic_cast<QSlider *>(m_widget), &QSlider::valueChanged, this, &AIMini::valueChanegedTHIS);
        connect(dynamic_cast<QSlider *>(p_widget), &QSlider::valueChanged, this, &AIMini::valueChanegedPARENT);
    }
    m_visualValue=new mProgressBar(this);
    m_layout->insertWidget(0, m_widget);
    m_layout->insertWidget(0, m_visualValue);
    m_visualValue->setMinimumWidth(Main->elSize(MainWindow::s_TAM_left));
    m_visualValue->setMaximumWidth(Main->elSize(MainWindow::s_TAM_left));
    m_widget->setMinimumWidth(Main->elSize(MainWindow::s_TAM_midle));
    m_widget->setMaximumWidth(Main->elSize(MainWindow::s_TAM_midle));
    m_widget->setStyleSheet("QLineEdit { background: rgb(180, 240, 255); selection-background-color: rgb(60, 130, 155); }");
    valueChanegedPARENT();
}

AIMini::~AIMini()
{
    delete m_widget;
    delete m_visualValue;
}

void AIMini::valueChanegedTHIS()
{
    if(m_type==AIWidgets::getTypeName(AIWidgets::LineEdit))
    {
        disconnect(dynamic_cast<QLineEdit *>(p_widget), &QLineEdit::textChanged, this, &AIMini::valueChanegedPARENT);
        dynamic_cast<QLineEdit *>(p_widget)->setText( dynamic_cast<QLineEdit *>(m_widget)->text());
        dynamic_cast<QLineEdit *>(p_widget)->editingFinished();
        m_visualValue->setValue(dynamic_cast<QLineEdit *>(m_widget)->text().toDouble());
        connect(dynamic_cast<QLineEdit *>(p_widget), &QLineEdit::textChanged, this, &AIMini::valueChanegedPARENT);
    }
    else if(m_type==AIWidgets::getTypeName(AIWidgets::Slider))
    {
        disconnect(dynamic_cast<QSlider *>(p_widget), &QSlider::sliderMoved, this, &AIMini::valueChanegedPARENT);
        int pos = dynamic_cast<QSlider *>(m_widget)->value();
        dynamic_cast<QSlider *>(p_widget)->setValue(pos);
        double buf = pos*(20.0/ dynamic_cast<QSlider *>(m_widget)->maximum());
        m_visualValue->setValue(buf);
        connect(dynamic_cast<QSlider *>(p_widget), &QSlider::sliderMoved, this, &AIMini::valueChanegedPARENT);
    }
}

void AIMini::valueChanegedPARENT()
{
    if(m_type==AIWidgets::getTypeName(AIWidgets::LineEdit))
    {
        disconnect(dynamic_cast<QLineEdit *>(m_widget), &QLineEdit::textChanged, this, &AIMini::valueChanegedTHIS);
        dynamic_cast<QLineEdit *>(m_widget)->setText( dynamic_cast<QLineEdit *>(p_widget)->text());
        m_visualValue->setValue(dynamic_cast<QLineEdit *>(m_widget)->text().toDouble());
        connect(dynamic_cast<QLineEdit *>(m_widget), &QLineEdit::textChanged, this, &AIMini::valueChanegedTHIS);
    }
    else if(m_type==AIWidgets::getTypeName(AIWidgets::Slider))
    {
        disconnect(dynamic_cast<QSlider *>(m_widget), &QSlider::sliderMoved, this, &AIMini::valueChanegedTHIS);
        int pos = dynamic_cast<QSlider *>(p_widget)->value();
        dynamic_cast<QSlider *>(m_widget)->setValue(pos);
        double buf = pos*(20.0/ dynamic_cast<QSlider *>(m_widget)->maximum());
        m_visualValue->setValue(buf);
        connect(dynamic_cast<QSlider *>(m_widget), &QSlider::sliderMoved, this, &AIMini::valueChanegedTHIS);
    }
}
