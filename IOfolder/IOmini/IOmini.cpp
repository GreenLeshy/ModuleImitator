#include "IOmini.h"
#include "IOs.h"
#include "Contacts.h"
#include "mainwindow.h"
#include "SystemTabs.h"

IOmini::IOmini(IOClass *IO) : QWidget()
{
    m_IO=IO;
    Main = IO->contact()->getMain();
    MyTab = Main->WidgetsTab();
    m_name = IO->name();
    m_nameEdit = new QLineEdit(m_name, this);
    p_nameEdit = IO->nameEdit();
    m_type = IO->contact()->widgetType();
    m_layout = new QHBoxLayout();
    m_nameEdit->setStyleSheet("QLineEdit { background: rgb(250, 240, 220); selection-background-color: rgb(150, 140, 120); }");
    //connect(m_nameEdit, &QLineEdit::textChanged, this, &IOmini::nameChanegedTHIS);
    connect(m_nameEdit, &QLineEdit::editingFinished, this, &IOmini::nameChanegedTHIS);
    //connect(p_nameEdit, &QLineEdit::textChanged, this, &IOmini::nameChanegedPARENT);
    m_layout->addWidget(m_nameEdit);
    m_nameEdit->setMinimumWidth(Main->elSize(MainWindow::s_TAM_right));
    m_nameEdit->setMaximumWidth(Main->elSize(MainWindow::s_TAM_right)*4);
}

IOmini::~IOmini()
{
    m_layout->deleteLater();
    m_nameEdit->deleteLater(); //Почему-то сам не удаляется
}

QString IOmini::name() const
{
    return m_name;
}

QString IOmini::type() const
{
    return m_type;
}

QLayout* IOmini::getLayout(void) const
{
    return m_layout;
}

void IOmini::nameChanegedTHIS(void)
{
    //disconnect(m_nameEdit, &QLineEdit::textChanged, this, &IOmini::nameChanegedTHIS);
    m_name=m_nameEdit->text();
    p_nameEdit->setText(m_name);
    //connect(m_nameEdit, &QLineEdit::textChanged, this, &IOmini::nameChanegedTHIS);
}

/*void IOmini::nameChanegedPARENT()
{
    disconnect(m_nameEdit, &QLineEdit::textChanged, this, &IOmini::nameChanegedTHIS);
    m_name=p_nameEdit->text();
    m_nameEdit->setText(m_name);
    connect(m_nameEdit, &QLineEdit::textChanged, this, &IOmini::nameChanegedTHIS);
}
*/
