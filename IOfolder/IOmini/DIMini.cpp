#include "DIMini.h"
#include "IOs.h"
#include "DIWidgets.h"
#include "led.h"
#include "mainwindow.h"

DIMini::DIMini(IOClass *IO) : IOmini(IO)
{
    p_widget = m_IO->widget()->widget();

    if(m_type==DIWidgets::getTypeName(DIWidgets::Button))
    {
        m_widget=new QPushButton(this);
        dynamic_cast<QPushButton *>(m_widget)->setText("1/0");
        connect(dynamic_cast<QPushButton *>(m_widget), &QPushButton::clicked, this, &DIMini::valueChanegedTHIS);
        connect(dynamic_cast<QPushButton *>(p_widget), &QPushButton::clicked, this, &DIMini::valueChanegedPARENT);
    }
    m_led=new led(this);
    //m_widget->setMinimumSize(100, 20);
    m_widget->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    m_layout->insertWidget(0, m_widget);
    m_layout->insertWidget(0, m_led);
    m_led->setMinimumWidth(Main->elSize(MainWindow::s_TAM_left));
    m_led->setMaximumWidth(Main->elSize(MainWindow::s_TAM_left));
    m_widget->setMinimumWidth(Main->elSize(MainWindow::s_TAM_midle));
    m_widget->setMaximumWidth(Main->elSize(MainWindow::s_TAM_midle));
    m_widget->setStyleSheet("QPushButton { background: rgb(150, 210, 225); selection-background-color: rgb(60, 130, 155); }");
    valueChanegedPARENT();
}

DIMini::~DIMini()
{
    delete m_led;
    delete m_widget;
}

void DIMini::valueChanegedTHIS()
{
    if(m_type==DIWidgets::getTypeName(DIWidgets::Button))
    {
        dynamic_cast<QPushButton *>(p_widget)->click();
    }
}

void DIMini::valueChanegedPARENT()
{
    if(m_type==DIWidgets::getTypeName(DIWidgets::Button))
    {
        disconnect(dynamic_cast<QPushButton *>(m_widget), &QPushButton::clicked, this, &DIMini::valueChanegedTHIS);
        m_value=dynamic_cast<DiscreteInput *>(m_IO)->value();
        dynamic_cast<QPushButton *>(m_widget)->setChecked(m_value);
        m_led->setValue(m_value);
        connect(dynamic_cast<QPushButton *>(m_widget), &QPushButton::clicked, this, &DIMini::valueChanegedTHIS);
    }
}
