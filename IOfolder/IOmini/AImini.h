#ifndef AIMINI_H
#define AIMINI_H

#include "IOmini.h"

class mProgressBar;
class AIMini : public IOmini
{
    Q_OBJECT
public:
    AIMini(IOClass *IO);
    ~AIMini();

protected:
    mProgressBar *m_visualValue;
    virtual void valueChanegedTHIS() override;
    virtual void valueChanegedPARENT() override;
};

#endif // AIMINI_H
