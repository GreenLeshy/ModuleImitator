#ifndef IOMINI_H
#define IOMINI_H

#include <QWidget>
#include <QLineEdit>
#include <QLayout>

class IOClass;
class MainWindow;
class WidgetsTab;
class IOmini : public QWidget               //Класс для отображения входов/выходов на специальной системной странице
{
    Q_OBJECT
public:
    explicit IOmini(IOClass *IO);                           //IO mini создается по IO, выступающем в роли прообраза и привязывается к нему
    ~IOmini();
    QString name() const;
    QString type() const;
    QLayout* getLayout(void) const;
    virtual void valueChanegedTHIS(void)=0;         //Пользователь изменил значение через виджет IOmini
    virtual void valueChanegedPARENT(void)=0;   //Пользователь изменил значение через виджет IO-прообраза
    virtual void nameChanegedTHIS(void);            //Пользователь изменил имя через виджет IOmini
    //virtual void nameChanegedPARENT(void);      //Пользователь изменил имя через виджет IO-прообраза

signals:

protected:
    MainWindow *Main;               //Указатель на главное окно
    WidgetsTab *MyTab;                //Указатель на вкладку с IOmini
    QString m_name;                     //Имя
    QLineEdit *m_nameEdit;          //Виджет для редактирования имени в IOmini
    QLineEdit *p_nameEdit;           //Указатель на виджет для редактирования имени в IO-прообразе
    QWidget *m_widget;                //Виджет для изменения значения в IOmini
    QWidget *p_widget;                 //Указатель на виджет для изменения значения в IO-прообразе
    QString m_type;                       //Тип виджета
    QHBoxLayout *m_layout;         //Layout, на котором размещается IOmini
    IOClass *m_IO;                        //Ссылка на IO-прообраз
};

#endif // IOMINI_H
