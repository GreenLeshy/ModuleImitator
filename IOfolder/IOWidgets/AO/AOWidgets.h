#ifndef AOWIDGETS_H
#define AOWIDGETS_H
#include "IOWidget.h"

class AOWidgets             //Объединяет AOWidget в одну библиотеку и реализует фабрику объектов
{
public:
    AOWidgets();

    enum AOW_types: unsigned        //Обязательно должен быть согласован по количеству и порядку элементов с typeNames
    {
        NONE=0,
    };

    static QStringList getTypeNames();
    static QString getTypeName(int i);
    static IOWidget *newWidget(QWidget *parent=nullptr, int type=0, QString name="");

private:
    static const QStringList typeNames;
};

#endif // AOWIDGETS_H
