#include "AIWLineEdit.h"
#include "mainwindow.h"
#include "IOs.h"
#include "AIWidgets.h"

AIWLineEdit::AIWLineEdit(QWidget *parent) : IOWidget(parent)
{
    m_widget=new QLineEdit("0", this);
    int s = Main->elSize(MainWindow::s_AIWidget);
    m_widget->setMinimumWidth(s);
    m_widget->setMaximumWidth(s*2);
    m_widget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    m_widget->setText(QString::number(dynamic_cast<AnalogInput*>(m_IO)->value()));
    m_widget->setStyleSheet("QLineEdit { background: rgb(180, 240, 255); selection-background-color: rgb(60, 130, 155); }");
    connect(m_widget, &QLineEdit::editingFinished, this, &AIWLineEdit::valueChangedSLOT);
    connect(this, &AIWLineEdit::valueChaneged, dynamic_cast<AnalogInput*>(m_IO), &AnalogInput::newValue);
    m_IO->layout()->insertWidget(1, m_widget);
    m_type = AIWidgets::getTypeName(0);
}

QJsonObject AIWLineEdit::toJson()
{
    QJsonObject jio=this->IOWidget::toJson();
    jio["value"]=m_widget->text();
    return jio;
}

void AIWLineEdit::fromJson(const QJsonObject jsn)
{
    this->IOWidget::fromJson(jsn);
    QString num =jsn["value"].toString();
    m_widget->setText(num);
    valueChangedSLOT();
}

QWidget *AIWLineEdit::widget()
{
    return m_widget;
}

AIWLineEdit::~AIWLineEdit()
{
    delete m_widget;
}

void AIWLineEdit::valueChangedSLOT()
{
    double buf = m_widget->text().toDouble();
    emit valueChaneged(buf);
}
