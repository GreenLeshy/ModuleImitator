#include "AIWSlider.h"
#include "mainwindow.h"
#include "IOs.h"
#include "static.h"
#include "AIWidgets.h"

AIWSlider::AIWSlider(QWidget *parent) : IOWidget(parent)
{
    m_widget=new QSlider(Qt::Horizontal, this);
    m_widget->setMinimum(0);
    m_widget->setMaximum(100);
    int s = Main->elSize(MainWindow::s_AIWidget);
    m_widget->setMinimumWidth(s);
    m_widget->setMaximumWidth(s*2);
    m_widget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    double buf = dynamic_cast<AnalogInput*>(m_IO)->value();
    int pos = buf*5;
    m_widget->setValue(pos);
    connect(m_widget, &QSlider::valueChanged, this, &AIWSlider::valueChangedSLOT);
    connect(this, &AIWSlider::valueChaneged, dynamic_cast<AnalogInput*>(m_IO), &AnalogInput::newValue);
    m_IO->layout()->insertWidget(1, m_widget);
    m_type = AIWidgets::getTypeName(1);
}

QJsonObject AIWSlider::toJson()
{
    QJsonObject jio=this->IOWidget::toJson();
    jio["value"]=m_widget->value();
    return jio;
}

void AIWSlider::fromJson(const QJsonObject jsn)
{
    this->IOWidget::fromJson(jsn);
    int num =jsn["value"].toInt();
    m_widget->setValue(num);
    valueChangedSLOT();
}

QWidget *AIWSlider::widget()
{
    return m_widget;
}

AIWSlider::~AIWSlider()
{
    delete m_widget;
}

void AIWSlider::valueChangedSLOT()
{
    double L = dynamic_cast<AnalogInput*>(m_IO)->lowerLimit();
    double H = dynamic_cast<AnalogInput*>(m_IO)->upperLimit();
    double buf = Static::scaleToDouble(m_widget->value(), 0, m_widget->maximum(), L, H);
    emit valueChaneged(buf);
}
