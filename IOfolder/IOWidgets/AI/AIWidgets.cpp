#include "AIWidgets.h"

AIWidgets::AIWidgets()
{}

const QStringList AIWidgets::typeNames =         //Обязательно должен быть согласован по количеству и порядку элементов с AIW_types
{
    "[W]LineEdit",
    "[W]Slider"
};

QStringList AIWidgets::getTypeNames()
{
    return typeNames;
}

QString AIWidgets::getTypeName(int i)
{
    return typeNames.at(i);
}

IOWidget *AIWidgets::newWidget(QWidget *parent, int type, QString name)
{
    IOWidget* newW;
    switch (type)
    {
    case int(AIWidgets::LineEdit):
        newW = new AIWLineEdit(parent);
    break;
    case int(AIWidgets::Slider):
        newW = new AIWSlider(parent);
    break;
    default:
        newW = new AINone(parent);
    break;
    }
    return newW;
}
