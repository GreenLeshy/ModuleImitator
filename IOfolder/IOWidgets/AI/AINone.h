#ifndef AINONE_H
#define AINONE_H

#include <IOWidget.h>

class AINone : public IOWidget      //Пустой виджет, который занимет место отсутвтующего виджета и сохраняет компановку
{
public:
    AINone(QWidget* parent= nullptr);
};

#endif // AINONE_H
