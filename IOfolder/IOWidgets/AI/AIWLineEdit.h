#ifndef AIWLINEEDIT_H
#define AIWLINEEDIT_H

#include <IOWidget.h>
#include <QLineEdit>

class AIWLineEdit : public IOWidget     //AIWidget с QLineEdit
{
    Q_OBJECT
public:
    AIWLineEdit(QWidget* parent= nullptr);
    virtual QJsonObject toJson(void) override;
    virtual void fromJson(const QJsonObject  jsn) override;
    virtual QWidget* widget(void) override;

    ~AIWLineEdit();
public slots:
    virtual void valueChangedSLOT(void) override;

signals:
    void valueChaneged(double);

protected:
    QLineEdit* m_widget;
};

#endif // AIWLINEEDIT_H
