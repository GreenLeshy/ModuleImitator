#include "AINone.h"
#include "IOs.h"
#include "mainwindow.h"

AINone::AINone(QWidget *parent) : IOWidget(parent)
{
    this->setMinimumWidth(Main->elSize(MainWindow::s_AIWidget));
    this->setMaximumWidth(Main->elSize(MainWindow::s_AIWidget));
    m_IO->layout()->insertWidget(1, this);
}
