#ifndef AIWSLIDER_H
#define AIWSLIDER_H

#include <IOWidget.h>
#include <QSlider>

class AIWSlider : public IOWidget     //AIWidget с QSlider
{
    Q_OBJECT
public:
    AIWSlider(QWidget* parent= nullptr);
    virtual QJsonObject toJson(void) override;
    virtual void fromJson(const QJsonObject  jsn) override;
    virtual QWidget* widget(void) override;
    ~AIWSlider();
public slots:
    virtual void valueChangedSLOT(void) override;

signals:
    void valueChaneged(double);

protected:
    QSlider* m_widget;
};

#endif // AIWSLIDER_H
