#include "IOWidget.h"
#include "IOs.h"

IOWidget::IOWidget(QWidget *parent) : QWidget(parent)
{
    m_IO=dynamic_cast<IOClass*>(parent);
    Main=m_IO->getMain();
}

QWidget *IOWidget::widget()
{
    return nullptr;
}

void IOWidget::valueChangedSLOT()
{

}

QJsonObject IOWidget::toJson()
{
    QJsonObject jio;
    return jio;
}

void IOWidget::fromJson(const QJsonObject jsn)
{

}

QString IOWidget::type() const
{
    return m_type;
}
