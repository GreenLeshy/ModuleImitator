#ifndef IOWIDGET_H
#define IOWIDGET_H

#include <QWidget>
#include <QObject>
#include <QJsonObject>
#include <QJsonArray>

class IOClass;
class MainWindow;
class IOWidget : public QWidget         //Виджеты для ручного ввода значений
{
    Q_OBJECT
public:
    IOWidget(QWidget* parent= nullptr);
    virtual QWidget* widget(void);          //Возвращает указатель на свой виджет ввода

    QString type() const;

public slots:
    virtual void valueChangedSLOT(void);
    virtual QJsonObject toJson(void);       //Сохранить в QJsonObject
    virtual void fromJson(const QJsonObject  jsn); //Загрузить из QJsonObject

signals:
    //void valueChaneged(double/bool);

protected:
    IOClass* m_IO;                      //Родительский IO
    MainWindow *Main;
    QString m_type="NONE";
};

#endif // IOWIDGET_H
