#ifndef DOWIDGETS_H
#define DOWIDGETS_H
#include "IOWidget.h"


class DOWidgets             //Объединяет DOWidget в одну библиотеку и реализует фабрику объектов
{
public:
    DOWidgets();

    enum DOW_types: unsigned         //Обязательно должен быть согласован по количеству и порядку элементов с typeNames
    {
        NONE=0,
    };

    static QStringList getTypeNames();
    static QString getTypeName(int i);
    static IOWidget *newWidget(QWidget *parent=nullptr, int type=0, QString name="");

private:
    static const QStringList typeNames;
};

#endif // DOWIDGETS_H
