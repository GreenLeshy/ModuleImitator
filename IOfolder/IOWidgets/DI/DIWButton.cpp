#include "DIWButton.h"
#include "mainwindow.h"
#include "IOs.h"
#include "DIWidgets.h"

DIWButton::DIWButton(QWidget *parent) : IOWidget(parent)
{
    m_widget=new QPushButton("1/0", this);
    m_widget->setMinimumWidth(Main->elSize(MainWindow::s_DIWidget));
    m_widget->setMaximumWidth(Main->elSize(MainWindow::s_DIWidget));
    connect(m_widget, &QPushButton::pressed, this, &DIWButton::valueChangedSLOT);
    connect(this, &DIWButton::valueChaneged, dynamic_cast<DiscreteInput*>(m_IO), &DiscreteInput::newValue);
    m_widget->setStyleSheet("QPushButton { background: rgb(150, 210, 225); selection-background-color: rgb(60, 130, 155); }");
    m_IO->layout()->insertWidget(1, m_widget);
    m_value=dynamic_cast<DiscreteInput*>(m_IO)->value();
    m_type = DIWidgets::getTypeName(0);
}

DIWButton::~DIWButton()
{
    delete m_widget;
}

QWidget *DIWButton::widget()
{
    return m_widget;
}

void DIWButton::valueChangedSLOT()
{
    m_value=dynamic_cast<DiscreteInput*>(m_IO)->value();
    m_value=1-m_value;
    emit valueChaneged(m_value);
}
