#ifndef DIWIDGETS_H
#define DIWIDGETS_H
#include "DIWButton.h"
#include "DINone.h"


class DIWidgets             //Объединяет DIWidget в одну библиотеку и реализует фабрику объектов
{
public:
    DIWidgets();

    enum DIW_types: unsigned        //Обязательно должен быть согласован по количеству и порядку элементов с typeNames
    {
        Button=0,
    };

    static QStringList getTypeNames();
    static QString getTypeName(int i);
    static IOWidget *newWidget(QWidget *parent=nullptr, int type=0, QString name="");

private:
    static const QStringList typeNames;
};

#endif // DIWIDGETS_H
