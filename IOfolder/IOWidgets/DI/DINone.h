#ifndef DINONE_H
#define DINONE_H

#include <IOWidget.h>

class DINone : public IOWidget      //Пустой виджет, который занимет место отсутвтующего виджета и сохраняет компановку
{
public:
    DINone(QWidget* parent= nullptr);
};

#endif // DINONE_H
