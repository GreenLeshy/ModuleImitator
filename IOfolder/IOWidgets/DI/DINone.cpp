#include "DINone.h"
#include "IOs.h"
#include "mainwindow.h"

DINone::DINone(QWidget *parent) : IOWidget(parent)
{
    this->setMinimumWidth(Main->elSize(MainWindow::s_DIWidget));
    this->setMaximumWidth(Main->elSize(MainWindow::s_DIWidget));
    m_IO->layout()->insertWidget(1, this);
}
