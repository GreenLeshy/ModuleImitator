#ifndef DIWBUTTON_H
#define DIWBUTTON_H

#include "IOWidget.h"
#include <QPushButton>

class DIWButton : public IOWidget     //DIWidget с QPushButton
{
    Q_OBJECT

public:
    DIWButton(QWidget* parent= nullptr);
    ~DIWButton();
    virtual QWidget* widget(void) override;

public slots:
    virtual void valueChangedSLOT(void) override;

signals:
    void valueChaneged(bool);

protected:
    QPushButton* m_widget;
    bool m_value=0;
};

#endif // DIWBUTTON_H
