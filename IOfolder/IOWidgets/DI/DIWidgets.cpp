#include "DIWidgets.h"

DIWidgets::DIWidgets()
{}

const QStringList DIWidgets::typeNames =         //Обязательно должен быть согласован по количеству и порядку элементов с DIW_types
{
    "[W]Button"
};

QStringList DIWidgets::getTypeNames()
{
    return typeNames;
}

QString DIWidgets::getTypeName(int i)
{
    return typeNames.at(i);
}

IOWidget *DIWidgets::newWidget(QWidget *parent, int type, QString name)
{
    IOWidget* newW;
    switch (type)
    {
    case int(DIWidgets::Button):
        newW = new DIWButton(parent);
    break;
    default:
        newW = new DINone(parent);
    break;
    }
    return newW;
}
