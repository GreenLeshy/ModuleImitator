#include "AIContact.h"
#include "Imitation.h"
#include "mainwindow.h"

AIContact::AIContact(QObject *parent, QString name) : Contact(parent, name)
{
    m_dataType=data_type::Analog;
    m_IOType=IO_type::Input;
    m_IsBusy = false;
    Main->addContact(this);
}

bool AIContact::bound(Contact *pair)
{
    if(!Contact::bound(pair))
        return 0;
    if(IsBusy())
        disbound();
    else if(m_widget!="")
        disboundWidget();
    pair->bound(this);  //Связывание всегда осуществляет тот, кто отправляет данные
    return 1;
}

void AIContact::disbound()
{
    if(m_pair!=nullptr)
        m_pair->disbound();  //Связывание всегда осуществляет тот, кто отправляет данные
    this->Contact::disbound();
}

void AIContact::setValue(double val)
{
    if(val!=m_value)
    {
        m_value=val;
        emit newValue(val);
    }
}

double AIContact::value() const
{
    return m_value;
}
