#ifndef AICONTACT_H
#define AICONTACT_H

#include "Contact.h"

class AIContact : public Contact
{
    Q_OBJECT
public:
    explicit AIContact(QObject *parent = nullptr, QString name = "WRONGNAME");

    double value() const;                           //Вернуть текущее значение

public slots:
    virtual bool bound(Contact* pair) override;      //Отправить запрос на привязку к pair
    virtual void disbound() override;                        //Отвязаться от текущей пары
    void setValue(double val);                   //Установить текущее значение

signals:
    void newValue(double val);                  //Есть новое значение

protected:
    double m_value;                                     //текущее значение
};

#endif // AICONTACT_H
