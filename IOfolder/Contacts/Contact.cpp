#include "Contact.h"
#include "IOClass.h"
#include "Imitation.h"
#include "mainwindow.h"
#include <QMessageBox>

Contact::Contact(QObject *parent,  QString name) : QObject(parent)
{
    setParent(parent);
    MyIO = dynamic_cast<IOClass*>(parent);
    MyTable = dynamic_cast<Imitation*>(MyIO->parent());
    Main = MyTable->getMain();
    m_name = name;
    connect(this, &Contact::contactChanged, Main, &MainWindow::contactChanged);
    connect(this, &Contact::contactRenamed, Main, &MainWindow::contactRenamed);
    connect(this, &Contact::boundToBusy, Main, &MainWindow::showBadBoundTry);

    m_widget="";
}

Contact::~Contact()
{
}

void Contact::boundWidget(QString widget, bool fromIO)
{
    if(m_pair!=nullptr&&widget!="NONE")
        disbound();
    if(m_widget!="")
        disboundWidget(fromIO);
    m_widget=widget;
    emit contactChanged(this);
}

void Contact::disbound()
{
    emit contactChanged(this);
}

void Contact::disboundWidget(bool fromIO)
{
    if(m_widget!="")
    {
        m_widget="";
        if(!fromIO)
            MyIO->disboundWidget();
    }
    emit contactChanged(this);
}

data_type Contact::dataType() const
{
    return m_dataType;
}

IO_type Contact::IOType() const
{
    return m_IOType;
}

bool Contact::IsBusy() const
{
    return m_IsBusy;
}

Contact *Contact::pair() const
{
    return m_pair;
}

QString Contact::name() const
{
    return m_name;
}

void Contact::setName(const QString &newName)
{
    m_name = newName;
    emit contactRenamed(this);
}

const QString Contact::widgetType() const
{
    return m_widget;
}

void Contact::setWidget(const QString &newWidget)
{
    m_widget = newWidget;
}

IOClass *Contact::getIO() const
{
    return MyIO;
}

MainWindow *Contact::getMain() const
{
    return Main;
}

void Contact::checkLoop(QVector<Contact *> &contacts, bool &loop)
{
    if(loop)
        return;
    if(contacts.contains(this))
    {
        loop=1;
        return;
    }
    else
        contacts.append(this);
    if(m_IOType==Output)
    {
        if(m_pair!=nullptr)
            m_pair->checkLoop(contacts, loop);
        return;
    }
    else
    {
        auto vect = MyTable->depends(this);
        foreach (auto cont, vect)
            cont->checkLoop(contacts, loop);
        return;
    }
}

void Contact::setIsBusy(bool newIsBusy)
{
    m_IsBusy = newIsBusy;
    emit contactChanged(this);
}

void Contact::setPair(Contact *newPair)
{
    m_pair = newPair;
    emit contactChanged(this);
}

bool Contact::bound(Contact*pair)
{
    if((pair->IsBusy())&(pair->pair()!=nullptr))    //Попытка привязаться к занятому контакту
    {
        emit boundToBusy();
        return 0;
    }
    if(pair->IOType()==IOType()||pair->dataType()!=dataType())
    {
        QMessageBox msgBox(Main);
        QString msg;
        msg.append("Произошла попытка связывания несоответствующих контактов!\n");
        msg.append("Скорее всего несколько контактов имеют одинаковые имена\n");
        msg.append(QString("Связываемая пара контактов: \"%1\" и \"%2\"").arg(this->name(), pair->name()));
        msgBox.setText(QString(msg));
        msgBox.exec();
        return 0;
    }
    bool loop=0;
    QVector<Contact *> vect;
    this->checkLoop(vect, loop);
    pair->checkLoop(vect, loop);    //Вызов checkLoop и для this и для pair проверяет, создадут ли они петля до того, как они действительно будут соединены
    if(loop)
    {
        QMessageBox msgBox(Main);
        QString msg;
        msg.append("Данное соединие приведет к созданию петли и остановке работы приложения!\n");
        msg.append("Если вы желаете осуществить данное соединение, используйте промежуточную линию задержки\n");
        msgBox.setText(QString(msg));
        msgBox.exec();
        return 0;
    }

    return 1;
}
