#ifndef CONTACT_H
#define CONTACT_H

#include <QDebug>
#include <QObject>
#include "static.h"

class MainWindow;
class Imitation;
class IOClass;
class Contact : public QObject
{
    Q_OBJECT
public:
    explicit Contact(QObject *parent = nullptr, QString name = "WRONGNAME");
    ~Contact();

    virtual void boundWidget(QString, bool fromIO = 0);  //Привязаться к виджету, если fromIO=1, значит команда пришла от IO и не надо отправлять IO команду на обновление IO.m_connectionChoise
public slots:
    virtual bool bound(Contact* pair);       //Необходимо привязаться к указанному контакту
    virtual void disbound();                //Необходимо отвязаться от текущего контакта
    virtual void disboundWidget(bool fromIO = 0);           //Отвязаться от виджета, если fromIO=1, значит команда пришла от IO и не надо отправлять IO команду на обновление IO.m_connectionChoise

    //void setValue(bool/double);       !!!!-ИМЕЕТ РАЗНЫЕ ТИПЫ ПАРАМЕТРОВ ДЛЯ РАЗНЫХ ТИПОВ СИГНАЛОВ-!!!! Пришло новое значение
signals:
    void contactChanged(Contact *con);  //Информация контакта изменилась
    void contactRenamed(Contact *con); //Контакт переименован
    void boundToBusy(void);
    //void newValue(bool/double)       !!!!-ИМЕЕТ РАЗНЫЕ ТИПЫ ПАРАМЕТРОВ ДЛЯ РАЗНЫХ ТИПОВ СИГНАЛОВ-!!!! Необходимо передать новое значение (связан с newValue IO и setValue других контактов)
public:

data_type dataType() const;                     //Возвращает тип передаваемых данных
IO_type IOType() const;                             //Возвращает тип контакта
bool IsBusy() const;                                   //Привязан ли контакт
void setIsBusy(bool newIsBusy);              //Установить isBusy
Contact *pair() const;                                 //Возвращает указатель на пару
void setPair(Contact *newPair);                //Устанавливает пару
QString name() const;                               //Возвращает имя

void setName(const QString &newName);   //Устанавливает имя

const QString widgetType() const;                          //Возвращает тип подключенного виджета
void setWidget(const QString &newWidget);   //Устанавливает тип подключенного виджета

IOClass *getIO() const;
MainWindow *getMain() const;

void checkLoop(QVector<Contact*> &contacts, bool &loop);        //Контакт проверяет, включен ли он в петлю, состав петли = contacts, loop = 1, если петля замкнулась

protected:
    data_type m_dataType;                           //Тип передаваемых данных
    IO_type m_IOType;                                   //Тип контакта (входной/выходной)
    bool m_IsBusy;                                         //Занят ли (привязан ли) контакт
    Contact *m_pair=nullptr;                        //К какому контакту привязан
    QString m_name;                                     //Имя контакта

    IOClass *MyIO;                                         //IO, который является родителем этого контакта
    Imitation *MyTable;                                 //Imitation, который является прародителем этого контакта
    MainWindow *Main;                               //Главное окно
    QString m_widget;                                   //Тип виджета, к которому привязан контакт
};

#endif // CONTACT_H
