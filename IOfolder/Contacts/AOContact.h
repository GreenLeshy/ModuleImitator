#ifndef AOCONTACT_H
#define AOCONTACT_H

#include "Contact.h"

class AOContact : public Contact
{
    Q_OBJECT
public:
    explicit AOContact(QObject *parent = nullptr, QString name = "WRONGNAME");

public slots:
    virtual bool bound(Contact* pair) override;      //Привязаться к pair
    virtual void disbound() override;                        //Отвязаться от текущей пары
    void setValue(double val);                   //Установить текущее значение

signals:
    void newValue(double val);                  //Есть новое значение

protected:
    double m_value;                                     //текущее значение
};

#endif // AOCONTACT_H
