#include "DIContact.h"
#include "Imitation.h"
#include "mainwindow.h"

DIContact::DIContact(QObject *parent, QString name) : Contact(parent, name)
{
    m_dataType=data_type::Discrete;
    m_IOType=IO_type::Input;
    m_IsBusy = false;
    Main->addContact(this);
}

bool DIContact::bound(Contact *pair)
{
    if(!Contact::bound(pair))
        return 0;
    if(IsBusy())
        disbound();
    else if(m_widget!="")
        disboundWidget();
    pair->bound(this);  //Связывание всегда осуществляет тот, кто отправляет данные
    return 1;
}

void DIContact::disbound()
{
  if(m_pair!=nullptr)
    m_pair->disbound();  //Связывание всегда осуществляет тот, кто отправляет данные
  this->Contact::disbound();
}

void DIContact::setValue(bool val)
{
    if(val!=m_value)
    {
        m_value=val;
        emit newValue(val);
    }
}
