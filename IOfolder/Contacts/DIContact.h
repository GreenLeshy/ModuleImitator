#ifndef DICONTACT_H
#define DICONTACT_H

#include "Contact.h"

class DIContact : public Contact
{
    Q_OBJECT
public:
    explicit DIContact(QObject *parent = nullptr, QString name = "WRONGNAME");

public slots:
    virtual bool bound(Contact* pair) override;
    virtual void disbound() override;
    void setValue(bool val);

signals:
    void newValue(bool val);

protected:
    bool m_value;
};

#endif // DICONTACT_H
