#include "Contacts.h"
#include "Imitation.h"
#include "mainwindow.h"
#include "IOs.h"
#include "AIWidgets.h"

AOContact::AOContact(QObject *parent, QString name) : Contact(parent, name)
{
    m_dataType=data_type::Analog;
    m_IOType=IO_type::Output;
    m_IsBusy = false;
    Main->addContact(this);
}

bool AOContact::bound(Contact *pair)
{
    if(!Contact::bound(pair))
        return 0;
    if(IsBusy())
        disbound();
    disboundWidget();
    this->blockSignals(true);
    pair->blockSignals(true);
    pair->disboundWidget();
    pair->getIO()->boundWidget("NONE");
    connect(this, &AOContact::newValue, dynamic_cast<AIContact*>(pair), &AIContact::setValue);
    this->m_IsBusy=true;
    this->m_pair=pair;
    pair->setIsBusy(true);
    pair->setPair(this);
    this->m_widget="";
    pair->setWidget("");
    this->blockSignals(false);
    pair->blockSignals(false);
    emit contactChanged(this);
    emit contactChanged(pair);
    (dynamic_cast<AIContact*>(pair))->setValue(m_value);
     return 1;
}

void AOContact::disbound()
{
    if(m_pair!=nullptr)
    {
        m_pair->blockSignals(true);
        m_pair->setIsBusy(false);
        m_pair->setPair(nullptr);
        m_pair->blockSignals(false);
        disconnect(this, &AOContact::newValue, dynamic_cast<AIContact*>(m_pair), &AIContact::setValue);
        m_pair->Contact::disbound();
        m_pair->getIO()->boundWidget(AIWidgets::getTypeName(AIWidgets::LineEdit));
        m_pair=nullptr;
    }
    this->blockSignals(true);
    this->m_IsBusy=false;
    this->blockSignals(false);
    this->Contact::disbound();
}

void AOContact::setValue(double val)
{
    if(val!=m_value)
    {
        m_value=val;
        emit newValue(val);
    }
}
