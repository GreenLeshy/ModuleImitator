#ifndef DOCONTACT_H
#define DOCONTACT_H

#include "Contact.h"

class DOContact : public Contact
{
    Q_OBJECT
public:
    explicit DOContact(QObject *parent = nullptr, QString name = "WRONGNAME");
    //virtual QJsonObject toJson(void);

public slots:
    virtual bool bound(Contact* pair) override;
    virtual void disbound() override;
    void setValue(bool val);

signals:
    void newValue(bool val);

protected:
    bool m_value;
};

#endif // DOCONTACT_H
