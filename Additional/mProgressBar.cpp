#include "mProgressBar.h"
#include "static.h"

mProgressBar::mProgressBar(QWidget *parent, double lowerLimit, double upperLimit, double lErrorLimit, double hErrorLimit) : QProgressBar(parent)
{
    m_lowerLimit=lowerLimit;
    m_upperLimit=upperLimit;
    m_lErrorLimit=lErrorLimit;
    m_hErrorLimit=hErrorLimit;
    this->setStyle(QStyleFactory::create("Fusion"));
    this->setStyleSheet(c_green);
    this->setMinimum(0);
    this->setMaximum(100);
}

mProgressBar::mProgressBar(mProgressBar *bar, QWidget *parent)
{
        if(parent!=nullptr)
            this->setParent(parent);
        else
            this->setParent(bar);
        m_lowerLimit=bar->lowerLimit();
        m_upperLimit=bar->upperLimit();
        m_lErrorLimit=bar->lErrorLimit();
        m_hErrorLimit=bar->hErrorLimit();
        this->setStyle(QStyleFactory::create("Fusion"));
        this->setStyleSheet(c_green);
        this->setMinimum(0);
        this->setMaximum(100);
        setValue(bar->value());
}

void mProgressBar::setLowerLimit(double newLowerLimit)
{
    m_lowerLimit = newLowerLimit;
    setValue(m_value);
}

void mProgressBar::setUpperLimit(double newUpperLimit)
{
    m_upperLimit = newUpperLimit;
    setValue(m_value);
}

void mProgressBar::setLErrorLimit(double newLErrorLimit)
{
    m_lErrorLimit = newLErrorLimit;
    setValue(m_value);
}

void mProgressBar::setHErrorLimit(double newHErrorLimit)
{
    m_hErrorLimit = newHErrorLimit;
    setValue(m_value);
}

void mProgressBar::setLimits(double lowerLimit, double upperLimit, double lErrorLimit, double hErrorLimit)
{
    m_lowerLimit=lowerLimit;
    m_upperLimit=upperLimit;
    m_lErrorLimit=lErrorLimit;
    m_hErrorLimit=hErrorLimit;
    setValue(m_value);
}
