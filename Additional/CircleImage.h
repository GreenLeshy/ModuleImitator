#ifndef CIRCLEIMAGE_H
#define CIRCLEIMAGE_H

#include <QImage>
//#include <QRgb>

class CircleImage : public QImage                       //Класс для изображения круга
{
public:
    inline CircleImage(int size, QColor colour, QColor backColour = Qt::white);
    inline void doCircle(int size, QColor colour);                     //Расчет изображения круга
};

CircleImage::CircleImage(int size, QColor colour, QColor backColour) : QImage(size, size, QImage::Format_RGB888)
{
    this->fill(backColour);
    doCircle(size, colour);
}

void CircleImage::doCircle(int size, QColor colour)
{
    int x, y;
    float center = (float)size/2;
    float cx, cy;
    int half = (size+1)/2;
    for(y=0; y<half+1; ++y) //Без 1 остается пустая полоса
        for(x=0; x<half; ++x)
        {
            cx=center - (float)x;
            cy=center - (float)y;
            if(center*center>(cx*cx+cy*cy))
            {
                for(int i=x; i<(size-x); ++i)   //Заполнение сразу по 2 строки
                {
                    this->setPixelColor(i, y, colour);
                    this->setPixelColor(i, size-y, colour);
                }
                for(int i=y; i<(size-y); ++i)   //Заполнение сразу по 2 строки
                {
                    this->setPixelColor(i, x, colour);
                    this->setPixelColor(i, size-x, colour);
                }
                x=half; //Переходим к следующей строке
            }
        }
}


#endif // CIRCLEIMAGE_H
