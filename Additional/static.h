#ifndef STATIC_H
#define STATIC_H

#include <QByteArray>
#include <QSize>
#include <QVariant>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#define ANALOG12_MINIMUM 0
#define ANALOG14_MINIMUM 0
#define ANALOG12_MAXIMUM 4095
#define ANALOG14_MAXIMUM 16383
#define ANALOG12_ERROR 819
#define ANALOG14_ERROR 3276

enum data_type : unsigned
{
    Discrete =0,
    Analog
};
enum IO_type : unsigned
{
    Input = 0,
    Output
};

class QLayout;
class Static
{
public:
    Static();
    static inline bool CRCRight(QByteArray &com);           //Проверка CRC
    static inline QByteArray CRC(const QByteArray &com, bool ignoreLast=0);         //Расчет CRC, если ignoreLast=1, то последние 2 байта не учитваются
    static inline QByteArray toU8(quint16);                      //Преобразование quint16 в U8 (в виде QByteArray длинной в 2 символа)
    static inline quint16 toU16(uchar H, uchar L);            //Преобразование U8 (в виде QByteArray длинной в 2 символа) в quint16
    static QString writeTag(const QString &string, const QString &tag, char Topen, char Tclose, char Tending=0);                          //Вписать tag в строку string от первого вхождения Topen до последнего вхождения Tclose
    static inline int scaleToInt(double val, double L1, double H1, int L2, int H2, bool limited=0);            //Масштабировать число (double)val из диапазона (double)L1-H1 в число (int) из диапазона (int)L2-H2, и ограничить его этим диапазоном, если limited=1
    template<typename T>
        static inline T clamp(T val, T L, T H);                                                             //Возвращает val если L<val<H, L если val<L, H если val>H
    static inline double scaleToDouble(int val, int L1, int H1, double L2, double H2, bool limited=0);    //Масштабировать число (int)val из диапазона (int)L1-H1 в число (double) из диапазона (double)L2-H2, и ограничить его этим диапазоном, если limited=1
    static void clearLayout(QLayout* layout, bool deleteWidgets = true);                        //Очистить layout, если deleteWidgets, то удалить все его элементы
    static bool shiftInsert(QMap<QString, int>* map, const QString &newString, int newValue); //Вставить значение в QMap, сдвинув на +1 все QMap.value, которые больше newValue, возвращает 1, если такой элемент уже был
    static void shiftRemove(QMap<QString, int>* map, const QString &deleteString);                //Удалить значение из QMap, сдвинув на -1 все QMap.value, которые больше newValue
    static QString changeFolderBranch(QString path, QString oldBranch, QString newBranch);  //Заменяет oldBranch на newBranch в пути path
    static QString cut(QString text, const QString &mark, bool leftRight=0);                                           //Возвращает часть строки text, находящуюся слева (leftRight=0) или справа (leftRight=1) от подстроки mark
    static void replaceInJSON( const QString &fileName, const QList<QString> &keys, const QList<QJsonValue> &values); //Заменяет пары значений keys-values в указанном JSON файле
    static void replaceInJSON( const QString &fileName, const QString  &key, const QJsonValue &value);                             //Перегруженная функция
    static bool StringListsIsEqual(const QStringList &L1, const QStringList &L2);

};

template<typename T>
T Static::clamp(T val, T L, T H)                                                             //Возвращает val если L<val<H, L если val<L, H если val>H
{
    if(val<L) return L;
    if(val>H) return H;
    return val;
}

//inline функции
bool Static::CRCRight(QByteArray &com)
{
    int n=com.size();
    if(n<3)
        return 0;
    QByteArray buf2 = CRC(com, 1);
    if(buf2[0]==com.at(n-2)&&buf2[1]==com.at(n-1))
        return 1;
    else
        return 0;
}

QByteArray Static::toU8(quint16 val)
{
    QByteArray buf;
    buf[0] = val & 0xff;
    buf[1] = (val >> 8);
    return buf;
}

QByteArray Static::CRC(const QByteArray &com, bool ignoreLast)
{
    quint16 crc = 0xFFFF;
    int n = com.size();
    if(ignoreLast)
        n-=2;
    for(int i =0; i<n; ++i)
    {
        crc = uchar(com.at(i)) ^ crc;
        for(int j = 0; j<8; ++j)
        {
            if((crc&0x01))
            {
                crc = crc >> 1;
                crc = crc ^ 0xA001;
            }
            else
                crc = crc >> 1;
        }
    }
    return Static::toU8(crc);
}

quint16 Static::toU16(uchar H, uchar L)
{
    quint16 buf;
    buf=(H<<8)+L;
    return buf;
}

int Static::scaleToInt(double val, double L1, double H1, int L2, int H2, bool limited)
{
    int buf;
    buf=L2;
    buf+=((val-L1)/(H1-L1))*(H2-L2)+0.001;
    if(limited)
        buf = clamp(buf, L2, H2);
    return buf;
}

double Static::scaleToDouble(int val, int L1, int H1, double L2, double H2, bool limited)
{
    double buf;
    buf=L2;
    buf+=(double(val-L1)/(H1-L1))*(H2-L2);
    if(limited)
        buf = clamp(buf, L2, H2);
    return buf;
}

#endif // STATIC_H
