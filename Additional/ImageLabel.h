#ifndef IMAGELABEL_H
#define IMAGELABEL_H

#include <QLabel>

class QPixmap;
class QPaintEvent;

class ImageLabel : public QLabel            //QLabel, оптимизированный для QPixmap
{
public:
    ImageLabel(QWidget *parent);
    void setPixmap(QPixmap* pxmp);
    void paintEvent(QPaintEvent *);

protected:
    QPixmap *m_pixmap=nullptr;
};

#endif // IMAGELABEL_H
