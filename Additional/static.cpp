#include "static.h"
#include <QDebug>
#include <QLayout>
#include <QLayoutItem>
#include <QWidget>
#include <QMap>

Static::Static()
{

}

QString Static::writeTag(const QString &string, const QString &tag, char Topen, char Tclose, char Tending)
{
    QString buf=string;
    int O, C, E;
    O = string.indexOf(Topen);
    E=INT_MAX;
    if(Tending!=0)
    {
        E = string.lastIndexOf(Tending);
        buf=string.left(E);
    }
    C = buf.lastIndexOf(Tclose);    //Поиск только до знака Tending
    if((O>-1)&&(C>O))
    {
        QString buf;
        for(int i =0; i < O+1; ++i)
            buf.append(string[i]);
        buf.append(tag);
        int n=string.size();
        for(int i =C; i < n; ++i)
            buf.append(string[i]);
        return buf;
    }
    else
        return string;
}

void Static::clearLayout(QLayout *layout, bool deleteWidgets)
{
    while (QLayoutItem* item = layout->takeAt(0))
    {
        if(deleteWidgets)
        {
            if(QWidget* widget = item->widget())
                widget->deleteLater();
        }
        if(QLayout* childLayout = item->layout())
            clearLayout(childLayout, deleteWidgets);
        delete item;
    }
}

bool Static::shiftInsert(QMap<QString, int>* map, const QString &newString, int newValue)
{
    bool buf=0;
    if(map->key(newValue)!="")
    {
        buf=1;
        shiftInsert(map, map->key(newValue), newValue+1);   //Сдвинуть все последующие на 1
    }
    map->insert(newString, newValue);
    return buf;
}

void Static::shiftRemove(QMap<QString, int>* map, const QString &deleteString)
{
    int k=map->value(deleteString);
    map->remove(deleteString);
    QStringList list=map->keys();
    for(int i=0; i<list.size(); ++i)
    {
        int j = map->value(list.at(i));
        if(j>k)
        {
            map->insert(list.at(i), j-1);
        }
    }
}

QString Static::changeFolderBranch(QString path, QString oldBranch, QString newBranch)
{
    path.replace('\\', '?');
    oldBranch.replace('\\', '?');
    newBranch.replace('\\', '?');
    path.replace('/', '?');
    oldBranch.replace('/', '?');
    newBranch.replace('/', '?');

    oldBranch.prepend('?');
    if(path.right(oldBranch.size())==oldBranch)                    //Если путь оканчивается oldBranch
        path.remove(path.size()-oldBranch.size(), path.size()); //Удаляем конец пути
    if(path.at(path.size()-1)=='?')                                          //На всякий случай удаляем возможные \ и /
        path.remove(path.size()-1);

    path.append('?');
    path.append(newBranch);       //Конец адреса, который мы хотим добавить

    char c='\\';                            //Определяем, какой разделитель используется на данной платформе
#if defined (Q_OS_WINDOWS)
    c='/';
#endif

    path.replace('?', c);
    return path;
}

QString Static::cut(QString text, const QString &mark, bool leftRight)
{
    int i = text.lastIndexOf(mark);
    if(i!=-1)
    {
        if(leftRight)
            text=text.right(text.size()-(i+mark.size()));
        else
            text=text.left(i);
    }
    return text;
}

void Static::replaceInJSON(const QString &fileName, const QList<QString> &keys, const QList<QJsonValue> &values)
{
    QFile confFile(fileName);
    confFile.open(QIODevice::ReadOnly);
    QString buf=confFile.readAll();
    QJsonDocument doc=QJsonDocument::fromJson(buf.toUtf8());
    QJsonObject Mainobject = doc.object();
    for(int i=0; i<keys.size(); ++i)
        Mainobject[keys.at(i)]=values.at(i);
    QJsonDocument doc2(Mainobject);
    QString jsonString = doc2.toJson(QJsonDocument::Indented);
    confFile.close();                                       //Нормальное удаление содержимого файла не работает
    confFile.open(QIODevice::WriteOnly);
    confFile.write(jsonString.toUtf8());
    confFile.close();
}

void Static::replaceInJSON(const QString &fileName, const QString &key, const QJsonValue &value)
{
    replaceInJSON(fileName, QList<QString>{key}, QList<QJsonValue>{value});
}

bool Static::StringListsIsEqual(const QStringList &L1, const QStringList &L2)
{
    int n=L1.size();
    if(n!=L2.size())
        return false;
    for(int i=0; i<n; ++i)
        if(L1.at(i)!=L2.at(i))
            return false;
    return true;
}
