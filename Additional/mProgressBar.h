#ifndef MPROGRESSBAR_H
#define MPROGRESSBAR_H

#include <QProgressBar>
#include <QObject>
#include <QWidget>
#include <QStyle>
#include <QStyleFactory>
#include <static.h>

class mProgressBar : public QProgressBar                                    //Класс-наследник QProgressBar, динамически меняющий цвет и использующий компактный стиль
{
    Q_OBJECT
public:
    mProgressBar(QWidget *parent=nullptr, double lowerLimit = 0, double upperLimit =20, double lErrorLimit =4, double hErrorLimit =20);
    mProgressBar(mProgressBar *bar, QWidget *parent=nullptr);   //Копирует свойства bar, но родителем ставит parent (если его нет, то родителем становится bar)
    inline void setValue(double value);

    inline double lowerLimit() const;
    void setLowerLimit(double newLowerLimit);
    inline double upperLimit() const;
    void setUpperLimit(double newUpperLimit);
    inline double lErrorLimit() const;
    void setLErrorLimit(double newLErrorLimit);
    inline double hErrorLimit() const;
    void setHErrorLimit(double newHErrorLimit);
    void setLimits(double lowerLimit = 0, double upperLimit =20, double lErrorLimit =4, double hErrorLimit =20);

    inline double value() const;

protected:
    double m_lowerLimit;            //Минимальное значение
    double m_upperLimit;            //Максимальное значение
    double m_lErrorLimit;             //Порог сигнализации по нижнему уровню
    double m_hErrorLimit;            //Порог сигнализации по верхнему уровню
    double m_value=0;

    QString c_green="QProgressBar {text-align: center; border: 1px solid green} QProgressBar::chunk{background-color: #20DD30}";    //Стиль для QStyleFactory
    QString c_red="QProgressBar {text-align: center; border: 1px solid red} QProgressBar::chunk{background-color: #FF0000}";              //Стиль для QStyleFactory
};

//inline


double mProgressBar::lowerLimit() const
{
    return m_lowerLimit;
}

double mProgressBar::upperLimit() const
{
    return m_upperLimit;
}

double mProgressBar::lErrorLimit() const
{
    return m_lErrorLimit;
}

double mProgressBar::hErrorLimit() const
{
    return m_hErrorLimit;
}

double mProgressBar::value() const
{
    return m_value;
}

void mProgressBar::setValue(double value)
{
    m_value = value;
    if((value<m_lErrorLimit)||(value>m_hErrorLimit))
        this->setStyleSheet(c_red);
    else
        this->setStyleSheet(c_green);
    double buf = Static::clamp(value, m_lowerLimit, m_upperLimit);
    this->QProgressBar::setValue(Static::scaleToInt(buf, m_lowerLimit, m_upperLimit, 0, 100, true));
}

#endif // MPROGRESSBAR_H
