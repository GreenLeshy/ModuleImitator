#ifndef MEMORYVIEW_H
#define MEMORYVIEW_H

#include <QWidget>
#include <QTableWidget>
#include <QLayout>
#include <QComboBox>
class Module;
class MemoryView : public QWidget       //Окно просмотра и редактирования регистров Custom-модуля
{
    Q_OBJECT
public:
    explicit MemoryView(QWidget *parent = nullptr);

    int memAddr() const;
    void setMemAddr(int newMemAddr);

    int memEnd() const;
    void setMemEnd(int newMemEnd);
    void readAll();

public slots:
    void cellChaneged(int i);
    void writeCell(void);
    void writeName(void);
    void razrChanged(QString razr);         //Пользователь сменил разрядность, в которой отображаются значения
    void search(QString text);

signals:

protected:
    Module *m_module;
    int m_memAddr;                                                   //Начальный адрес памяти
    int m_memEnd;                                                     //Конечный адрес памяти
    QTableWidget m_table;
    QVBoxLayout *m_layout;
    int m_firstRow;
    int m_lastRow;
    QVector<QLineEdit *> m_cellsLines;
    QVector<QLineEdit *> m_namesLines;
    int m_razr = 10;
    QComboBox *m_razrWidget;
    QLineEdit *m_searchWidget;
};

#endif // MEMORYVIEW_H
