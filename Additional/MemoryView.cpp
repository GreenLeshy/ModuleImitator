#include "MemoryView.h"
#include "Module.h"

MemoryView::MemoryView(QWidget *parent) : QWidget(parent)
{
    m_module = dynamic_cast<Module*>(parent);
    //setWindowFlags(Qt::Window | Qt::CustomizeWindowHint);
    setWindowFlag(Qt::Window);
    m_memAddr = m_module->memAddr();
    m_memEnd = m_module->memEnd();

    m_table.setColumnCount(10);

    m_layout = new QVBoxLayout(this);
    m_layout->addWidget(&m_table);
    this->setMinimumSize(m_layout->minimumSize());

    readAll();
    connect(m_module, &Module::registerChanged, this, &MemoryView::cellChaneged);

    QLabel *descr = new QLabel("Формат: ", this);
    m_razrWidget = new QComboBox(this);
    m_razrWidget->addItems({"2", "10", "16"});
    m_razrWidget->setCurrentIndex(1);
    QHBoxLayout *HL=new QHBoxLayout;
    HL->addWidget(descr);
    HL->addWidget(m_razrWidget);
    HL->addStretch(1);
    descr = new QLabel("Найти: ", this);
    m_searchWidget = new QLineEdit(this);
    HL->addWidget(descr);
    HL->addWidget(m_searchWidget);
    m_layout->addLayout(HL);

    connect(m_razrWidget, &QComboBox::currentTextChanged, this, &MemoryView::razrChanged);
    connect(m_searchWidget, &QLineEdit::textChanged, this, &MemoryView::search);
}

int MemoryView::memAddr() const
{
    return m_memAddr;
}

void MemoryView::setMemAddr(int newMemAddr)
{
    m_memAddr = newMemAddr;
}

int MemoryView::memEnd() const
{
    return m_memEnd;
}

void MemoryView::setMemEnd(int newMemEnd)
{
    m_memEnd = newMemEnd;
}

void MemoryView::readAll()
{
    m_firstRow = m_memAddr/10;
    m_lastRow = m_memEnd/10;
    int n = m_lastRow-m_firstRow+1;

    m_table.setRowCount(n*2);

    QStringList VLabels;
    for(int i=0; i<n; ++i)
    {
        VLabels.append(QString::number((i+m_firstRow)*10));
        VLabels.append(QString::number((i+m_firstRow)*10));
    }
    n=m_memEnd+1-m_memAddr;
    for(int i=0; i<n; ++i)
    {
        int j = i+m_memAddr;
        int buf1 = m_module->getRegister(j);
        QLineEdit *cell = new QLineEdit(QString::number(m_module->getRegister(j)), &m_table);
        m_table.setCellWidget(((j-m_firstRow*10)/10)*2, j%10, cell);
        QString buf = m_module->getRegisterName(j);
        QLineEdit *name = new QLineEdit(m_module->getRegisterName(j), &m_table);
        name->setStyleSheet("QLineEdit { background: rgb(250, 240, 220); selection-background-color: rgb(150, 140, 120); }");
        m_table.setCellWidget(((j-m_firstRow*10)/10)*2+1, j%10, name);

        m_cellsLines.append(cell);
        connect(cell, &QLineEdit::editingFinished, this, &MemoryView::writeCell);
        m_namesLines.append(name);
        connect(name, &QLineEdit::editingFinished, this, &MemoryView::writeName);

    }
    for(int i=0; i<10; ++i)
        m_table.setColumnWidth(i, m_table.columnWidth(i)+10);   //Немного расширить надо
    m_table.setVerticalHeaderLabels(VLabels);
    m_table.setHorizontalHeaderLabels({"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"});
    this->resize(m_table.columnWidth(0)*11, m_table.rowHeight(0)*21);
}

void MemoryView::cellChaneged(int i)
{
    QLineEdit *cell = dynamic_cast<QLineEdit *>(m_table.cellWidget(((i-m_firstRow*10)/10)*2, i%10));
    QString buf = QString::number(m_module->getRegister(i), m_razr);
    if(m_razr==2)
        while(buf.length()<16)
            buf.prepend('0');
    else if(m_razr==16)
    {
        while(buf.length()<4)
            buf.prepend('0');
        buf = buf.toUpper();
    }
    cell->setText(buf);
}

void MemoryView::writeCell()
{
    QLineEdit *cell = dynamic_cast<QLineEdit *>(sender());
    int i = m_cellsLines.indexOf(cell)+m_memAddr;
    m_module->setRegister(i, cell->text().toInt(nullptr, m_razr));
}

void MemoryView::writeName()
{
    QLineEdit *name = dynamic_cast<QLineEdit *>(sender());
    int i = m_namesLines.indexOf(name)+m_memAddr;
    m_module->setRegisterName(i, name->text());
}

void MemoryView::razrChanged(QString razr)
{
    this->blockSignals(true);
    int m_oldRazr=m_razr;
    m_razr = razr.toInt();
    int n = m_cellsLines.size();
    QLineEdit *cell;
    QString buf;
    if(m_razr==10)
        for(int i=0; i<n; ++i)
        {
            cell=m_cellsLines.at(i);
            cell->setText(QString::number(cell->text().toInt(nullptr, m_oldRazr), m_razr));
        }
    else if(m_razr==2)
        for(int i=0; i<n; ++i)
        {
            cell=m_cellsLines.at(i);
            buf=QString::number(cell->text().toInt(nullptr, m_oldRazr), m_razr);
            while(buf.length()<16)
                buf.prepend('0');
            cell->setText(buf);
        }
    else if(m_razr==16)
        for(int i=0; i<n; ++i)
        {
            cell=m_cellsLines.at(i);
            buf=QString::number(cell->text().toInt(nullptr, m_oldRazr), m_razr).toUpper();
            while(buf.length()<4)
                buf.prepend('0');
            cell->setText(buf);
        }
    this->blockSignals(false);
}

void MemoryView::search(QString text)
{
    int size = m_cellsLines.size();
    if(text!="")
    {
        for(int i=0; i<size; ++i)
        {
            QLineEdit* cell = m_cellsLines.at(i);
            if(cell->text().contains(text))
                cell->setStyleSheet("QLineEdit { background: rgb(0, 240, 0); selection-background-color: rgb(20, 170, 20); }");
            else
                cell->setStyleSheet("QLineEdit { background: rgb(255, 255, 255); selection-background-color: rgb(20, 110, 180); }");
            cell = m_namesLines.at(i);
            if(cell->text().contains(text))
                cell->setStyleSheet("QLineEdit { background: rgb(0, 240, 0); selection-background-color: rgb(20, 170, 20); }");
            else
                cell->setStyleSheet("QLineEdit { background: rgb(250, 240, 220); selection-background-color: rgb(150, 140, 120); }");
        }
    }
    else
    {
        for(int i=0; i<size; ++i)
        {
            QLineEdit* cell = m_cellsLines.at(i);
                cell->setStyleSheet("QLineEdit { background: rgb(255, 255, 255); selection-background-color: rgb(20, 110, 180); }");
            cell = m_namesLines.at(i);
                cell->setStyleSheet("QLineEdit { background: rgb(250, 240, 220); selection-background-color: rgb(150, 140, 120); }");
        }
    }
}
