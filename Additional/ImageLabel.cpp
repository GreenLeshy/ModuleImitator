#include "ImageLabel.h"
#include <QPainter>

ImageLabel::ImageLabel(QWidget *parent) : QLabel(parent)
{}

void ImageLabel::setPixmap(QPixmap *pxmp)
{
    m_pixmap=pxmp;
    //this->resize(pxmp->size());
    this->setMinimumSize(pxmp->size());
    this->repaint();
}

void ImageLabel::paintEvent(QPaintEvent *)
{
    if(m_pixmap!=nullptr)
    {
        QPainter painter(this);
        painter.setWindow(this->frameGeometry());
        painter.drawPixmap(this->frameGeometry(), *m_pixmap);
    }
}

