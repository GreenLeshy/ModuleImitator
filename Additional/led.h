#ifndef LED_H
#define LED_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QLayout>
#include <QTimer>

class ImageLabel;
class QPixmap;
class led : public QWidget                                  //Класс, изображающий работу индикации модулей
{
    Q_OBJECT
public:
    explicit led(QWidget *parent = nullptr, QString name = "", int size = 20);
    ~led();

    QHBoxLayout *layout();

public slots:
    void setValue(int val, bool resetVal=0);            //Установить значение
    void blink(int val, int time = 50);                       //Мигнуть указанным цветов на указанное время в мс
    void stopBlink();                                                 //Вызывается при срабатывании таймера, отмеряющего время одиночного мигания
    void swap(bool sw=true);                                    //Поменять местами имя контакта и лампочку

protected:
    QHBoxLayout m_layout;                                      //Layout, на котором находятся все визуальные элементы
    ImageLabel *m_led;                                                      //QLabel, на который помещается изображение лампочки (круга)
    QLabel m_label;                                                    //QLabel с подписью
    int m_val;                                                              //Текущее значение (0 - серый, 1 - зеленый, 2 - красный, 3 - оранжевый)
    QPixmap *m_green;                                             //Изображение зеленой лампочки
    QPixmap *m_red;                                                 //Изображение красной лампочки
    QPixmap *m_gray;                                                //Изображение серой лампочки
    QPixmap *m_yellow;                                           //Изображение jhfy;tdjq лампочки
};

#endif // LED_H
