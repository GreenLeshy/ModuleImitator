#include "led.h"
#include <QPixmap>
#include "CircleImage.h"
#include <ImageLabel.h>

led::led(QWidget *parent, QString name, int size) : QWidget(parent)
{
    m_led=new ImageLabel(this);
    m_green=new QPixmap();
    m_green->convertFromImage(CircleImage(size, Qt::green, Qt::white));
    m_red=new QPixmap;
    m_red->convertFromImage(CircleImage(size, Qt::red, Qt::white));
    m_gray=new QPixmap;
    m_gray->convertFromImage(CircleImage(size, Qt::gray, Qt::white));
    m_yellow=new QPixmap;
    m_yellow->convertFromImage(CircleImage(size, Qt::yellow));

    this->setLayout(&m_layout);
    if(size>0)
        m_layout.addWidget(m_led);
    m_layout.addWidget(&m_label);
    m_led->setPixmap(m_gray);
    m_led->setMaximumSize(size, size);
    m_label.setText(name);
    m_layout.setMargin(2);
    m_layout.setSpacing(2);

    m_val=0;
    this->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
}

led::~led()
{
    delete m_green;
    delete m_gray;
    delete m_red;
    delete m_yellow;
}

void led::setValue(int val, bool resetVal)
{
    if(val==0)
        m_led->setPixmap(m_gray);
    else if(val==1)
        m_led->setPixmap(m_green);
    else if(val==2)
        m_led->setPixmap(m_red);
    else if(val==3)
        m_led->setPixmap(m_yellow);
    if(resetVal)
        m_val=val;
}

void led::blink(int val, int time)
{
    this->setValue(val);
    QTimer::singleShot(time, this, &led::stopBlink);
}

void led::stopBlink()
{
    this->setValue(m_val);
}

void led::swap(bool sw)
{
    m_layout.insertWidget(1, m_led);
}

QHBoxLayout *led::layout()
{
    return &m_layout;
}
