#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDialog>
#include <QLayout>
#include <QCheckBox>
#include <QLineEdit>
#include <QLabel>

class MainWindow;
class Options : public QDialog      //Класс, реализующий окно опций
{
    Q_OBJECT
public:
    Options(MainWindow *parent);

public slots:
    void accept();
    void reject();

protected:
    QVBoxLayout *m_mainLayout;
    QCheckBox *m_multiThread;
    QCheckBox *m_sounds;
    QLineEdit *m_calcCycle;
    QCheckBox *m_portAutoRestart;
    QCheckBox *m_nMessageIsLimited;
    MainWindow *Main;
};

#endif // OPTIONS_H
