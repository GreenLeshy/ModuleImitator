#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "Port.h"
#include <QThread>
#include <QMainWindow>
#include <QWidget>
#include <QByteArray>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QMap>
#include <QShortcut>
#include <QTime>
//#include <QSound>
#include <QMediaPlayer>

#define ADDR_AVALIABLE 64
#define CPU_PROBLEM_TIME 2500
#define CYCLE_N 20

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

enum class module_type;
class Module;
class objectModel;
class Contact;
class AIContact;
class AOContact;
class DIContact;
class DOContact;
class TabCOM;
class WidgetsTab;
class Imitation;
class MI_TableView;
class Custom;
class MainWindow : public QMainWindow
{
    Q_OBJECT

    //Переделать выбор типа?
    //Добавить настройки COM по умолчанию в ini-файл?
    //У строк неверно выставляется размер, не фиксится

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    double const PROGRAM_VERSION=1.04;
    TabCOM* COMTab(void);                                                       //Вернуть ссылку на вкладку с COM портом
    WidgetsTab* WidgetsTab(void);                                             //Вернуть ссылку на вкладку виджетами
    void addContact(Contact* newContact);                               //Добавить контакт в базу контактов
    void removeContact(Contact* delContact);                          //Удалить контакт из базы контактов
    void removeContactsImit(Imitation* Imit);                           //Удалить все кнотакты указанного модуля/объекта из базы
    void updateDiscreteContacts(void);                                      //Обновить список свободный дискретных контактов
    void updateAnalogContacts(void);                                        //Обновить список свободных аналоговых контактов (две разных функции нужны для оптимизации)
    inline Contact* contact(int i);                                                            //Вернуть ссылку на контакт (из базы контактов)
    inline Contact* contact(const  QString &name);                                //Вернуть ссылку на контакт (из базы контактов)
    bool isConnected() const;                                                       //Вернуть состояние isConnected
    int calcCycle() const;                                                                //Действие на срабатывание таймера рассчета объектов
    MI_TableView* tab(QString &name);                                         //Вызов вкладки по имени
    Module* module(QString &name);                                            //Вызов модуля по имени
    int elSize(int i);                                                                         //Узнать размер элемента sizeList[sizes i]
    const QList<Module *> &modules() const;                            //Возвращает список модулей
    const QVector<Contact *> &contacts();                                   //Возвращает список контактов
    const QVector<int> &sizeList() const;                                       //Список размеров
    bool loadFlag() const;                                                               //Флаг загрузки, равен 1, если сейчас идет загрузка
    bool isSaveConfig() const;
    void setSaveConfig(bool newSaveConf);
    bool multithread() const;
    void setMultithread(bool newMultithread);
    void setCalcCycle(int newCalcCycle);
    int cycleIndex() const;
    bool sounds() const;
    void setSounds(bool newSoundsOn);

    enum sizes: unsigned                                                                //Перечисление размеров
    {
        s_choise,
        s_name,
        s_ledD,
        s_ledA,
        s_visualA,
        s_AIWidget,
        s_AOwidth,
        s_DIWidget,
        s_DOwidth,
        s_timeout,
        s_TAM_left,
        s_TAM_midle,
        s_TAM_right
    };

    bool nMessageLimited() const;
    void setNMessageLimited(bool newNMessageLimited);
    bool autoRestart() const;
    void setAutoRestart(bool newAutoRestart);
    bool saved() const;
    void setSaved(bool newSaved);

    const QStringList &customBlocks() const;

public slots:
    void newMessageSLOT(QByteArray const &text, bool check);        //Пришло новое сообщение (+испускает newMessage)
    void answerSLOT(QByteArray const &text);                                    //Необходимо отправить ответ (+испускает answer)
    void connectedSLOT(bool const);                                         //Успешное/неуспешное подключение (+испускает connected)
    void disconnectedSLOT(void);                                                //Отключение (+испускает disconnected)
    void BConnectPressed(void);                                                 //Нажата кнопка подключения (+испускает connectToPort)
    void BDisconnectPressed(void);                                             //Нажата кнопка коннекта (+испускает disconnectFromPort)
    void moduleChangeAddr(int oldA, int newA);                       //Модуль изменил адрес
    Module* addModule(int type = 0, int addr = 0, int pos = -1);                              //Добавить новый модуль
    objectModel* addObject(int type = 0, QString name = "NN", int pos = -1);               //Добавить новый объект
    MI_TableView* addSystemTab(int type = 0, QString name = "NN", int pos = -1);//Добавить новую системную вкладку
    void moduleOpened(int i);                                                      //Открыта вкладка модуля
    void objectOpened(int i);                                                         //Открыта вкладка объекта
    void moduleChangeName(QString &name);                              //Модуль сменил имя
    void objectChangeName(QString &name);                                //Объект сменил имя
    void deleteModule(void);                                                         //Удалить модуль
    void deleteobject(void);                                                           //удалить объект
    void deleteSystem(MI_TableView *);                                         //удалить системную вкладку
    void moduleChangeType(int type = 0);                                   //Модуль сменил тип
    void objectChangeType(int type = 0);                                     //Объект сменил тип
    void contactChanged(Contact*);                                             //Изменилась информация о контакте
    void contactRenamed(Contact*);                                            //Изменилось имя контакта
    void timerSlot(void);                                                                 //Сработал таймер расчета объектов
    void savePressed(void);                                                             //Нажата кнопка сохранения
    void saveAsPressed(void);                                                        //Нажата кнопка сохранения как
    void openFilePressed(void);                                                     //Нажата кнопка загрузки
    void newProjectPressed(void);                                                  //Нажата кнопка создания нового проекта
    void problemTimerTimeout(void);                                            //Истек таймер, определяющий, что нет связи
    void baudRateChangedSLOT(QString text);                               //Слоты для конфигурации COM-порта, вызываются виджетами вкладки COM
    void dataBitsChangedSLOT(QString text);                                 //Слоты для конфигурации COM-порта, вызываются виджетами вкладки COM
    void parityChangedSLOT(QString text);                                     //Слоты для конфигурации COM-порта, вызываются виджетами вкладки COM
    void stopBitsChangedSLOT(QString text);                                 //Слоты для конфигурации COM-порта, вызываются виджетами вкладки COM
    void readTimeoutChangedSLOT(QString text);                          //Слоты для конфигурации COM-порта, вызываются виджетами вкладки COM
    void writeTimeoutChangedSLOT(QString text);                         //Слоты для конфигурации COM-порта, вызываются виджетами вкладки COM
    void optionsPressed(void);                                                         //Пользователь вызвал меню параметров
    void currentCOMChaneged(void);                                              //Пользователь переключил COM-порт
    void playOnOff(void);                                                                 //Воспроизвести свук срабатывания конечника
    void gateStartWorking(int n=1);                                                 //n = количество клапанов, которые начали работать, если закончили, то n<0
    void closeEvent(QCloseEvent *ev);                                              //Переопределяет closeEvent
    void showBadBoundTry(void);                                                    //Показывает сообщение о том, что связываение не произошло из-за повторяющихся имен
    void hideBadBoundTry(void);                                                    //Скрывает сообщение о том, что связываение не произошло из-за повторяющихся имен
    void resize(void);                                                                          //Обновляет минимальный размер окна
    void saveCustom(Custom *module);                                            //Сохраняет Custom-блок в файл

signals:
    void connectToPort(QString const &);                                      //Необходимо подключиться (связывается с connectToPort порта)
    void disconnectFromPort();                                                   //Необходимо отключиться (связывается с disconnectFromPort порта)
    void connected(bool const);                                                 //Успешное/неуспешное подключение (связывается с connected модулей)
    void disconnected(void);                                                       //Отключение (связывается с disconnected модулей)
    void newMessage(QByteArray const &M, bool const);                 //Пришло новое сообщение  (связывается с newMessage модулей)
    void answer(QByteArray const &text);                                            //Необходимо отправить ответ (связывается с write порта)
    void addrListUpdated(QVector<bool>);                                    //Изменился список свободных адресов
    void DIContactsUpdated(QStringList &list);                            //Обновился список контактов
    void DOContactsUpdated(QStringList &list);                           //Обновился список контактов
    void AIContactsUpdated(QStringList &list);                            //Обновился список контактов
    void AOContactsUpdated(QStringList &list);                           //Обновился список контактов
    void timerSignal(int calcCycle, int cycleIndex);                      //Сработал таймер расчета объектов
    void communicationProblem(int val);                                   //val=0, если нет проблем; val=1, если неверный CRC; val=2, если нет сообщений в течении CPU_PROBLEM_TIME мс
    void baudRateChanged(QString text);                                  //Сигналы для конфигурации COM-порта
    void dataBitsChanged(QString text);                                     //Сигналы для конфигурации COM-порта
    void parityChanged(QString text);                                        //Сигналы для конфигурации COM-порта
    void stopBitsChanged(QString text);                                     //Сигналы для конфигурации COM-порта
    void readTimeoutChanged(int val);                                       //Сигналы для конфигурации COM-порта
    void writeTimeoutChanged(int val);                                       //Сигналы для конфигурации COM-порта


private:
    Module* module(int i);                                                        //Вызов модуля по номеру
    objectModel* object(int i);                                                           //Вызов объекта по номеру
    MI_TableView* systemTab(int i);                                             //Вызов системной вкладки по номеру
    MI_TableView* systemTab(QString name);                             //Вызов системной вкладки по имени
    void addTabModule(Module *newTab, int pos = -1);           //Добавить вкладку модуля
    void addTabobject(objectModel *newTab, int pos = -1);             //Добавить вкладку объекта
    void addTabSystem(MI_TableView *newTab, int pos = -1);      //Добавить системную вкладку
    void deleteModuleFromLists(int i);                                          //Удалить модуль из базы
    void deleteobjectFromLists(int i);                                            //Удалить объект из базы
    void deleteSystemFromLists(int i);                                           //Удались системную вкладку из базы
    void save(QString &fileName);                                                    //Сохранить в файл
    void load(QString &fileName);                                                    //Загрузить из файла
    void readConfig(void);                                                             //Прочитать файл конфигурации
    void updateLastProject(QString &fileName);                              //Обновить последний проект
    void deleteAll(void);                                                                  //Удалить все модули и объекты
    void iniPort(void);                                                                     //Инициализация COM-порта
    void deletePort(void);                                                               //Удалить вкладку COM-порта, закрыть порт
    void writeConfig(void);                                                             //Написать/обновить файл конфигурации
    void updateIndex(void);                                                            //Узнать свой индекс и написать его в indexes.txt
    void deleteIndex(void);                                                              //Удалить свой индекс из indexes.txt
    void updateNameList(void);                                                      //Обновить список имен контактов
    void updateCustomList(void);                                                    //Найти файлы Custom-блоков
    Custom* loadCustomBlock(QString className, int pos=-1, QString name="");              //Загрузить Custom-блок
    void sendAllContacts(void);                                                         //Принудительная отправка списков контактов

    QThread *m_pThread=nullptr;                                                    //Поток с портом
    Port *m_port=nullptr;                                                                 //COM порт
    QList<Module*> m_modules;                                                   //Список модулей
    QMap <QString, int> m_modulesNameList;                               //Список соответствия имени модуля и его номера
    QList<objectModel*> m_objects;                                               //Список объектов
    QMap <QString, int> m_objectsNameList;                                 //Список соответствия имени объекта и его номера
    QList<MI_TableView*> m_systemTabs;                                       //Список модулей
    QMap <QString, int> m_systemTabsNameList;                           //Список соответствия имени модуля и его номера
    QString m_currentComm;                                                            //Имя используемого COM-порта
    bool m_isConnected=0;                                                               //Подключен ли
    QTabWidget *m_modulesTabWidget;                                          //TabWidget с модулями
    QTabWidget *m_objectsTabWidget;                                             //TabWidget с объектами
    QTabWidget *m_systemTabWidget;                                              //TabWidget с системными вкладками
    Ui::MainWindow *ui;                                                                     //ui
    QVector<bool> m_addresses;                                                       //Список адресов (1 - занят, 0 - свободен)
    QVector<Contact*> m_contacts;                                                    //Список контактов
    QMap<QString, int> m_contactsNameList;                                   //Список соответствия имени контакта и его номера
    int m_calcCycle=100;                                                                    //Период расчета объектов в мс
    QString m_fileName="";                                                               //Последний сохраненный/загруженный файл
    QTimer *m_problemTimer;                                                            //Таймер, отслеживающий проблемы со связью
    bool m_communicationProblem=0;                                               //Флаг проблем со связью
    bool m_loadFlag=0;                                                                       //Флаг загрузки
    bool m_dedablerAnswer=0;                                                           //Флаг для избежания дублирования ответов на слипшиеся запросы
    QVector<int> m_sizeList;                                                               //Список размеров графических элементов
    QShortcut *keyCtrlS;                                                                      //Шорткаты для действий в меню
    QShortcut *keyShiftCtrlS;                                                               //Шорткаты для действий в меню
    QShortcut *keyCtrlO;                                                                      //Шорткаты для действий в меню
    QShortcut *keyCtrlN;                                                                      //Шорткаты для действий в меню
    bool m_multithread=true;                                                                 //Флаг многопоточности, равен 1, если программа в многопоточном режиме
    bool m_saveConfig=1;                                                                    //Сохранять ли конфигурацию при выключении приложения
    int m_index=-1;                                                                               //Индекс приложения, в норме должен быть от 0 до 9
    QString m_config="MI_config.txt";                                                  //Базовое имя файла конфигурации
    int m_cycleIndex=0;                                                                         //Количество просчитанных циклов со старта программы
    int m_lastTime;                                                                                 //Системное время в момент начала просчета предыдущего цикла
    int m_cycleTimes[CYCLE_N]={0};                                                       //Массив с реальными аременами расчета цикла
    TabCOM* m_COM=nullptr;                                                                //Для оптимизации, чтобы не вызывать tab() так часто
    QMediaPlayer* sound_OnOff=nullptr;                                              //Звук срабатывания конечника
    QMediaPlayer* sound_inWork=nullptr;                                             //Звук электромотора
    int m_gateInWork=0;                                                                        //Количество работающих сейчас электромоторов
    bool m_soundsOn=false;                                                                  //Использовать ли звуки
    bool m_nMessageLimited=true;                                                        //Ограничено ли число сообщений, которое помещается в окно сообщений (при 0 может вызвать переполнение памяти)
    bool m_autoRestart=false;                                                                 //Перезапускать ли порт при зависании
    bool m_saved=true;                                                                           //Изменения соединений проекта сохранены
    int m_reservedContactsCount=0;                                                       //Количество "зарезервированных" контактов
    QStringList m_customBlocks;                                                             //Список найденных custom-блоков
    QStringList m_lastDIs;                                                                        //Последие отправленные списки контактов, помогают предотвратить повторные отправки при загрузке
    QStringList m_lastDOs;
    QStringList m_lastAIs;
    QStringList m_lastAOs;
};

//inline
Contact *MainWindow::contact(int i)
{
    return m_contacts.at(i);
}

Contact *MainWindow::contact(const QString &name)
{
    if(!m_contactsNameList.contains(name))
        return nullptr;
    return contact(m_contactsNameList.value(name));
}


#endif // MAINWINDOW_H
