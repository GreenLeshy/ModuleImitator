#ifndef SYSTEMTABS_H
#define SYSTEMTABS_H
#include <QStringList>
#include "TabCOM.h"
#include "TabAllModules.h"
#include "WidgetsTab.h"

class SystemTabs               //Объединяет все системные вкладки в одну библиотеку и реализует фабрику объектов
{
public:
    SystemTabs();

    enum sysTab_types: unsigned        //Обязательно должен быть согласован по количеству и порядку элементов с typeNames
    {
        NO_DEFINED= 0,
        TabCOM,
        TabAllModules,
        WidgetsTab
    };

    static QStringList getTypeNames();
    static QString getTypeName(int i);
    static MI_TableView *newSysTab(QWidget *parent=nullptr, int type=0, QString name="");

private:
    static const QStringList typeNames;
};

#endif // SYSTEMTABS_H
