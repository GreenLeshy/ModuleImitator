#include "TabCOM.h"
#include <QSerialPortInfo>
#include "mainwindow.h"
#include <QDebug>
#include <QList>
#include <QDialog>
#include <QDialogButtonBox>
#include <QLabel>

TabCOM::TabCOM(QWidget *parent, QString &name) : MI_TableView(parent, name)
{
    m_text = new QPlainTextEdit(this);
    //m_text->setMaximumBlockCount(2000);
    setIsLimited(Main->nMessageLimited());
    m_COMchoose = new QComboBox(this);
    m_BConnect = new QPushButton(this);
    m_BDisconnect = new QPushButton(this);
    m_mainLayuot->addWidget(m_text);
    m_mainLayuot->addWidget(m_COMchoose);
    m_BConnect->setText("Connect");
    m_BDisconnect->setText("Disconnect");
    m_mainLayuot->addWidget(m_BConnect);
    m_mainLayuot->addWidget(m_BDisconnect);
    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    for(int i=0; i<ports.size(); ++i)
    {
        m_COMchoose->addItem(ports.at(i).portName());
    }
    m_BDisconnect->setEnabled(false);

    m_BaudRate=new QComboBox(this);
    m_BaudRate->addItems(m_BaudVariants);
    m_DataBits=new QComboBox(this);
    m_DataBits->addItems(m_DataVariants);
    m_Parity=new QComboBox(this);
    m_Parity->addItems(m_ParityVariants);
    m_StopBits=new QComboBox(this);
    m_StopBits->addItems(m_StopVariants);

    m_BaudRate->setCurrentText("Baud:115200");
    m_DataBits->setCurrentText("8 data bits");
    m_Parity->setCurrentText("OddParity");
    m_StopBits->setCurrentText("2 stop bits");

    m_auto=new QPushButton("Определить автоматически", this);

    QHBoxLayout* L = new QHBoxLayout;
    L->addWidget(m_BaudRate);
    L->addWidget(m_DataBits);
    L->addWidget(m_Parity);
    L->addWidget(m_StopBits);
    m_mainLayuot->addLayout(L);


    m_readTimeout =new QLineEdit("3", this);
    m_readTimeout->setMaximumWidth(Main->elSize(MainWindow::s_timeout));
    m_readTimeoutDescription =new QLabel("Таймаут при чтении, мс:", this);
    m_writeTimeout =new QLineEdit("50", this);
    m_writeTimeout->setMaximumWidth(Main->elSize(MainWindow::s_timeout));
    m_writeTimeoutDescription =new QLabel("Таймаут при записи, мс:", this);
    L = new QHBoxLayout;
    L->addWidget(m_readTimeoutDescription);
    L->addWidget(m_readTimeout);
    L->addWidget(m_writeTimeoutDescription);
    L->addWidget(m_writeTimeout);
    m_mainLayuot->addLayout(L);

    L = new QHBoxLayout;
    L->addWidget(m_auto);
    m_mainLayuot->addLayout(L);

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    connect(m_BConnect, &QPushButton::pressed, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::BConnectPressed);
    connect(m_BDisconnect, &QPushButton::pressed, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::BDisconnectPressed);
    connect(m_BaudRate, &QComboBox::currentTextChanged, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::baudRateChangedSLOT);
    connect(m_Parity, &QComboBox::currentTextChanged, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::parityChangedSLOT);
    connect(m_DataBits, &QComboBox::currentTextChanged, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::dataBitsChangedSLOT);
    connect(m_StopBits, &QComboBox::currentTextChanged, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::stopBitsChangedSLOT);
    connect(m_readTimeout, &QLineEdit::textChanged, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::readTimeoutChangedSLOT);
    connect(m_writeTimeout, &QLineEdit::textChanged, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::writeTimeoutChangedSLOT);
    connect(m_COMchoose, &QComboBox::currentTextChanged, dynamic_cast<MainWindow*>(this->parent()), &MainWindow::currentCOMChaneged);
    connect(m_auto, &QPushButton::pressed, this, &TabCOM::BAutoPressed);

}

QPlainTextEdit *TabCOM::text() const
{
    return m_text;
}

void TabCOM::setText(QPlainTextEdit *newText)
{
    m_text = newText;
}

QComboBox *TabCOM::COMchoose() const
{
    return m_COMchoose;
}

void TabCOM::setCOMchoose(QComboBox *newCOMchoose)
{
    m_COMchoose = newCOMchoose;
}

QPushButton *TabCOM::BConnect() const
{
    return m_BConnect;
}

void TabCOM::setBConnect(QPushButton *newBConnect)
{
    m_BConnect = newBConnect;
}

QPushButton *TabCOM::BDisconnect() const
{
    return m_BDisconnect;
}

void TabCOM::setBDisconnect(QPushButton *newBDisconnect)
{
    m_BDisconnect = newBDisconnect;
}

QJsonObject TabCOM::toJson()
{
    QJsonObject obj=this->MI_TableView::toJson();
    obj["type"]="TabCOM";
    obj["COM"]=m_COMchoose->currentText();
    obj["BaudRate"]=baudRate();
    obj["DataBits"]=dataBits();
    obj["Parity"]=parity();
    obj["StopBits"]=stopBits();
    obj["writeTimeout"]=m_writeTimeout->text();
    obj["readTimeout"]=m_readTimeout->text();
    return obj;
}

void TabCOM::fromJson(const QJsonObject &obj)
{
    this->MI_TableView::fromJson(obj);
    m_COMchoose->setCurrentText(obj["COM"].toString());
    m_BaudRate->setCurrentText(obj["BaudRate"].toString());
    m_DataBits->setCurrentText(obj["DataBits"].toString());
    m_Parity->setCurrentText(obj["Parity"].toString());
    m_StopBits->setCurrentText(obj["StopBits"].toString());
    m_writeTimeout->setText(obj["writeTimeout"].toString());
    m_readTimeout->setText(obj["readTimeout"].toString());
}

QString TabCOM::baudRate()
{
    return m_BaudRate->currentText();
}

QString TabCOM::dataBits()
{
    return m_DataBits->currentText();
}

QString TabCOM::parity()
{
    return m_Parity->currentText();
}

QString TabCOM::stopBits()
{
    return m_StopBits->currentText();
}

void TabCOM::write(const QString &text)
{
    ////qDebug() << text;
    m_text->appendPlainText(text);
}

QString TabCOM::CurrentCOM() const
{
    return m_COMchoose->currentText();
}

void TabCOM::connected(bool val)
{
    if(val)
    {
        m_BConnect->setEnabled(false);
        m_BDisconnect->setEnabled(true);
    }
}

void TabCOM::disconnected()
{
    m_BConnect->setEnabled(true);
    m_BDisconnect->setEnabled(false);
}

void TabCOM::newMessage(const QByteArray &text, bool check)
{
    if(!check)
        ++m_nWrongMessages;
    ++m_nMessages;
    if(m_nWrongMessages>10||m_nMessages>20)
        writePoints();
}

void TabCOM::BAutoPressed()
{
    if(!m_isTuning)
    {
        QDialog dialog(this);
        dialog.setWindowTitle("Выберите диапазон поиска:");
        dialog.setWindowFlags(dialog.windowFlags()&~Qt::WindowContextHelpButtonHint);
        QVBoxLayout M;
        dialog.setLayout(&M);
        QHBoxLayout *H;
        H=new QHBoxLayout;
        H->addWidget(new QLabel("С:", &dialog));
        H->addWidget(new QLabel("По:", &dialog));
        M.addLayout(H);
        QComboBox B1(this);
        B1.addItems(m_BaudVariants);
        QComboBox B2(this);
        B2.addItems(m_BaudVariants);
        B2.setCurrentIndex(B2.count()-1);
        H=new QHBoxLayout;
        H->addWidget(&B1);
        H->addWidget(&B2);
        M.addLayout(H);
        QComboBox D1(this);
        D1.addItems(m_DataVariants);
        QComboBox D2(this);
        D2.addItems(m_DataVariants);
        D2.setCurrentIndex(D2.count()-1);
        H=new QHBoxLayout;
        H->addWidget(&D1);
        H->addWidget(&D2);
        M.addLayout(H);
        QComboBox P1(this);
        P1.addItems(m_ParityVariants);
        QComboBox P2(this);
        P2.addItems(m_ParityVariants);
        P2.setCurrentIndex(P2.count()-1);
        H=new QHBoxLayout;
        H->addWidget(&P1);
        H->addWidget(&P2);
        M.addLayout(H);
        QComboBox S1(this);
        S1.addItems(m_StopVariants);
        QComboBox S2(this);
        S2.addItems(m_StopVariants);
        S2.setCurrentIndex(S2.count()-1);
        H=new QHBoxLayout;
        H->addWidget(&S1);
        H->addWidget(&S2);
        M.addLayout(H);

        QDialogButtonBox buttonBox(QDialogButtonBox::StandardButton::Ok | QDialogButtonBox::Cancel, &dialog);
        connect(&buttonBox, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
        connect(&buttonBox, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);
        M.addWidget(&buttonBox);
        dialog.setMinimumSize(M.minimumSize());
        dialog.setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        //dialog.setModal(true);
        if(dialog.exec())
        {
            n_B1 = B1.currentIndex();
            n_B2 = B2.currentIndex();
            n_D1 = D1.currentIndex();
            n_D2 = D2.currentIndex();
            n_P1 = P1.currentIndex();
            n_P2 = P2.currentIndex();
            n_S1 = S1.currentIndex();
            n_S2 = S2.currentIndex();

            m_COMchoose->setEnabled(false);
            m_BaudRate->setEnabled(false);
            m_StopBits->setEnabled(false);
            m_Parity->setEnabled(false);
            m_DataBits->setEnabled(false);
            if(!Main->isConnected())
                m_BConnect->click();
            m_BConnect->setEnabled(false);
            m_BDisconnect->setEnabled(false);
            m_writeTimeout->setEnabled(false);
            m_readTimeout->setEnabled(false);
            m_isTuning=true;
            m_points.clear();
            for(int ib=0; ib<m_BaudRate->count(); ++ib)
            {
                m_points.append(new QVector<QVector<QVector<QVector<int>*>*>*>);
                for(int id=0; id<m_DataBits->count(); ++id)
                {
                    m_points[ib]->append(new QVector<QVector<QVector<int>*>*>);
                    for(int ip=0; ip<m_Parity->count(); ++ip)
                    {
                        m_points.at(ib)->at(id)->append(new QVector<QVector<int>*>);
                        for(int is=0; is<m_StopBits->count(); ++is)
                        {
                            m_points.at(ib)->at(id)->at(ip)->append(new QVector<int>);
                            for(int ir=0; ir<6; ++ir)
                            {
                                m_points.at(ib)->at(id)->at(ip)->at(is)->append(0);
                            }
                        }
                    }
                }
            }
            m_BaudRate->setCurrentIndex(n_B1);
            m_DataBits->setCurrentIndex(n_D1);
            m_Parity->setCurrentIndex(n_P1);
            m_StopBits->setCurrentIndex(n_S1);
            m_readTimeout->setText(QString::number(c_timeStep));
            connect(Main, &MainWindow::newMessage, this, &TabCOM::newMessage);
    }
    }
    else
    {
        m_COMchoose->setEnabled(true);
        m_BaudRate->setEnabled(true);
        m_StopBits->setEnabled(true);
        m_Parity->setEnabled(true);
        m_DataBits->setEnabled(true);
        m_BConnect->setEnabled(true);
        m_BDisconnect->setEnabled(true);
        m_writeTimeout->setEnabled(true);
        m_readTimeout->setEnabled(true);
        m_isTuning=false;
        disconnect(Main, &MainWindow::newMessage, this, &TabCOM::newMessage);
    }
}

void TabCOM::writePoints()
{
    int ib = m_BaudRate->currentIndex();
    int id = m_DataBits->currentIndex();
    int ip = m_Parity->currentIndex();
    int is = m_StopBits->currentIndex();
    int ir = m_readTimeout->text().toInt()/c_timeStep;
    (*m_points.at(ib)->at(id)->at(ip)->at(is))[ir]=m_nMessages-m_nWrongMessages;
    m_nMessages=0;
    m_nWrongMessages=0;

    ++ir;
    if(ir>5)
    {
        ir=1;
        ++is;
        if(is>n_S2)
        {
            is=n_S1;
            ++ip;
            if(ip>n_P2)
            {
                ip=n_P1;
                ++id;
                if(id>n_D2)
                {
                    id=n_D1;
                    ++ib;
                    if(ib>n_B2)
                    {
                        pointsWrited();
                        return;
                    }
                }
            }
        }
    }
    m_BaudRate->setCurrentIndex(ib);
    m_DataBits->setCurrentIndex(id);
    m_Parity->setCurrentIndex(ip);
    m_StopBits->setCurrentIndex(is);
    m_readTimeout->setText(QString::number(ir*c_timeStep));
}

void TabCOM::pointsWrited()
{
    int buf = 0;
    int bib=0;
    int bid=0;
    int bip=0;
    int bis=0;
    int bir=1;
    for(int ib=0; ib<m_BaudRate->count(); ++ib)
        for(int id=0; id<m_DataBits->count(); ++id)
            for(int ip=0; ip<m_Parity->count(); ++ip)
                for(int is=0; is<m_StopBits->count(); ++is)
                    for(int ir=1; ir<6; ++ir)
                    {
                        if(m_points.at(ib)->at(id)->at(ip)->at(is)->at(ir)>buf)
                        {
                            buf=m_points.at(ib)->at(id)->at(ip)->at(is)->at(ir);
                            bib=ib;
                            bid=id;
                            bip=ip;
                            bis=is;
                            bir=ir;
                        }
                    }
    m_BaudRate->setCurrentIndex(bib);
    m_DataBits->setCurrentIndex(bid);
    m_Parity->setCurrentIndex(bip);
    m_StopBits->setCurrentIndex(bis);
    m_readTimeout->setText(QString::number(bir*c_timeStep));
    for(int ib=0; ib<m_BaudRate->count(); ++ib)
    {
        for(int id=0; id<m_DataBits->count(); ++id)
        {
            for(int ip=0; ip<m_Parity->count(); ++ip)
            {
                /*for(int is=0; is<m_StopBits->count(); is++)
                {
                    //qDeleteAll(*m_points.at(ib)->at(id)->at(ip)->at(is));
                    m_points.at(ib)->at(id)->at(ip)->at(is)->clear();
                }*/
                qDeleteAll(*m_points.at(ib)->at(id)->at(ip));
            }
            qDeleteAll(*m_points.at(ib)->at(id));
        }
        qDeleteAll(*m_points.at(ib));
    }
    qDeleteAll(m_points);
    m_points.clear();
    BAutoPressed();

}

bool TabCOM::isLimited() const
{
    return m_isLimited;
}

void TabCOM::setIsLimited(bool newIsLimited)
{
    m_isLimited = newIsLimited;
    if(m_isLimited)
        m_text->setMaximumBlockCount(2000);
    else
        m_text->setMaximumBlockCount(0);
}


