#include "TabAllModules.h"
#include "mainwindow.h"
#include "Modules.h"
#include "static.h"
#include "led.h"

TabAllModules::TabAllModules(QWidget *parent, QString name) : MI_TableView(parent, name)
{
    for(int i=0; i<ADDR_AVALIABLE; ++i)
    {
        m_projectModules.append(false);
        m_CPUModules.append(false);
        m_activeModules.append(false);
    }

    connect(Main, &MainWindow::newMessage, this, &TabAllModules::newMessage);
    connect(Main, &MainWindow::answer, this, &TabAllModules::answer);

    counterOut();

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

void TabAllModules::newMessage(const QByteArray &text, bool check)
{
    if(check)
    {
        if(text.at(1)==3)   //Проверка только на запросах на чтение
        {
            if(text.size()==8)  //Запрос от CPU
                {
                m_CPUModules[text[0]]=true;
                ++m_counter;
                if(m_counter>m_counterMax)
                    counterOut();
                }
            else if(text.size()==13)    //Ответ от модуля
                {
                m_activeModules[text[0]]=true;
                ++m_counter;
                if(m_counter>m_counterMax)
                    counterOut();
                }
        }
    }
}

void TabAllModules::answer(const QByteArray &text)
{
    m_activeModules[text.at(0)]=true;
    ++m_counter;
    if(m_counter>m_counterMax)
        counterOut();
}

void TabAllModules::counterOut()
{
    m_counter=0;
    Static::clearLayout(m_mainLayuot, 1);
    //m_pseudoParent = new QWidget(this);
    QList<Module*> modules = Main->modules();
    for(int i=0; i<modules.size()-1; ++i)
    {
        m_projectModules[modules.at(i)->address()]=true;
    }
    for(int i=0; i<ADDR_AVALIABLE; ++i)
    {
        if(m_projectModules.at(i)||m_CPUModules.at(i)||m_activeModules.at(i))
        {
            QHBoxLayout *L = new QHBoxLayout;
            QString buf = QString::number(i).append(":");
            QLabel* Num = new QLabel(buf, this);
            led* PM = new led(this, "Есть в проекте", 15);
            led* CPUM = new led(this, "Запрашивается CPU", 15);
            led* AM = new led(this, "Активен", 15);
            L->addWidget(Num);
            L->addWidget(PM);
            L->addWidget(CPUM);
            L->addWidget(AM);
            m_mainLayuot->addLayout(L);
            if(m_projectModules.at(i))
                PM->setValue(1);
            if(m_CPUModules.at(i))
                CPUM->setValue(1);
            if(m_activeModules.at(i))
                AM->setValue(1);
        }
        m_projectModules[i]=false;
        m_CPUModules[i]=false;
        m_activeModules[i]=false;
    }
    m_mainLayuot->addStretch(10);
}
