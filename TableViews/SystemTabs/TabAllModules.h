#ifndef TABALLMODULES_H
#define TABALLMODULES_H

#include <MI_TableView.h>

class led;
class TabAllModules : public MI_TableView               //Вкладка с информацией о модулях в проекте
{
public:
    TabAllModules(QWidget *parent, QString name);

public slots:
    void newMessage(QByteArray const &text, bool check);   //Пришло новое сообщение
    void answer(QByteArray const &);                            //Модуль ответил на запрос

protected:
    void counterOut();                                                  //Счетчик переполнился


    int m_counter=0;                                //Счетчик
    int m_counterMax=500;                      //Максимальное значение счетчика
    QList<bool> m_projectModules;         //Модули в проекте ModuleImitator
    QList<bool> m_CPUModules;             //Модули, запрашиваемые CPU
    QList<bool> m_activeModules;           //Работающие модули CPU
    QList<led*> m_projectModulesLed;
    QList<led*> m_CPUModulesLed;
    QList<led*> m_activeModulesLed;
};

#endif // TABALLMODULES_H
