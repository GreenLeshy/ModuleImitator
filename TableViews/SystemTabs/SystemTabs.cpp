#include "SystemTabs.h"

SystemTabs::SystemTabs()
{

}

QStringList SystemTabs::getTypeNames()
{
    return typeNames;
}

QString SystemTabs::getTypeName(int i)
{
    return typeNames.at(i);
}

MI_TableView *SystemTabs::newSysTab(QWidget *parent, int type, QString name)
{
    MI_TableView* newTab;
    QString objName=name;
    if(objName=="")
        objName=typeNames.at(type);
    switch (type)
    {
    case int(SystemTabs::NO_DEFINED):
        newTab = new MI_TableView(parent, objName);
    break;
    case int(SystemTabs::TabCOM):
        newTab = new class TabCOM(parent, objName);
    break;
    case int(SystemTabs::TabAllModules):
        newTab = new class TabAllModules(parent, objName);
    break;
    case int(SystemTabs::WidgetsTab):
        newTab = new class WidgetsTab(parent, objName);
    break;
    default:
        return nullptr;
    break;
    }
    return newTab;
}

const QStringList SystemTabs::typeNames =         //Обязательно должен быть согласован по количеству и порядку элементов с sysTab_types
{
    "TabCOM",
    "TabAllModules",
    "WidgetsTab"
};
