#include "WidgetsTab.h"
#include "mainwindow.h"
#include "AImini.h"
#include "DIMini.h"
#include "Contacts.h"
#include "IOWidgets.h"
#include "IOs.h"

WidgetsTab::WidgetsTab(QWidget *parent, QString name) : MI_TableView(parent, name)
{
    m_area = new QScrollArea(this);
    QWidget *w = new QWidget(m_area);
    m_area->setWidget(w);
    m_area->setWidgetResizable(true);
    m_layout = new QVBoxLayout(w);
    m_mainLayuot->addWidget(m_area);
    m_area->setBackgroundRole(QPalette::Light);
}

void WidgetsTab::insert(Contact *c, IOmini *t)
{
    auto List = Main->contacts();
    int j=0;
    int n = List.size();
    for(int i=0; i<n; ++i)
    {
        if(List.at(i)==c)
        {
            m_contacts.insert(j, c);
            m_layout->insertLayout(j, t->getLayout());
            m_miniList.insert(j, t);
            return;
        }
        else if(m_contacts.contains(List.at(i)))
            ++j;
    }
}

void WidgetsTab::updateIOmini(Contact *c)
{
    deleteContact(c);
    IOClass *p=c->getIO();
    QString name = p->name().toLower();
    if(name!="резерв")
        if(c->IOType()==IO_type::Input)
        {
            if(c->pair()==nullptr)
            {
                if(p->widget()!=nullptr)
                {
                    QString type = c->widgetType();
                    if(AIWidgets::getTypeNames().contains(type)||DIWidgets::getTypeNames().contains(type))
                    {
                        IOmini *t;
                        if(p->contact()->dataType()==data_type::Analog)
                            t=new AIMini(p);
                        else
                            t=new DIMini(p);
                        insert(c, t);
                    }
                }
            }
        }
}

void WidgetsTab::deleteContact(Contact *c)
{
    if(m_contacts.contains(c))
    {
        int n=m_contacts.indexOf(c);
        m_contacts.removeAt(n);
        m_miniList.at(n)->deleteLater();
        m_miniList.removeAt(n);
    }
}

void WidgetsTab::clear()
{
    for(int i=m_miniList.size()-1; i>-1; --i)
        deleteContact(m_contacts.at(i));
}
