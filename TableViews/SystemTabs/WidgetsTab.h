#ifndef WIDGETSTAB_H
#define WIDGETSTAB_H

#include "MI_TableView.h"
#include <QScrollArea>

class IOClass;
class IOmini;
class Contact;
class WidgetsTab : public MI_TableView
{
    Q_OBJECT
public:
    WidgetsTab(QWidget *parent, QString name);

    void updateIOmini(Contact *c);          //Обновить статус указанного контакта
    void deleteContact(Contact *c);         //Удалить IOmini связанный с контактом и удалить их из списков
    void clear(void);                                  //Удалить все IOmini

protected:
    void insert(Contact *c, IOmini *t);        //Вставить t связанный с c на виджет и в списки

    QList<IOmini *> m_miniList;                 //Список IOmini
    QList<Contact *> m_contacts;              //Список контактов
    QScrollArea *m_area;                            //Виджет для отображения IOmini
    QVBoxLayout *m_layout;
};

#endif // WIDGETSTAB_H
