#ifndef TABCOM_H
#define TABCOM_H

#include "MI_TableView.h"
#include <QPlainTextEdit>

class TabCOM : public MI_TableView      //Вкладка с информацией о COM-соединении
{
public:
    TabCOM(QWidget *parent, QString &name);
    QPlainTextEdit *text() const;
    void setText(QPlainTextEdit *newText);
    QComboBox *COMchoose() const;
    void setCOMchoose(QComboBox *newCOMchoose);
    QPushButton *BConnect() const;
    void setBConnect(QPushButton *newBConnect);
    QPushButton *BDisconnect() const;
    void setBDisconnect(QPushButton *newBDisconnect);

    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject  &obj) override;    //Загружает информацию об элементе из QJsonObject

    const int c_timeStep=3;                                             //Шаг, с которым меняется таймаут при подборе оптимальных настроек
    QString baudRate(void);
    QString dataBits(void);
    QString parity(void);
    QString stopBits(void);

    void write(QString const &text);                               //Написать text в текстовое поле
    QString CurrentCOM() const;

    bool isLimited() const;
    void setIsLimited(bool newIsLimited);

public slots:
    virtual void connected(bool val) override;
    virtual void disconnected(void) override;
    void newMessage(QByteArray const &text, bool check);   //Пришло новое сообщение
    void BAutoPressed(void);                                //Нажата кнопка автоматического подбора настроек com-порта


private:
    void writePoints();                                             //Записать рейтинг (количество удачно принятых посылок) для текущей конфигурации
    void pointsWrited();                                          //Все конфигурации оценены

    QPlainTextEdit *m_text;                                     //Виджет для отображения сообщений
    QComboBox *m_COMchoose;                          //Виджет для выбора COM-порта
    QPushButton *m_BConnect;                             //Кнопка коннекта
    QPushButton *m_BDisconnect;                         //Кнопка дисконнекта
    QComboBox *m_BaudRate;                              //Виджет для выбора скорости COM-порта
    QComboBox *m_DataBits;                                 //Виджет для количества бит в посылке COM-порта
    QComboBox *m_Parity;                                     //Виджет для выбора паритета COM-порта
    QComboBox *m_StopBits;                                  //Виджет для выбора количества стоп-бит COM-порта
    QLineEdit *m_readTimeout;                               //Виджет для выбора таймаута на запись
    QLabel  *m_readTimeoutDescription;                 //Пояснение к m_readTimeout
    QLineEdit *m_writeTimeout;                               //Виджет для выбора таймаута на чтение
    QLabel  *m_writeTimeoutDescription;                 //Пояснение к m_writeTimeout
    QPushButton *m_auto;                                       //Кнопка автоподбора параметров COM-порта
    bool m_isTuning=0;                                            //Флаг автоподбора, если 1, значит сейчас идет подбор
    QVector<QVector<QVector<QVector<QVector<int>*>*>*>*> m_points;    //Список рейтинга для всех конфигураций
    int m_nMessages=0;                                           //Количество принятых сообщений на данной конфигурации
    int m_nWrongMessages=0;                                 //Количество неверно принятых сообщений на данной конфигурации
    QStringList m_BaudVariants={"Baud:1200", "Baud:2400", "Baud:4800", "Baud:9600", "Baud:19200", "Baud:38400", "Baud:57600", "Baud:115200"};
    QStringList m_DataVariants={"5 data bits", "6 data bits", "7 data bits", "8 data bits"};
    QStringList m_ParityVariants={"NoParity", "EvenParity", "OddParity", "SpaceParity", "MarkParity"};
    QStringList m_StopVariants={"1 stop bit", "2 stop bits"};
    int n_B1, n_B2, n_D1, n_D2, n_P1, n_P2, n_S1, n_S2; //Границы поиска при автоподборе
    bool m_isLimited=1;                                             //Ограничено ли число сообщений, которое помещается в окно сообщений (при 0 может вызвать переполнение памяти)

};

#endif // TABCOM_H
