#include "MI_TableView.h"
#include "mainwindow.h"

MI_TableView::MI_TableView(QWidget *parent, QString name) : QTableView(parent)
{
    this->setParent(parent);
    m_name = name;
    m_mainLayuot = new QVBoxLayout(this);
    Main = dynamic_cast<MainWindow*>(parent);

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    connect(Main, &MainWindow::connected, this, &MI_TableView::connected);
    connect(Main, &MainWindow::disconnected, this, &MI_TableView::disconnected);
}

const QString MI_TableView::name() const
{
    return m_name;
}

void MI_TableView::setName(const QString &newName)
{
    m_name = newName;
}

QVBoxLayout *MI_TableView::mainLayuot() const
{
    return m_mainLayuot;
}

void MI_TableView::setMainLayuot(QVBoxLayout *newMainLayuot)
{
    m_mainLayuot = newMainLayuot;
}

void MI_TableView::connected(bool val)
{

}

void MI_TableView::disconnected()
{

}

void MI_TableView::communicationProblem(int isProblem)
{

}

MainWindow *MI_TableView::getMain() const
{
    return Main;
}

QJsonObject MI_TableView::toJson()
{
    QJsonObject obj;
    obj["name"]=m_name;
    return obj;
}

void MI_TableView::fromJson(const QJsonObject &jsn)
{
    m_name = jsn["name"].toString();
}
