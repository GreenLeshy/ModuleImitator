#include "Imitation.h"
#include "mainwindow.h"
#include "IOs.h"
#include "Modules.h"
#include "Objects.h"
#include "QMessageBox"
#include "static.h"

Imitation::Imitation(QWidget *parent, QString name) : MI_TableView(parent, name)
{
    m_typeBox = new QComboBox(this);
    m_infoLayout = new QHBoxLayout();
    m_interfaceLayout = new QVBoxLayout();
    m_nicknameEdit = new QLineEdit(this);
    m_nicknameDesription = new QLabel(this);
    m_delete = new QPushButton("Удалить", this);
    m_type="NO DEFINED";

    m_Main = dynamic_cast<MainWindow*>(parent);

    m_typeBox->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    m_nicknameEdit->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    m_nicknameDesription->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    m_delete->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

    //this->setMaximumHeight(600);

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    connect(m_nicknameEdit, &QLineEdit::textEdited, this, &Imitation::changeName);
    connect(m_typeBox, QOverload<int>::of(&QComboBox::activated), this, &Imitation::changeTypePressedSLOT);
    connect(m_delete, &QPushButton::clicked, this, &Imitation::deletePressedSLOT);
    connect(Main, &MainWindow::timerSignal, this, &Imitation::newCycle);
}

Imitation::~Imitation()
{
    m_mainLayuot->deleteLater();
    m_infoLayout->deleteLater();
    m_interfaceLayout->deleteLater();
}


QJsonObject Imitation::toJson()
{
    QJsonObject obj=this->MI_TableView::toJson();
    obj["type"]=m_type;
    QJsonArray AI;
    for(int i=0; i<m_analogInputs.size(); ++i)
    {
        if(m_analogInputs.at(i)->save())
            AI.append(m_analogInputs.at(i)->toJson());
    }
    obj.insert("AnalogInputs", AI);

    QJsonArray AO;
    for(int i=0; i<m_analogOutputs.size(); ++i)
    {
        if(m_analogOutputs.at(i)->save())
            AO.append(m_analogOutputs.at(i)->toJson());
    }
    obj.insert("AnalogOutputs", AO);

    QJsonArray DI;
    for(int i=0; i<m_discreteInputs.size(); ++i)
    {
        if(m_discreteInputs.at(i)->save())
            DI.append(m_discreteInputs.at(i)->toJson());
    }
    obj.insert("DiscreteInputs", DI);

    QJsonArray DO;
    for(int i=0; i<m_discreteOutputs.size(); ++i)
    {
        if(m_discreteOutputs.at(i)->save())
            DO.append(m_discreteOutputs.at(i)->toJson());
    }
    obj.insert("DisctreteOutputs", DO);

    return obj;
}

void Imitation::fromJson(const QJsonObject &jsn)
{
    this->blockSignals(true);
    this->MI_TableView::fromJson(jsn);
    //m_type=jsn["type"].toString();
    m_name = jsn["name"].toString();
    m_nicknameEdit->setText(jsn["name"].toString());
    if(Modules::getTypeNames().contains(m_type)||objects::getTypeNames().contains(m_type))    //На случай если модуль не определен при загрузке
    {
        QJsonArray Arr=jsn["AnalogInputs"].toArray();
        int n = qMin(Arr.size(), m_analogInputs.size());
        for(int i=0; i<n; ++i)
        {
            m_analogInputs.at(i)->fromJson(Arr.at(i).toObject());
        }
        Arr=jsn["AnalogOutputs"].toArray();
        n = qMin(Arr.size(), m_analogOutputs.size());
        for(int i=0; i<n; ++i)
        {
            m_analogOutputs.at(i)->fromJson(Arr.at(i).toObject());
        }
        Arr=jsn["DiscreteInputs"].toArray();
        n = qMin(Arr.size(), m_discreteInputs.size());
        for(int i=0; i<n; ++i)
        {
            m_discreteInputs.at(i)->fromJson(Arr.at(i).toObject());
        }
        Arr=jsn["DisctreteOutputs"].toArray();
        n = qMin(Arr.size(), m_discreteOutputs.size());
        for(int i=0; i<n; ++i)
        {
            m_discreteOutputs.at(i)->fromJson(Arr.at(i).toObject());
        }

    }
    else
    {
        QString type=jsn["obj_type"].toString();
        QMessageBox msgBox;
        if(type=="module")
            msgBox.setText(QString("Не удалось определить тип модуля %1").arg(m_name));
        else
            msgBox.setText(QString("Не удалось определить тип объекта %1").arg(m_name));
        msgBox.exec();
    }
    this->blockSignals(false);
    emit nameChanged(m_name);
}

QVector<Contact *> Imitation::depends(Contact *cont)
{   //Для тех, у кого нет мгновенных зависимостей, будет создаваться пустой вектор
    QVector<Contact *> vect;
    return vect;
}

QList<Contact *> Imitation::contacts()
{
    QList<Contact *> buf;
    for(int i=0; i<m_analogInputs.size(); ++i)
        buf.append(m_analogInputs.at(i)->contact());
    for(int i=0; i<m_analogOutputs.size(); ++i)
        buf.append(m_analogOutputs.at(i)->contact());
    for(int i=0; i<m_discreteInputs.size(); ++i)
        buf.append(m_discreteInputs.at(i)->contact());
    for(int i=0; i<m_discreteOutputs.size(); ++i)
        buf.append(m_discreteOutputs.at(i)->contact());
    return buf;
}



void Imitation::inputsChaneged()
{

}

void Imitation::readInputs()
{

}

void Imitation::changeName()
{
    m_name=m_nicknameEdit->text();
    emit nameChanged(m_name);
}

void Imitation::deletePressedSLOT()
{
    emit deletePressed();
}

void Imitation::changeTypePressedSLOT(int type)
{
    if(m_type!=m_typeBox->itemText(type))
        emit changeTypePressed(type);
}

void Imitation::newCycle(int calcCycle)
{
    m_timeout=calcCycle;
}

const QString &Imitation::type() const
{
    return m_type;
}

