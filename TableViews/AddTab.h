#ifndef ADDTAB_H
#define ADDTAB_H

#include "Imitation.h"

class AddTab : public Imitation
{
public:
    AddTab(QWidget *parent, QString name = "+");    //Вкладка, при переходе на которую добавляется новый модуль/объект
};

#endif // ADDTAB_H
