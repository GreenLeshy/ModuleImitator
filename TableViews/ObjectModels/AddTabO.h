#ifndef ADDTABO_H
#define ADDTABO_H

#include <ObjectModel.h>

class AddTabO : public objectModel      //Объект, который ничего не делает, но нужен для работы логики добавления объектов
{
    Q_OBJECT
public:
     AddTabO(QWidget *parent, QString name = "+");    //Вкладка, при переходе на которую добавляется новый модуль
};

#endif // ADDTABO_H
