#ifndef objECTMODEL_H
#define objECTMODEL_H

#include <Imitation.h>

class Custom;
class objectModel : public Imitation
{
    Q_OBJECT
public:
    objectModel(QWidget *parent, QString name="NN");    //Класс, являющийся родительским для моделей объектов

    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject  &jsn) override;

    enum chrono_type : unsigned
    {
        Instant = 1,                                    //Чтение входных значений и рассчет модели производится при изменении входных значений
        temporary,                                    //Чтение входных значений и рассчет модели производится при срабатывании таймера
        mixed                                            //Реагирует и на изменение входных значений и на срабатывание таймера
    };

    Custom *parentCustom() const;
    void setParentCustom(Custom *newParentCustom);

protected:
    virtual void calculate(void);                  //Расчет модели объекта


public slots:
    void inputsChaneged(void) override;   //Изменились входные значения
    void newCycle(int calcCycle) override;                            //Сработал таймер
    void parentCustomRenamed(QString newName);

signals:
    void calculated(void);                          //Расчет модели окончен

protected:
    chrono_type m_chronoType;               //Тип блока
    Custom *m_parentCustom=nullptr;    //Custom-модуль-родитель, если он есть, то объект переименовывается и сохраняется вместе с ним
};

#endif // objECTMODEL_H
