#include "AnalogSetter.h"
#include "Objects.h"
#include "IOs.h"
#include "Contacts.h"

AnalogSetter::AnalogSetter(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    switch (n) {
    case 8:
        m_type=objects::getTypeName(objects::AnalogSetter_8);
        m_n=n;
    break;
    case 4:
        m_type=objects::getTypeName(objects::AnalogSetter_4);
        m_n=n;
    break;
    case 2:
        m_type=objects::getTypeName(objects::AnalogSetter_2);
        m_n=n;
    break;
    default:
        m_type=objects::getTypeName(objects::AnalogSetter);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);



    QString BUFname;
    for(int i=1; i<m_n+1; ++i)
    {
        QWidget *horizontalLine;
        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(3);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        BUFname=QString("IN").append(QString::number(i));
        DiscreteInput *DI = new DiscreteInput(this, BUFname);
        m_discreteInputs.append(DI);

        BUFname=QString("OUT").append(QString::number(i));
        AnalogOutput*AO = new AnalogOutput(this, BUFname);
        m_analogOutputs.append(AO);

        QHBoxLayout *L = new QHBoxLayout;
        L->addLayout(DI->layout());
        L->addLayout(AO->layout());
        m_interfaceLayout->addLayout(L);

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(2);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #D0D0D0;"));
        m_interfaceLayout->addWidget(horizontalLine);

        L = new QHBoxLayout;
        QLabel *label = new QLabel("Значение OFF:", this);
        L->addWidget(label);
        m_valueOFF.append(new QLineEdit("0.0", this));
        L->addWidget(m_valueOFF.last());
        label = new QLabel("Значение ON:", this);
        L->addWidget(label);
        m_valueON.append(new QLineEdit("20.0", this));
        L->addWidget(m_valueON.last());
        m_interfaceLayout->addLayout(L);

        connect(m_valueON.last(), &QLineEdit::textChanged, this, &AnalogSetter::OnOffValueChaneged);
        connect(m_valueOFF.last(), &QLineEdit::textChanged, this, &AnalogSetter::OnOffValueChaneged);

        m_values.append(false);
    }

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

QJsonObject AnalogSetter::toJson()
{
    QJsonObject jmod = this->objectModel::toJson();
    QJsonArray array;
    for(int i=0; i<m_n; ++i)
    {
        QJsonObject obj;
        obj["ValueON"]=m_valueON.at(i)->text();
        obj["ValueOFF"]=m_valueOFF.at(i)->text();
        array.append(obj);
    }
    jmod.insert("options", array);
    return jmod;
}

void AnalogSetter::fromJson(const QJsonObject &jmod)
{
    this->Imitation::fromJson(jmod);
    QJsonArray array=jmod["options"].toArray();
    int k = qMin(array.size(), m_n);
    for(int i=0; i<k; ++i)
    {
        QJsonObject obj= array.at(i).toObject();
        m_valueON.at(i)->setText(obj["ValueON"].toString());
        m_valueOFF.at(i)->setText(obj["ValueOFF"].toString());
    }
}

QVector<Contact *> AnalogSetter::depends(Contact *cont)
{
    IOClass *IO=cont->getIO();
    int i = m_discreteInputs.indexOf(dynamic_cast<DiscreteInput*>(IO));
    QVector<Contact *> vect;
    if(i>-1)
        vect.append(m_analogOutputs.at(i)->contact());
    return vect;
}

void AnalogSetter::calculate()
{
     for(int i=0; i<m_n; ++i)
        if(m_values.at(i))
            m_analogOutputs.at(i)->setValue(m_valueON.at(i)->text().toDouble());
        else
            m_analogOutputs.at(i)->setValue(m_valueOFF.at(i)->text().toDouble());
}

void AnalogSetter::readInputs()
{
    for(int i=0; i<m_n; ++i)
        m_values[i]=m_discreteInputs.at(i)->value();
}

void AnalogSetter::OnOffValueChaneged()
{
    readInputs();
    calculate();
}
