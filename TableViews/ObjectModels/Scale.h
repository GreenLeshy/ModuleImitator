#ifndef SCALE_H
#define SCALE_H

#include "ObjectModel.h"

class Scale : public objectModel
{
    Q_OBJECT
public:
    Scale(QWidget *parent=nullptr, QString name="NN", int n=2);      //Класс, реализующий масштабирование аналоговых сигналов
    virtual QVector<Contact *> depends(Contact *cont) override;

public slots:
    void setBorders(void);
    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject  &jmod) override;    //Загружает информацию об элементе из QJsonObject


protected:
    virtual void calculate(void) override;
    virtual void readInputs(void) override;

protected:
    QVector <QLineEdit*> m_MinInWidget;                           //Список виджетов для установки
    QVector <QLineEdit*> m_MinOutWidget;                           //Список виджетов для установки
    QVector <QLineEdit*> m_MaxInWidget;                           //Список виджетов для установки
    QVector <QLineEdit*> m_MaxOutWidget;                           //Список виджетов для установки
    QVector<double> m_maxIn;
    QVector<double> m_maxOut;
    QVector<double> m_minIn;
    QVector<double> m_minOut;
    QVector<double> m_value;
};

#endif // SCALE_H
