#ifndef COMPARATOR_H
#define COMPARATOR_H

#include "ObjectModel.h"

class Comparator : public objectModel       //Объект, реализующий работу компаратора
{
    Q_OBJECT
public:
    Comparator(QWidget *parent=nullptr, QString name="NN", int n=1);
    virtual QVector<Contact *> depends(Contact *cont) override;

protected:
    virtual void calculate(void) override;
};

#endif // COMPARATOR_H
