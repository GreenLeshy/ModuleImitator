#include "Scale.h"
#include "Objects.h"
#include "IOs.h"
#include "mainwindow.h"
#include "Contacts.h"

Scale::Scale(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    m_n=n;
    switch (n) {
    case 6:
        m_type=objects::getTypeName(objects::Scale_6);
    break;
    case 4:
        m_type=objects::getTypeName(objects::Scale_4);
    break;
    case 2:
        m_type=objects::getTypeName(objects::Scale_2);
    break;
    default:
        m_type=objects::getTypeName(objects::Scale);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;
    for(int i=1; i<m_n+1; ++i)
    {
        QHBoxLayout *HL;
        AnalogInput *AI;
        AnalogOutput*AO;
        QLabel *label;
        QLineEdit *lineedit;
        QWidget *horizontalLine;

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(3);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        BUFname=QString("IN").append(QString::number(i));
        AI = new AnalogInput(this, BUFname, 0, 20, 0, 20);
        m_analogInputs.append(AI);
        HL->addLayout(AI->layout());

        BUFname=QString("OUT").append(QString::number(i));
        AO = new AnalogOutput(this, BUFname, 0, 20, 0, 20);
        m_analogOutputs.append(AO);
        HL->addLayout(AO->layout());
        m_interfaceLayout->addLayout(HL);

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(2);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #D0D0D0"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        label = new QLabel("Мин:", this);
        HL->addWidget(label);
        lineedit=new QLineEdit("0", this);
        m_MinInWidget.append(lineedit);
        HL->addWidget(lineedit);
        connect(lineedit, &QLineEdit::textChanged, this, &Scale::setBorders);
        label = new QLabel("Макс:", this);
        HL->addWidget(label);
        lineedit=new QLineEdit("20", this);
        m_MaxInWidget.append(lineedit);
        HL->addWidget(lineedit);
        connect(lineedit, &QLineEdit::textChanged, this, &Scale::setBorders);
        label = new QLabel("Мин:", this);
        HL->addWidget(label);
        lineedit=new QLineEdit("0", this);
        m_MinOutWidget.append(lineedit);
        HL->addWidget(lineedit);
        connect(lineedit, &QLineEdit::textChanged, this, &Scale::setBorders);
        label = new QLabel("Макс:", this);
        HL->addWidget(label);
        lineedit=new QLineEdit("20", this);
        m_MaxOutWidget.append(lineedit);
        HL->addWidget(lineedit);
        connect(lineedit, &QLineEdit::textChanged, this, &Scale::setBorders);

        m_interfaceLayout->addLayout(HL);

        m_maxIn.append(0);
        m_maxOut.append(20);
        m_minIn.append(0);
        m_minOut.append(20);
        m_value.append(0);
    }
    setBorders();
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
    calculate();
}

void Scale::setBorders()
{
    for(int i=0; i<m_n; ++i)
    {
        m_minIn[i] = m_MinInWidget.at(i)->text().toDouble();
        m_minOut[i] = m_MinOutWidget.at(i)->text().toDouble();
        m_maxIn[i] = m_MaxInWidget.at(i)->text().toDouble();
        m_maxOut[i] = m_MaxOutWidget.at(i)->text().toDouble();
        m_analogInputs.at(i)->setLimits(m_minIn[i], m_maxIn[i], m_minIn[i], m_maxIn[i]);
        m_analogOutputs.at(i)->setLimits(m_minOut[i], m_maxOut[i], m_minOut[i], m_maxOut[i]);
    }
    calculate();
}

QJsonObject Scale::toJson()
{
    QJsonObject jmod = this->objectModel::toJson();
    QJsonArray array;
    for(int i=0; i<m_n; ++i)
    {
        QJsonObject obj;
        obj["MinIn"]=m_MinInWidget.at(i)->text();
        obj["MinOut"]=m_MinOutWidget.at(i)->text();
        obj["MaxIn"]=m_MaxInWidget.at(i)->text();
        obj["MaxOut"]=m_MaxOutWidget.at(i)->text();
        obj["value"]=m_value.at(i);
        array.append(obj);
    }
    jmod.insert("options", array);
    return jmod;
}

void Scale::fromJson(const QJsonObject &jmod)
{
    this->Imitation::fromJson(jmod);
    QJsonArray array=jmod["options"].toArray();
    int k = qMin(array.size(), m_n);
    for(int i=0; i<k; ++i)
    {
        QJsonObject obj= array.at(i).toObject();

        m_MinInWidget.at(i)->setText(obj["MinIn"].toString());
        m_MinOutWidget.at(i)->setText(obj["MinOut"].toString());
        m_MaxInWidget.at(i)->setText(obj["MaxIn"].toString());
        m_MaxOutWidget.at(i)->setText(obj["MaxOut"].toString());
        m_value[i]=obj["value"].toDouble();
    }
    setBorders();
}

QVector<Contact *> Scale::depends(Contact *cont)
{
    IOClass *IO=cont->getIO();
    int i = m_analogInputs.indexOf(dynamic_cast<AnalogInput*>(IO));
    QVector<Contact *> vect;
    if(i>-1)
        vect.append(m_analogOutputs.at(i)->contact());
    return vect;
}

void Scale::calculate()
{
    for(int i=0; i<m_n; ++i)
    {
        double buf = ((m_value.at(i)-m_minIn.at(i))/(m_maxIn.at(i)-m_minIn.at(i)))*(m_maxOut.at(i)-m_minOut.at(i))+m_minOut.at(i);
        m_analogOutputs.at(i)->setValue(buf);
    }
}

void Scale::readInputs()
{
    for(int i=0; i<m_n; ++i)
        m_value[i]=m_analogInputs.at(i)->value();
}
