#include "Gate.h"
#include "Objects.h"
#include "IOs.h"
#include "Contacts.h"
#include "mainwindow.h"

Gate::Gate(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::mixed;
    switch (n) {
    case 4:
        m_type=objects::getTypeName(objects::Gate_4);
        m_n=n;
    break;
    case 2:
        m_type=objects::getTypeName(objects::Gate_2);
        m_n=n;
    break;
    default:
        m_type=objects::getTypeName(objects::Gate);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;
    for(int i=1; i<m_n+1; ++i)
    {
        QHBoxLayout *HL;
        DiscreteInput *DI;
        DiscreteOutput*DO;
        QLabel *label;
        QComboBox *combobox;
        QLineEdit *lineedit;
        QWidget *horizontalLine;
        QWidget *verticalLine;

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(3);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        BUFname=QString("OP").append(QString::number(i));
        DI = new DiscreteInput(this, BUFname);
        m_discreteInputs.append(DI);
        HL->addLayout(DI->layout());

        verticalLine=new QWidget(this);
        verticalLine->setFixedWidth(2);
        verticalLine->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        verticalLine->setStyleSheet(QString("background-color: #008020;"));
        HL->addWidget(verticalLine);

        BUFname=QString("OPD").append(QString::number(i));
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);
        connect(dynamic_cast<DOContact*>(DO->contact()), &DOContact::newValue, Main, &MainWindow::playOnOff); //Звук срабатывания конечников

        HL=new QHBoxLayout;
        BUFname=QString("CL").append(QString::number(i));
        DI = new DiscreteInput(this, BUFname);
        m_discreteInputs.append(DI);
        HL->addLayout(DI->layout());

        verticalLine=new QWidget(this);
        verticalLine->setFixedWidth(2);
        verticalLine->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        verticalLine->setStyleSheet(QString("background-color: #008020;"));
        HL->addWidget(verticalLine);

        BUFname=QString("CLD").append(QString::number(i));
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);
        connect(dynamic_cast<DOContact*>(DO->contact()), &DOContact::newValue, Main, &MainWindow::playOnOff); //Звук срабатывания конечников

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(2);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #D0D0D0"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        label = new QLabel("Норм. сост.:", this);
        HL->addWidget(label);
        combobox=new QComboBox(this);
        combobox->addItem("НЗ");
        combobox->addItem("НО");
        combobox->addItem("Сохр. пол.");
        connect(combobox, &QComboBox::currentTextChanged, this, &Gate::setNormalVal);
        m_normalValWidget.append(combobox);
        HL->addWidget(combobox);

        label = new QLabel("СН:", this);
        HL->addWidget(label);
        combobox=new QComboBox(this);
        combobox->addItem("Норм");
        combobox->addItem("Залип откр.");
        combobox->addItem("Залип закр.");
        combobox->addItem("Залип");
        combobox->addItem("Не откр.");
        combobox->addItem("Не закр.");
        combobox->addItem("Застрял");
        combobox->addItem("Всегда откр.");
        combobox->addItem("Всегда закр.");
        connect(combobox, &QComboBox::currentTextChanged, this, &Gate::setSN);
        m_SNWidget.append(combobox);
        HL->addWidget(combobox);

        label = new QLabel("Время откр, мс:", this);
        HL->addWidget(label);
        lineedit=new QLineEdit("1000", this);
        m_TWidget.append(lineedit);
        HL->addWidget(lineedit);
        connect(lineedit, &QLineEdit::textChanged, this, &Gate::setT);
        m_interfaceLayout->addLayout(HL);

        m_inProcess.append(true);
        m_value.append(0);
        m_oldValue.append(0);
        m_lastInp.append(0);
        m_lastInp.append(0);
        m_normalVal.append(0);
        m_SN.append(0);
        m_T.append(1000);
        m_InWork.append(false);

    }

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

Gate::~Gate()
{
    int buf=0;
    for(int i=0; i<m_n; ++i)
    {
        if(m_InWork.at(i))
            --buf;
    }
    Main->gateStartWorking(buf);
}

void Gate::setNormalVal()
{
    for(int i=0; i<m_n; ++i)
    {
        int b = m_normalValWidget.at(i)->currentIndex();
        if(b!=m_normalVal.at(i))
        {
            m_normalVal[i]=b;
            m_inProcess[i]=true;
        }
    }
}

void Gate::setSN()
{
    for(int i=0; i<m_n; ++i)
    {
        int b = m_SNWidget.at(i)->currentIndex();
        if(b!=m_SN.at(i))
        {
            m_SN[i]=b;
            m_inProcess[i]=true;
        }
    }
}

void Gate::setT()
{
    for(int i=0; i<m_n; ++i)
    {
        int b=m_TWidget.at(i)->text().toInt();
            if(b<1)
            {
                b=1000;
                m_TWidget.at(i)->setText("1000");
            }
        m_T[i] = b;
    }
}

QJsonObject Gate::toJson()
{
    QJsonObject jmod = this->objectModel::toJson();
    QJsonArray array;
    for(int i=0; i<m_n; ++i)
    {
        QJsonObject obj;
        obj["NormalValue"]=m_normalValWidget.at(i)->currentText();
        obj["SN"]=m_SNWidget.at(i)->currentText();
        obj["T"]=m_TWidget.at(i)->text();
        obj["value"]=m_value.at(i);
        array.append(obj);
    }
    jmod.insert("options", array);
    return jmod;
}

void Gate::fromJson(const QJsonObject &jmod)
{
    this->objectModel::fromJson(jmod);
    QJsonArray array=jmod["options"].toArray();
    int k = qMin(array.size(), m_n);
    for(int i=0; i<k; i++)
    {
        QJsonObject obj= array.at(i).toObject();
        m_normalValWidget.at(i)->setCurrentText(obj["NormalValue"].toString());
        m_SNWidget.at(i)->setCurrentText(obj["SN"].toString());
        m_TWidget.at(i)->setText(obj["T"].toString());
        m_value[i]=obj["value"].toDouble();
    }
}

void Gate::calculate()
{
    int ibuf = 0;
    for(int i=0; i<m_n; ++i)
    {
        bool bbuf=false;
        if(m_inProcess.at(i))
        {
            int k=m_discreteInputs.at(2*i)->value()-m_discreteInputs.at(2*i+1)->value(); //Сигнал на открытие-сигнал на закрытие

            if(m_normalVal.at(i)==0)   //НЗ
            {
                if(k==0)
                    k=-1;
            }
            else if(m_normalVal.at(i)==1)   //НО
            {
                if(k==0)
                    k=1;
            }
            /*else if(m_SN.at(i)->currentIndex()==2)   //Сохр пол
            {

            }*/

            m_value[i]=m_value[i]+k*(double(m_timeout)/m_T.at(i))*100;
            if(m_value.at(i)<0)
            {
                m_value[i]=0;
            }
            if(m_value.at(i)>100)
            {
                m_value[i]=100;
            }

            if(m_value.at(i)==m_oldValue.at(i))
                m_inProcess[i]=false;                               //Если состояние клапана больше не меняется, то его не надо больше расчитывать (пока не сменятся входные значения или свойства клапана)
            m_oldValue[i]=m_value.at(i);

            bool opened=false;
            bool closed=false;
            if(m_value.at(i)==100)
                opened=true;
            else if(m_value.at(i)==0)
                closed=true;

            /*if(m_SN.at(i)==0)   //Норм СН
            {

            }
            else */if(m_SN.at(i)==1)   //Залип откр
            {
                opened=true;
            }
            else if(m_SN.at(i)==2)   //Залип закр
            {
                closed=true;
            }
            else if(m_SN.at(i)==3)   //Залип
            {
                opened=true;
                closed=true;
            }
            else if(m_SN.at(i)==4)   //Не откр
            {
                opened=false;
            }
            else if(m_SN.at(i)==5)   //Не закр
            {
                closed=false;
            }
            else if(m_SN.at(i)==6)   //Застрял
            {
                opened=false;
                closed=false;
            }
            else if(m_SN.at(i)==7)   //Всегда откр
            {
                opened=true;
                closed=false;
            }
            else if(m_SN.at(i)==8)   //Всегда закр
            {
                opened=false;
                closed=true;
            }
            if(k)
            {
                if(((k==1) & (!opened))|((k==-1) & (!closed)))
                    bbuf=true;
            }
            if(bbuf & !m_InWork.at(i))
                ++ibuf;
            else if(!bbuf & m_InWork.at(i))
                --ibuf;
            m_InWork[i]=bbuf;
            m_discreteOutputs.at(i*2)->setValue(opened);
            m_discreteOutputs.at(i*2+1)->setValue(closed);
        }
    }
    if(ibuf)
        Main->gateStartWorking(ibuf);
}
void Gate::readInputs()
{
    for(int i=0; i<m_n*2; ++i)
    {
        if(m_discreteInputs.at(i)->value()!=m_lastInp.at(i))
                m_inProcess[i/2]=true;
        m_lastInp[i]=m_discreteInputs.at(i)->value();
    }
}
