#include "Splitter_A.h"
#include "Objects.h"
#include "IOs.h"
#include "Contacts.h"

Splitter_A::Splitter_A(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    m_n=n;
    switch (n) {
    case 8:
        m_type=objects::getTypeName(objects::Splitter_A8);
    break;
    case 4:
        m_type=objects::getTypeName(objects::Splitter_A4);
    break;
    default:
        m_type=objects::getTypeName(objects::Splitter_A2);
        m_n=2;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;

    BUFname=QString("IN");
    AnalogInput *AI = new AnalogInput(this, BUFname);
    m_analogInputs.append(AI);
    BUFname=QString("OUT").append(QString::number(1));
    AnalogOutput* AO = new AnalogOutput(this, BUFname);
    m_analogOutputs.append(AO);

    QHBoxLayout *L = new QHBoxLayout;
    L->addLayout(AI->layout());
    L->addStretch(100);
    L->addLayout(AO->layout());
    m_interfaceLayout->addLayout(L);


    for(int i=2; i<m_n+1; ++i)
    {
        BUFname=QString("OUT").append(QString::number(i));
        AnalogOutput* AO = new AnalogOutput(this, BUFname);
        m_analogOutputs.append(AO);

        QHBoxLayout *L = new QHBoxLayout;
        L->addStretch(100);
        L->addLayout(AO->layout());
        m_interfaceLayout->addLayout(L);
    }

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

QVector<Contact *> Splitter_A::depends(Contact *cont)
{
    IOClass *IO=cont->getIO();
    QVector<Contact *> vect;
    if(m_analogInputs.contains(dynamic_cast<AnalogInput*>(IO)))
    {
        int n = m_analogOutputs.size();
        for(int j=0; j<n; ++j)
            vect.append(m_analogOutputs.at(j)->contact());
    }
    return vect;
}

void Splitter_A::calculate()
{
     for(int i=0; i<m_n; ++i)
        m_analogOutputs.at(i)->setValue(m_value);
}

void Splitter_A::readInputs()
{
        m_value=m_analogInputs.at(0)->value();
}
