#ifndef LOGICALUNIVERSAL_H
#define LOGICALUNIVERSAL_H

#include "ObjectModel.h"

class LogicalUniversal : public objectModel     //Объект, реализующий базовые логические функции
{
public:
    LogicalUniversal(QWidget *parent=nullptr, QString name="NN", int n=2);
    virtual QVector<Contact *> depends(Contact *cont) override;

protected:
    virtual void calculate(void) override;

protected:
};

#endif // LOGICALUNIVERSAL_H
