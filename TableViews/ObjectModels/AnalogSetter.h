#ifndef ANALOGSETTER_H
#define ANALOGSETTER_H

#include "ObjectModel.h"

class AnalogSetter : public objectModel         //Объект, выдающий одно из двух аналоговых значений в зависимости от входного сигнала
{
public:
    AnalogSetter(QWidget *parent=nullptr, QString name="NN", int n=4);
    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject  &jmod) override;    //Загружает информацию об элементе из QJsonObject
    virtual QVector<Contact *> depends(Contact *cont) override;

protected:
    virtual void calculate(void) override;
    virtual void readInputs(void) override;

public slots:
    void OnOffValueChaneged();

protected:
    QVector<bool> m_values;
    QVector<QLineEdit*> m_valueON;
    QVector<QLineEdit*> m_valueOFF;
};
#endif // ANALOGSETTER_H
