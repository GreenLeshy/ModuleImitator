#ifndef GATE_H
#define GATE_H

#include "ObjectModel.h"

class Gate : public objectModel
{
public:
    Gate(QWidget *parent=nullptr, QString name="NN", int n=2);      //Класс, реализующих модель клапанов (а так же насосов, вентиляторов и тд), он же двухбитная точка
    ~Gate();

public slots:
    void setNormalVal(void);
    void setSN(void);
    void setT(void);
    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject & jmod) override;    //Загружает информацию об элементе из QJsonObject

protected:
    virtual void calculate(void) override;
    virtual void readInputs(void) override;

protected:
    QVector <QComboBox*> m_normalValWidget;       //Список виджетов для установки m_normalVal
    QVector <QComboBox*> m_SNWidget;                   //Список виджетов для установки m_SN
    QVector <QLineEdit*> m_TWidget;                           //Список виджетов для установки m_T
    QVector<int> m_normalVal;                                      //Поведение объекта без управляющего сигнала (НЗ, НО или сохраняет состояние)
    QVector<int> m_SN;                                                   //Поведение конечных переключаетей (нормальная работа, или работа с ошибками)
    QVector<int> m_T;                                                      //Время переключения состояний объекта
    QVector<bool> m_inProcess;                                     //Флаг, показывающих необходимость расчета модели
    QVector<bool> m_lastInp;                                          //Последнее сохраненное значение входных значений
    QVector<double> m_value;                                        //Положение ОУ от закрыто (0) до открыто (100)
    QVector<double> m_oldValue;                                  //Положение ОУ, полученное при прошлом расчете
    QVector<bool> m_InWork;                                         //Двигатель работает
};

#endif // GATE_H
