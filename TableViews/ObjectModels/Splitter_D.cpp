#include "Splitter_D.h"
#include "Objects.h"
#include "IOs.h"
#include "Contacts.h"

Splitter_D::Splitter_D(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    m_n=n;
    switch (n) {
    case 8:
        m_type=objects::getTypeName(objects::Splitter_D8);
    break;
    case 4:
        m_type=objects::getTypeName(objects::Splitter_D4);
    break;
    default:
        m_type=objects::getTypeName(objects::Splitter_D2);
        m_n=2;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;

    BUFname=QString("IN");
    DiscreteInput *DI = new DiscreteInput(this, BUFname);
    m_discreteInputs.append(DI);
    BUFname=QString("OUT").append(QString::number(1));
    DiscreteOutput* DO = new DiscreteOutput(this, BUFname);
    m_discreteOutputs.append(DO);

    QHBoxLayout *L = new QHBoxLayout;
    L->addLayout(DI->layout());
    L->addStretch(100);
    L->addLayout(DO->layout());
    m_interfaceLayout->addLayout(L);


    for(int i=2; i<m_n+1; ++i)
    {
        BUFname=QString("OUT").append(QString::number(i));
        DiscreteOutput* DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);

        QHBoxLayout *L = new QHBoxLayout;
        L->addStretch(100);
        L->addLayout(DO->layout());
        m_interfaceLayout->addLayout(L);
    }

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

QVector<Contact *> Splitter_D::depends(Contact *cont)
{
    IOClass *IO=cont->getIO();
    QVector<Contact *> vect;
    if(m_discreteInputs.contains(dynamic_cast<DiscreteInput*>(IO)))
    {
        int n = m_discreteOutputs.size();
        for(int j=0; j<n; ++j)
            vect.append(m_discreteOutputs.at(j)->contact());
    }
    return vect;
}

void Splitter_D::calculate()
{
     for(int i=0; i<m_n; ++i)
        m_discreteOutputs.at(i)->setValue(m_value);
}

void Splitter_D::readInputs()
{
        m_value=m_discreteInputs.at(0)->value();
}
