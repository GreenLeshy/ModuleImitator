#ifndef SPLITTER_A_H
#define SPLITTER_A_H

#include "ObjectModel.h"

class Splitter_A : public objectModel       //Класс, реализующий разветвление аналоговых сигналов
{
public:
    Splitter_A(QWidget *parent=nullptr, QString name="NN", int n=4);
    virtual QVector<Contact *> depends(Contact *cont) override;

protected:
    virtual void calculate(void) override;
    virtual void readInputs(void) override;

public slots:

protected:
    double m_value;
};

#endif // SPLITTER_A_H
