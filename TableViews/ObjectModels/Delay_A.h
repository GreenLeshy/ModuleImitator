#ifndef DELAY_A_H
#define DELAY_A_H

#include "ObjectModel.h"
#include <QQueue>

class Delay_A : public objectModel         //Объект, реализующий задержку для аналогового сигнала
{
public:
    Delay_A(QWidget *parent=nullptr, QString name="NN", int n=4);
    void setDelay(int);
    int delay() const;
    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject  &jmod) override;    //Загружает информацию об элементе из QJsonObject

protected:
    virtual void calculate(void) override;
    virtual void readInputs(void) override;

public slots:
    void newDelay(const QString delay);
    void delayOut(void);

protected:
    int m_delay=10;
    QLineEdit *m_delayEdit;             //Виджет для редактирования задержки
    QLabel *m_delayDescription;     //Описание для m_delayEdit
    QList<double> m_inValues;       //Текущие входные значения
    QQueue<QList<double>> m_inValuesHistory;    //Очередь входных значений, хранит их от момента изменения входных значений до истечения таймера
};

#endif // DELAY_A_H
