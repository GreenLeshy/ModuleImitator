#include "Limit.h"
#include "Objects.h"
#include "IOs.h"
#include "Contacts.h"

Limit::Limit(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    m_n=n;
    switch (n) {
    case 6:
        m_type=objects::getTypeName(objects::Limit_6);
    break;
    case 4:
        m_type=objects::getTypeName(objects::Limit_4);
    break;
    case 2:
        m_type=objects::getTypeName(objects::Limit_2);
    break;
    default:
        m_type=objects::getTypeName(objects::Limit);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;
    for(int i=1; i<m_n+1; ++i)
    {
        QHBoxLayout *HL;
        AnalogInput *AI;
        AnalogOutput*AO;
        QLabel *label;
        QLineEdit *lineedit;
        QWidget *horizontalLine;

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(3);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        BUFname=QString("IN").append(QString::number(i));
        AI = new AnalogInput(this, BUFname, 0, 20, 0, 20);
        m_analogInputs.append(AI);
        HL->addLayout(AI->layout());

        BUFname=QString("OUT").append(QString::number(i));
        AO = new AnalogOutput(this, BUFname, 0, 20, 0, 20);
        m_analogOutputs.append(AO);
        HL->addLayout(AO->layout());
        m_interfaceLayout->addLayout(HL);

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(2);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #D0D0D0"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        label = new QLabel("Мин:", this);
        HL->addWidget(label);
        lineedit=new QLineEdit("0", this);
        m_MinWidget.append(lineedit);
        HL->addWidget(lineedit);
        connect(lineedit, &QLineEdit::textChanged, this, &Limit::setBorders);
        label = new QLabel("Макс:", this);
        HL->addWidget(label);
        lineedit=new QLineEdit("20", this);
        m_MaxWidget.append(lineedit);
        HL->addWidget(lineedit);
        connect(lineedit, &QLineEdit::textChanged, this, &Limit::setBorders);
        m_interfaceLayout->addLayout(HL);
        m_max.append(20);
        m_min.append(0);
        m_value.append(0);
    }
    setBorders();
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

QVector<Contact *> Limit::depends(Contact *cont)
{
    IOClass *IO=cont->getIO();
    int i = m_analogInputs.indexOf(dynamic_cast<AnalogInput*>(IO));
    QVector<Contact *> vect;
    if(i>-1)
        vect.append(m_analogOutputs.at(i)->contact());
    return vect;
}

void Limit::setBorders()
{
    for(int i=0; i<m_n; ++i)
    {
        m_min[i] = m_MinWidget.at(i)->text().toDouble();
        m_max[i] = m_MaxWidget.at(i)->text().toDouble();
        m_analogInputs.at(i)->setLimits(m_min[i], m_max[i], m_min[i], m_max[i]);
    }
    calculate();
}

QJsonObject Limit::toJson()
{
    QJsonObject jmod = this->objectModel::toJson();
    QJsonArray array;
    for(int i=0; i<m_n; ++i)
    {
        QJsonObject obj;
        obj["Min"]=m_MinWidget.at(i)->text();
        obj["Max"]=m_MaxWidget.at(i)->text();
        obj["value"]=m_value.at(i);
        array.append(obj);
    }
    jmod.insert("options", array);
    return jmod;
}

void Limit::fromJson(const QJsonObject &jmod)
{
    this->Imitation::fromJson(jmod);
    QJsonArray array=jmod["options"].toArray();
    int k = qMin(array.size(), m_n);
    for(int i=0; i<k; ++i)
    {
        QJsonObject obj= array.at(i).toObject();

        m_MinWidget.at(i)->setText(obj["Min"].toString());
        m_MaxWidget.at(i)->setText(obj["Max"].toString());
        m_value[i]=obj["value"].toDouble();
    }
    setBorders();
}

void Limit::calculate()
{
    for(int i=0; i<m_n; ++i)
    {
        m_analogOutputs.at(i)->setValue(Static::clamp(m_value.at(i), m_min.at(i), m_max.at(i)));
    }
}

void Limit::readInputs()
{
    for(int i=0; i<m_n; ++i)
        m_value[i]=m_analogInputs.at(i)->value();
}

