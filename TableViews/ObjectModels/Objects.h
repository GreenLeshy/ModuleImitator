#ifndef objECTS_H
#define objECTS_H
#include <QStringList>
#include "Delay_D.h"
#include "Delay_A.h"
#include "Gate.h"
#include "Splitter_D.h"
#include "Splitter_A.h"
#include "AnalogSetter.h"
#include "Scale.h"
#include "LogicalUniversal.h"
#include "ArithmeticUniversal.h"
#include "Comparator.h"
#include "Limit.h"

class objects               //Объединяет все объекты в одну библиотеку и реализует фабрику объектов
{
public:
    objects();

    enum obj_types: unsigned        //Обязательно должен быть согласован по количеству и порядку элементов с typeNames
    {
        NO_DEFINED= 0,
        Delay_D,
        Delay_D2,
        Delay_D4,
        Delay_D8,
        Delay_A,
        Delay_A2,
        Delay_A4,
        Delay_A8,
        Gate,
        Gate_2,
        Gate_4,
        AnalogSetter,
        AnalogSetter_2,
        AnalogSetter_4,
        AnalogSetter_8,
        Comparator,
        Comparator_2,
        Comparator_4,
        Splitter_D2,
        Splitter_D4,
        Splitter_D8,
        Splitter_A2,
        Splitter_A4,
        Splitter_A8,
        Scale,
        Scale_2,
        Scale_4,
        Scale_6,
        Limit,
        Limit_2,
        Limit_4,
        Limit_6,
        LogicalUniversal,
        LogicalUniversal_2,
        LogicalUniversal_4,
        ArithmeticUniversal,
        ArithmeticUniversal_2,
        ArithmeticUniversal_4
    };

    static QStringList getTypeNames();
    static QString getTypeName(int i);
    static objectModel *newobject(QWidget *parent=nullptr, int type=0, QString name="");

private:
    static const QStringList typeNames;
};


#endif // objECTS_H
