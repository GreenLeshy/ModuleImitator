#include "ArithmeticUniversal.h"
#include "Objects.h"
#include "IOs.h"
#include "Contacts.h"

ArithmeticUniversal::ArithmeticUniversal(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    m_n=n;
    switch (n) {
    case 4:
        m_type=objects::getTypeName(objects::ArithmeticUniversal_4);
    break;
    case 2:
        m_type=objects::getTypeName(objects::ArithmeticUniversal_2);
    break;
    default:
        m_type=objects::getTypeName(objects::ArithmeticUniversal);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;

    for(int i=1; i<m_n+1; ++i)
    {
        QHBoxLayout *HL;
        AnalogInput *AI;
        AnalogOutput*AO;
        QWidget *horizontalLine;
        QWidget *verticalLine;

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(3);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        BUFname=QString::number(i).append(" x");
        AI = new AnalogInput(this, BUFname);
        m_analogInputs.append(AI);
        HL->addLayout(AI->layout());

        verticalLine=new QWidget(this);
        verticalLine->setFixedWidth(2);
        verticalLine->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        verticalLine->setStyleSheet(QString("background-color: #008020;"));
        HL->addWidget(verticalLine);

        BUFname=QString::number(i).append(" +");
        AO = new AnalogOutput(this, BUFname);
        m_analogOutputs.append(AO);
        HL->addStretch(100);
        HL->addLayout(AO->layout());
        m_interfaceLayout->addLayout(HL);

        HL=new QHBoxLayout;
        BUFname=QString::number(i).append(" y");
        AI = new AnalogInput(this, BUFname);
        m_analogInputs.append(AI);
        HL->addLayout(AI->layout());

        verticalLine=new QWidget(this);
        verticalLine->setFixedWidth(2);
        verticalLine->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        verticalLine->setStyleSheet(QString("background-color: #008020;"));
        HL->addWidget(verticalLine);

        BUFname=QString::number(i).append(" -");
        AO = new AnalogOutput(this, BUFname);
        m_analogOutputs.append(AO);
        HL->addStretch(100);
        HL->addLayout(AO->layout());
        m_interfaceLayout->addLayout(HL);

        int size = m_analogInputs.at(0)->width()*4;     //Сам не знаю почему, но работает только с *4

        BUFname=QString::number(i).append(" *");
        AO = new AnalogOutput(this, BUFname);
        m_analogOutputs.append(AO);
        HL = new QHBoxLayout;
        HL->addSpacing(size);
        HL->addStretch(100);
        HL->addLayout(AO->layout());
        m_interfaceLayout->addLayout(HL);

        BUFname=QString::number(i).append(" /");
        AO = new AnalogOutput(this, BUFname);
        m_analogOutputs.append(AO);
        HL = new QHBoxLayout;
        HL->addSpacing(size);
        HL->addStretch(100);
        HL->addLayout(AO->layout());
        m_interfaceLayout->addLayout(HL);
    }

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    calculate();
}

QVector<Contact *> ArithmeticUniversal::depends(Contact *cont)
{
    IOClass *IO=cont->getIO();
    int i = m_analogInputs.indexOf(dynamic_cast<AnalogInput*>(IO));
    QVector<Contact *> vect;
    if(i>-1)
    {
        i=i/2;
        int t=i*4;
        for(int j=0; j<4; ++j)
            vect.append(m_analogOutputs.at(t+j)->contact());
    }
    return vect;
}

void ArithmeticUniversal::calculate()
{
     for(int i=0; i<m_n; ++i)
     {
         double x = m_analogInputs.at(i*2)->value();
         double y = m_analogInputs.at(i*2+1)->value();
         m_analogOutputs.at(i*4)->setValue(x+y);
         m_analogOutputs.at(i*4+1)->setValue(x-y);
         m_analogOutputs.at(i*4+2)->setValue(x*y);
         m_analogOutputs.at(i*4+3)->setValue(x/y);
     }
}
