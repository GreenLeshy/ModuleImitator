#include "Objects.h"


objects::objects()
{
}

const QStringList objects::typeNames =         //Обязательно должен быть согласован по количеству и порядку элементов с obj_types
{
    "NO DEFINED",
    "DelayD",
    "DelayD_2",
    "DelayD_4",
    "DelayD_8",
    "DelayA",
    "DelayA_2",
    "DelayA_4",
    "DelayA_8",
    "2-bit Point",
    "2-bit Point_2",
    "2-bit Point_4",
    "AnalogSetter",
    "AnalogSetter_2",
    "AnalogSetter_4",
    "AnalogSetter_8",
    "Comparator",
    "Comparator_2",
    "Comparator_4",
    "SplitterD_2",
    "SplitterD_4",
    "SplitterD_8",
    "SplitterA_2",
    "SplitterA_4",
    "SplitterA_8",
    "Scale",
    "Scale_2",
    "Scale_4",
    "Scale_6",
    "Limit",
    "Limit_2",
    "Limit_4",
    "Limit_6",
    "LogicUniversal",
    "LogicUniversal_2",
    "LogicUniversal_4",
    "MathUniversal",
    "MathUniversal_2",
    "MathUniversal_4"
};

QStringList objects::getTypeNames()
{
    return typeNames;
}

QString objects::getTypeName(int i)
{
    return typeNames.at(i);
}

objectModel *objects::newobject(QWidget *parent, int type, QString name)
{
    objectModel* newTab;
    QString objName=name;
    if(objName=="")
        objName=typeNames.at(type);
    switch (type)
    {
    case int(objects::NO_DEFINED):
        newTab = new objectModel(parent, objName);
    break;
    case int(objects::Delay_D):
        newTab = new class Delay_D(parent, objName, 1);
    break;
    case int(objects::Delay_D2):
        newTab = new class Delay_D(parent, objName, 2);
    break;
    case int(objects::Delay_D4):
        newTab = new class Delay_D(parent, objName, 4);
    break;
    case int(objects::Delay_D8):
        newTab = new class Delay_D(parent, objName, 8);
    break;
    case int(objects::Delay_A):
        newTab = new class Delay_A(parent, objName, 1);
    break;
    case int(objects::Delay_A2):
        newTab = new class Delay_A(parent, objName, 2);
    break;
    case int(objects::Delay_A4):
        newTab = new class Delay_A(parent, objName, 4);
    break;
    case int(objects::Delay_A8):
        newTab = new class Delay_A(parent, objName, 8);
    break;
    case int(objects::Gate):
        newTab = new class Gate(parent, objName, 1);
    break;
    case int(objects::Gate_2):
        newTab = new class Gate(parent, objName, 2);
    break;
    case int(objects::Gate_4):
        newTab = new class Gate(parent, objName, 4);
    break;
    case int(objects::AnalogSetter):
        newTab = new class AnalogSetter(parent, objName, 1);
    break;
    case int(objects::AnalogSetter_2):
        newTab = new class AnalogSetter(parent, objName, 2);
    break;
    case int(objects::AnalogSetter_4):
        newTab = new class AnalogSetter(parent, objName, 4);
    break;
    case int(objects::AnalogSetter_8):
        newTab = new class AnalogSetter(parent, objName, 8);
    break;
    case int(objects::Comparator):
        newTab = new class Comparator(parent, objName, 1);
    break;
    case int(objects::Comparator_2):
        newTab = new class Comparator(parent, objName, 2);
    break;
    case int(objects::Comparator_4):
        newTab = new class Comparator(parent, objName, 4);
    break;
    case int(objects::Splitter_D2):
        newTab = new class Splitter_D(parent, objName, 2);
    break;
    case int(objects::Splitter_D4):
        newTab = new class Splitter_D(parent, objName, 4);
    break;
    case int(objects::Splitter_D8):
        newTab = new class Splitter_D(parent, objName, 8);
    break;
    case int(objects::Splitter_A2):
        newTab = new class Splitter_A(parent, objName, 2);
    break;
    case int(objects::Splitter_A4):
        newTab = new class Splitter_A(parent, objName, 4);
    break;
    case int(objects::Splitter_A8):
        newTab = new class Splitter_A(parent, objName, 8);
    break;
    case int(objects::Scale):
        newTab = new class Scale(parent, objName, 1);
    break;
    case int(objects::Scale_2):
        newTab = new class Scale(parent, objName, 2);
    break;
    case int(objects::Scale_4):
        newTab = new class Scale(parent, objName, 4);
    break;
    case int(objects::Scale_6):
        newTab = new class Scale(parent, objName, 6);
    break;
    case int(objects::Limit):
        newTab = new class Limit(parent, objName, 1);
    break;
    case int(objects::Limit_2):
        newTab = new class Limit(parent, objName, 2);
    break;
    case int(objects::Limit_4):
        newTab = new class Limit(parent, objName, 4);
    break;
    case int(objects::Limit_6):
        newTab = new class Limit(parent, objName, 6);
    break;
    case int(objects::LogicalUniversal):
        newTab = new class LogicalUniversal(parent, objName, 1);
    break;
    case int(objects::LogicalUniversal_2):
        newTab = new class LogicalUniversal(parent, objName, 2);
    break;
    case int(objects::LogicalUniversal_4):
        newTab = new class LogicalUniversal(parent, objName, 4);
    break;
    case int(objects::ArithmeticUniversal):
        newTab = new class ArithmeticUniversal(parent, objName, 1);
    break;
    case int(objects::ArithmeticUniversal_2):
        newTab = new class ArithmeticUniversal(parent, objName, 2);
    break;
    case int(objects::ArithmeticUniversal_4):
        newTab = new class ArithmeticUniversal(parent, objName, 4);
    break;
    default:
        return nullptr;
    break;
    }
    return newTab;
}
