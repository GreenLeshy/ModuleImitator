#ifndef LIMIT_H
#define LIMIT_H
#include "ObjectModel.h"

class Limit : public objectModel    //Объект, ограничивающий аналоговое значение
{
    Q_OBJECT
public:
    Limit(QWidget *parent=nullptr, QString name="NN", int n=1);
    virtual QVector<Contact *> depends(Contact *cont) override;

public slots:
    void setBorders(void);
    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject  &jmod) override;    //Загружает информацию об элементе из QJsonObject

protected:
    virtual void calculate(void) override;
    virtual void readInputs(void) override;

    QVector <QLineEdit*> m_MinWidget;                           //Список виджетов для установки
    QVector <QLineEdit*> m_MaxWidget;                           //Список виджетов для установки
    QVector<double> m_max;
    QVector<double> m_min;
    QVector<double> m_value;
};

#endif // LIMIT_H
