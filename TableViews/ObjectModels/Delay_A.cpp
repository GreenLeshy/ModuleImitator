#include "Delay_A.h"
#include "Objects.h"
#include <QTimer>
#include "IOs.h"

Delay_A::Delay_A(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    switch (n) {
    case 8:
        m_type=objects::getTypeName(objects::Delay_A8);
        m_n=n;
    break;
    case 4:
        m_type=objects::getTypeName(objects::Delay_A4);
        m_n=n;
    break;
    case 2:
        m_type=objects::getTypeName(objects::Delay_A2);
        m_n=n;
    break;
    default:
        m_type=objects::getTypeName(objects::Delay_A);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;
    for(int i=1; i<m_n+1; ++i)
    {
        BUFname=QString("AIN").append(QString::number(i));
        AnalogInput *AI = new AnalogInput(this, BUFname);
        m_analogInputs.append(AI);
        BUFname=QString("AOUT").append(QString::number(i));
        AnalogOutput*AO = new AnalogOutput(this, BUFname);
        m_analogOutputs.append(AO);
        QHBoxLayout *L = new QHBoxLayout;
        L->addLayout(AI->layout());
        L->addLayout(AO->layout());
        m_interfaceLayout->addLayout(L);
        m_inValues.append(0);
    }

    m_delayEdit=new QLineEdit("10", this);
    m_delayDescription=new QLabel("Задержка, мс:", this);
    QHBoxLayout *L = new QHBoxLayout;
    L->addWidget(m_delayDescription);
    L->addWidget(m_delayEdit);
    m_mainLayuot->addLayout(L);

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    connect(m_delayEdit, &QLineEdit::textChanged, this, &Delay_A::newDelay);
}

void Delay_A::setDelay(int val)
{
    if(val<1)
        m_delayEdit->setText("10");
    m_delay=val;
}

int Delay_A::delay() const
{
    return m_delay;
}

QJsonObject Delay_A::toJson()
{
    QJsonObject jmod = this->objectModel::toJson();
    jmod["delay"]=m_delay;
    return jmod;
}

void Delay_A::fromJson(const QJsonObject &jmod)
{
    this->Imitation::fromJson(jmod);
    m_delayEdit->setText(QString::number(jmod["delay"].toInt()));
}

void Delay_A::calculate()
{
    m_inValuesHistory.enqueue(m_inValues);
    QTimer::singleShot(m_delay, this, &Delay_A::delayOut);
}

void Delay_A::readInputs()
{
    m_inValues.clear();
    for(int i=0; i<m_n; ++i)
    {
        m_inValues.append(m_analogInputs.at(i)->value());
    }
}

void Delay_A::newDelay(const QString delay)
{
    setDelay(delay.toInt());
}

void Delay_A::delayOut()
{
    QList<double> outValues=m_inValuesHistory.dequeue();
    for(int i=0; i<m_n; ++i)
    {
        m_analogOutputs.at(i)->setValue(outValues.at(i));
    }
}
