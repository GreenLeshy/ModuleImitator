#ifndef SPLITTER_D_H
#define SPLITTER_D_H

#include "ObjectModel.h"

class Splitter_D : public objectModel       //Класс, реализующий разветвление дискретных сигналов
{
public:
    Splitter_D(QWidget *parent=nullptr, QString name="NN", int n=4);
    virtual QVector<Contact *> depends(Contact *cont) override;

protected:
    virtual void calculate(void) override;
    virtual void readInputs(void) override;

public slots:

protected:
    bool m_value;
};
#endif // SPLITTER_D_H
