#ifndef ARITHMETICUNIVERSAL_H
#define ARITHMETICUNIVERSAL_H

#include "ObjectModel.h"

class ArithmeticUniversal : public objectModel      //Объект, реализующий базовые математические функции
{
public:
    ArithmeticUniversal(QWidget *parent=nullptr, QString name="NN", int n=2);
    virtual QVector<Contact *> depends(Contact *cont) override;


protected:
    virtual void calculate(void) override;
};

#endif // ARITHMETICUNIVERSAL_H
