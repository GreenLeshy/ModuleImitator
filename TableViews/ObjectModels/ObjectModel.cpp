#include "ObjectModel.h"
#include "Imitation.h"
#include "mainwindow.h"
#include "Objects.h"
#include "Custom.h"
#include "static.h"

objectModel::objectModel(QWidget *parent, QString name) : Imitation(parent, name)
{
    m_typeBox->addItems(objects::getTypeNames());

    m_nicknameDesription->setText("Имя:");
    m_name = name;
    m_nicknameEdit->setText(name);

    m_typeBox->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_nicknameDesription->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    m_nicknameEdit->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

    m_infoLayout->addWidget(m_typeBox);
    m_infoLayout->addStretch(1);
    m_infoLayout->addWidget(m_nicknameDesription);
    m_infoLayout->addWidget(m_nicknameEdit);
    m_infoLayout->addWidget(m_delete);
    m_mainLayuot->addLayout(m_infoLayout);


    m_mainLayuot->addStretch(10);
    m_mainLayuot->addLayout(m_interfaceLayout);
    m_mainLayuot->addStretch(100);

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    connect(this, &Imitation::changeTypePressed, Main, &MainWindow::objectChangeType);
    connect(this, &Imitation::nameChanged, Main, &MainWindow::objectChangeName);
    connect(this, &Imitation::deletePressed, Main, &MainWindow::deleteobject);
}

void objectModel::calculate()
{

}

QJsonObject objectModel::toJson()
{
    QJsonObject jmod = this->Imitation::toJson();
    jmod["obj_type"]="object";
    if(m_parentCustom==nullptr)
        jmod["parentCustom"]="nullptr";
    else
        jmod["parentCustom"]=m_parentCustom->name();
    return jmod;
}

void objectModel::fromJson(const QJsonObject &jsn)
{
    this->Imitation::fromJson(jsn);
    QString pC = jsn["parentCustom"].toString();
    if(pC!="nullptr" && pC!="")
    {
        auto custom = Main->module(pC);
        if(custom!=nullptr)
            setParentCustom(dynamic_cast<Custom*>(custom));
    }
}

void objectModel::inputsChaneged()
{
    if(m_chronoType==objectModel::chrono_type::Instant)
    {
        readInputs();
        calculate();
    }
    else if(m_chronoType==objectModel::chrono_type::mixed)
        readInputs();
}

void objectModel::newCycle(int calcCycle)
{
    Imitation::newCycle(calcCycle);
    if(m_chronoType==objectModel::chrono_type::temporary)
    {
        readInputs();
        calculate();
    }
    else if(m_chronoType==objectModel::chrono_type::mixed)
    {
        calculate();
    }
}

void objectModel::parentCustomRenamed(QString newName)
{
    //qDebug() <<"oM parentCustomRenamed from"+m_name+" to "+Static::writeTag(m_name, newName, '{', '}');
    QString name = m_name;
    if(!(name.contains("{") && name.contains("}")))
        name.prepend("{}");
    name = Static::writeTag(name, newName, '{', '}');
    m_nicknameEdit->setText(name);
    m_nicknameEdit->textChanged(name);
    m_nicknameEdit->textEdited(name);
}

Custom *objectModel::parentCustom() const
{
    return m_parentCustom;
}

void objectModel::setParentCustom(Custom *newParentCustom)
{
    //qDebug("setParentCustom");
    if(m_parentCustom!=nullptr)
        disconnect(m_parentCustom, &Custom::nameChanged, this, &objectModel::parentCustomRenamed);
    m_parentCustom = newParentCustom;
    if(m_name!=Static::writeTag(m_name, m_parentCustom->name(), '{', '}'))
        parentCustomRenamed(m_parentCustom->name());
    connect(m_parentCustom, &Custom::nameChanged, this, &objectModel::parentCustomRenamed);
}

