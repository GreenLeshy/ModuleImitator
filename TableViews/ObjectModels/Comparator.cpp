#include "Comparator.h"
#include "Objects.h"
#include "IOs.h"
#include "Contacts.h"

Comparator::Comparator(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    m_n=n;
    switch (n) {
    case 4:
        m_type=objects::getTypeName(objects::Comparator_4);
    break;
    case 2:
        m_type=objects::getTypeName(objects::Comparator_2);
    break;
    default:
        m_type=objects::getTypeName(objects::Comparator);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;

    for(int i=1; i<m_n+1; ++i)
    {
        QHBoxLayout *HL;
        AnalogInput *AI;
        DiscreteOutput* DO;
        QWidget *horizontalLine;
        QWidget *verticalLine;

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(3);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        BUFname=QString::number(i).append(" x");
        AI = new AnalogInput(this, BUFname);
        m_analogInputs.append(AI);
        HL->addLayout(AI->layout());

        verticalLine=new QWidget(this);
        verticalLine->setFixedWidth(2);
        verticalLine->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        verticalLine->setStyleSheet(QString("background-color: #008020;"));
        HL->addWidget(verticalLine);

        BUFname=QString::number(i).append(" x>y");
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL->addStretch(100);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);

        HL=new QHBoxLayout;
        BUFname=QString::number(i).append(" y");
        AI = new AnalogInput(this, BUFname);
        m_analogInputs.append(AI);
        HL->addLayout(AI->layout());

        verticalLine=new QWidget(this);
        verticalLine->setFixedWidth(2);
        verticalLine->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        verticalLine->setStyleSheet(QString("background-color: #008020;"));
        HL->addWidget(verticalLine);

        BUFname=QString::number(i).append(" x<y");
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL->addStretch(100);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);

        int size = m_analogInputs.at(0)->width()*4;     //Сам не знаю почему, но работает только с *4

        BUFname=QString::number(i).append(" x=y");
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL = new QHBoxLayout;
        HL->addSpacing(size);
        HL->addStretch(100);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);
    }

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    calculate();
}

QVector<Contact *> Comparator::depends(Contact *cont)
{
    IOClass *IO=cont->getIO();
    int i = m_analogInputs.indexOf(dynamic_cast<AnalogInput*>(IO));
    QVector<Contact *> vect;
    if(i>-1)
    {
        i=i/2;
        int t=i*3;
        for(int j=0; j<3; ++j)
            vect.append(m_discreteOutputs.at(t+j)->contact());
    }
    return vect;
}

void Comparator::calculate()
{
    for(int i=0; i<m_n; ++i)
    {
        double x = m_analogInputs.at(i*2)->value();
        double y = m_analogInputs.at(i*2+1)->value();
        auto more = m_discreteOutputs.at(i*3);
        auto less = m_discreteOutputs.at(i*3+1);
        auto equal = m_discreteOutputs.at(i*3+2);
        bool b1=0, b2=0, b3=0;

        if(x>y)
            b1=true;
        else
        {
            if(x<y)
                b2=true;
        else
                b3=true;
        }
        more->setValue(b1);
        less->setValue(b2);
        equal->setValue(b3);
    }
}
