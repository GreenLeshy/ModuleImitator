#include "Delay_D.h"
#include "DiscreteInput.h"
#include "DiscreteOutput.h"
#include <QTimer>
#include "Objects.h"

Delay_D::Delay_D(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    switch (n) {
    case 8:
        m_type=objects::getTypeName(objects::Delay_D8);
        m_n=n;
    break;
    case 4:
        m_type=objects::getTypeName(objects::Delay_D4);
        m_n=n;
    break;
    case 2:
        m_type=objects::getTypeName(objects::Delay_D2);
        m_n=n;
    break;
    default:
        m_type=objects::getTypeName(objects::Delay_D);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;
    for(int i=1; i<m_n+1; ++i)
    {
        BUFname=QString("DIN").append(QString::number(i));
        DiscreteInput *DI = new DiscreteInput(this, BUFname);
        m_discreteInputs.append(DI);
        BUFname=QString("DOUT").append(QString::number(i));
        DiscreteOutput*DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        QHBoxLayout *L = new QHBoxLayout;
        L->addLayout(DI->layout());
        L->addLayout(DO->layout());
        m_interfaceLayout->addLayout(L);
        m_inValues.append(false);
    }

    m_delayEdit=new QLineEdit("10", this);
    m_delayDescription=new QLabel("Задержка, мс:", this);
    QHBoxLayout *L = new QHBoxLayout;
    L->addWidget(m_delayDescription);
    L->addWidget(m_delayEdit);
    m_mainLayuot->addLayout(L);

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    connect(m_delayEdit, &QLineEdit::textChanged, this, &Delay_D::newDelay);
}

void Delay_D::setDelay(int val)
{
    if(val<1)
        m_delayEdit->setText("10");
    m_delay=val;
}

int Delay_D::delay() const
{
    return m_delay;
}

QJsonObject Delay_D::toJson()
{
    QJsonObject jmod = this->objectModel::toJson();
    jmod["delay"]=m_delay;
    return jmod;
}

void Delay_D::fromJson(const QJsonObject &jmod)
{
    this->Imitation::fromJson(jmod);
    m_delayEdit->setText(QString::number(jmod["delay"].toInt()));
}

void Delay_D::calculate()
{
    m_inValuesHistory.enqueue(m_inValues);
    QTimer::singleShot(m_delay, this, &Delay_D::delayOut);

}

void Delay_D::readInputs()
{
    m_inValues.clear();
    for(int i=0; i<m_n; ++i)
    {
        m_inValues.append(m_discreteInputs.at(i)->value());
    }
}

void Delay_D::newDelay(const QString delay)
{
    setDelay(delay.toInt());
}

void Delay_D::delayOut()
{
    QList<bool> outValues=m_inValuesHistory.dequeue();
    for(int i=0; i<m_n; ++i)
    {
        m_discreteOutputs.at(i)->setValue(outValues.at(i));
    }
}
