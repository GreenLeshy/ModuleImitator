#include "LogicalUniversal.h"
#include "Objects.h"
#include "IOs.h"
#include "Contacts.h"

LogicalUniversal::LogicalUniversal(QWidget *parent, QString name, int n) : objectModel(parent, name)
{
    m_chronoType=chrono_type::Instant;
    m_n=n;
    switch (n) {
    case 4:
        m_type=objects::getTypeName(objects::LogicalUniversal_4);
    break;
    case 2:
        m_type=objects::getTypeName(objects::LogicalUniversal_2);
    break;
    default:
        m_type=objects::getTypeName(objects::LogicalUniversal);
        m_n=1;
    break;
    }
    m_typeBox->setCurrentText(m_type);

    QString BUFname;

    for(int i=1; i<m_n+1; ++i)
    {
        QHBoxLayout *HL;
        DiscreteInput *DI;
        DiscreteOutput*DO;
        QWidget *horizontalLine;
        QWidget *verticalLine;

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(3);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        BUFname=QString::number(i).append(" x");
        DI = new DiscreteInput(this, BUFname);
        m_discreteInputs.append(DI);
        HL->addLayout(DI->layout());

        verticalLine=new QWidget(this);
        verticalLine->setFixedWidth(2);
        verticalLine->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        verticalLine->setStyleSheet(QString("background-color: #008020;"));
        HL->addWidget(verticalLine);

        BUFname=QString::number(i).append(" &");
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL->addStretch(100);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);

        HL=new QHBoxLayout;
        BUFname=QString::number(i).append(" y");
        DI = new DiscreteInput(this, BUFname);
        m_discreteInputs.append(DI);
        HL->addLayout(DI->layout());

        verticalLine=new QWidget(this);
        verticalLine->setFixedWidth(2);
        verticalLine->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        verticalLine->setStyleSheet(QString("background-color: #008020;"));
        HL->addWidget(verticalLine);

        BUFname=QString::number(i).append(" |");
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL->addStretch(100);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);

        BUFname=QString::number(i).append(" =");
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL = new QHBoxLayout;
        HL->addStretch(100);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);

        BUFname=QString::number(i).append(" ^");
        DO = new DiscreteOutput(this, BUFname);
        m_discreteOutputs.append(DO);
        HL = new QHBoxLayout;
        HL->addStretch(100);
        HL->addLayout(DO->layout());
        m_interfaceLayout->addLayout(HL);
    }

    m_nicknameDesription->setText("Имя:");

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    calculate();
}

QVector<Contact *> LogicalUniversal::depends(Contact *cont)
{
    IOClass *IO=cont->getIO();
    int i = m_discreteInputs.indexOf(dynamic_cast<DiscreteInput*>(IO));
    QVector<Contact *> vect;
    if(i>-1)
    {
        i=i/2;
        int t=i*4;
        for(int j=0; j<4; ++j)
            vect.append(m_discreteOutputs.at(t+j)->contact());
    }
    return vect;
}

void LogicalUniversal::calculate()
{
     for(int i=0; i<m_n; ++i)
     {
         bool x = m_discreteInputs.at(i*2)->value();
         bool y = m_discreteInputs.at(i*2+1)->value();
         m_discreteOutputs.at(i*4)->setValue(x&y);
         m_discreteOutputs.at(i*4+1)->setValue(x|y);
         m_discreteOutputs.at(i*4+2)->setValue(x==y);
         m_discreteOutputs.at(i*4+3)->setValue(x^y);
     }
}

