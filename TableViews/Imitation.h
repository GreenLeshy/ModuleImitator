#ifndef IMITATION_H
#define IMITATION_H

#include "MI_TableView.h"

class IOClass;
class AnalogInput;
class AnalogOutput;
class DiscreteInput;
class DiscreteOutput;
class Contact;
class MainWindow;
class Imitation : public MI_TableView
{
    Q_OBJECT
public:

    Imitation(QWidget *parent, QString name);       //Класс, являющийся родительским для модулей и моделей объектов
    ~Imitation();

    QList<Contact*> contacts(void);                 //Возвращает список контактов
    virtual void readInputs(void);                      //Чтение входных значений
    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject &jsn) override;    //Загружает информацию об элементе из QJsonObject
    virtual QVector<Contact *> depends(Contact *cont);  //Возвращает список контактов, которые мгновенно связаны с указанным (мгновенно меняют свое значение при изменении значения указанного) Данная функция необходима для избегания зацикливания, во всех мгновенных объектах ее необходимо переопределить

    const QString &type() const;

public slots:
    virtual void inputsChaneged(void);  //Изменились входные значения
    virtual void changeName();              //Пользователь изменил имя модуля/объекта
    void deletePressedSLOT();                 //Нажата кнопка удаления модуля/объекта
    void changeTypePressedSLOT(int);   //Пользователь изменил тип модуля/объекта
    virtual void newCycle(int calcCycle);  //Сработал таймер


signals:
    void outputsChaneged(void);         //Изменились выходные значения
    void nameChanged(QString &);         //Изменилось имя модуля/объекта
    void deletePressed();                       //Нажата кнопка удаления модуля/объекта
    void changeTypePressed(int);         //Пользователь изменил тип модуля/объекта

protected:
    QVector<AnalogInput*> m_analogInputs;         //Список аналоговых входов
    QVector<AnalogOutput*> m_analogOutputs;   //Список аналоговых выходов
    QVector<DiscreteInput*> m_discreteInputs;         //Список дискретных входов
    QVector<DiscreteOutput*> m_discreteOutputs;   //Список дискретных выходов
    QComboBox *m_typeBox;                   //Виджет выбора типа модуля/объекта
    QHBoxLayout *m_infoLayout;              //Layuot с информацией о модуле/объекте (тип и имя)
    QVBoxLayout *m_interfaceLayout;      //Layuot с входами и выходами
    QLineEdit *m_nicknameEdit;                //Виджет редактирования псевдонима
    QLabel *m_nicknameDesription;         //Пояснение к m_nicknameEdit
    QPushButton *m_delete;                      //Кнопка удаления модуля/объекта
    QString m_type;                                    //Тип модуля/объекта
    int m_n;                                                //Количество входов или выходов (для каждого по-разному)
    MainWindow *m_Main;
    int m_timeout;                                    //Период расчета, мс
};

#endif // IMITATION_H
