#ifndef MI_TABLEVIEW_H
#define MI_TABLEVIEW_H

#include <QTabWidget>
#include <QTableView>
#include <QObject>
#include <QWidget>
#include <QTextEdit>
#include <QComboBox>
#include <QTabWidget>
#include <QTableView>
#include <QSlider>
#include <QPushButton>
#include <QLayout>
#include <QLabel>
#include <QCheckBox>
#include <QLineEdit>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

class MainWindow;
class MI_TableView : public QTableView      //Родительский класс для всех вкладок
{
    Q_OBJECT
public:
    MI_TableView(QWidget *parent, QString name);

    const QString name() const;
    void setName(const QString &newName);
    QVBoxLayout *mainLayuot() const;
    void setMainLayuot(QVBoxLayout *newMainLayuot);

    virtual QJsonObject toJson(void);               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject  &jsn);    //Загружает информацию об элементе из QJsonObject

    MainWindow *getMain() const;

public slots:
    virtual void connected(bool val);      //Установлено COM соединение
    virtual void disconnected(void);       //Разорвано COM соединение
    virtual void communicationProblem(int isProblem); //Проблемы со связью 0 - отсутсвуют, 1 - присутствуют, 2 - отсутсвие связи


protected:
    QString m_name;                                 //Имя вкладки
    MainWindow *Main;                            //Указатель на главное окно
    QVBoxLayout *m_mainLayuot;            //Главный Layuot
};

#endif // MI_TABLEVIEW_H
