#include "ERU_ECP.h"
#include "mainwindow.h"
#include "IOs.h"
#include "led.h"
#include "static.h"
#include "Modules.h"

ERU_ECP::ERU_ECP(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_type=Modules::getTypeName(Modules::ERU);
    m_typeBox->setCurrentText(m_type);

    QString name;
    QHBoxLayout *HL;
    QLabel *label;
    QWidget *horizontalLine;

        name=QString("IN");
        AnalogInput *AI = new AnalogInput(this, name, 0, 100, 0, 100);
        m_interfaceLayout->addLayout(AI->layout());
        m_analogInputs.append(AI);

        name=QString("OUT");
        AnalogOutput *AO = new AnalogOutput(this, name, 0, 50, 0, 50);
        m_interfaceLayout->addLayout(AO->layout());
        m_analogOutputs.append(AO);

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(3);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        label = new QLabel("Уставка: ", this);
        HL->addWidget(label);
        m_targetWidget = new QLabel("0", this);
        HL->addWidget(m_targetWidget);
        label = new QLabel("Входное давл: ", this);
        HL->addWidget(label);
        m_valueWidget = new QLabel("0", this);
        HL->addWidget(m_valueWidget);
        label = new QLabel("Позиция штока: ", this);
        HL->addWidget(label);
        m_positionWidget = new QLabel("0", this);
        HL->addWidget(m_positionWidget);
        m_interfaceLayout->addLayout(HL);

        horizontalLine=new QWidget(this);
        horizontalLine->setFixedHeight(2);
        horizontalLine->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        horizontalLine->setStyleSheet(QString("background-color: #008020;"));
        m_interfaceLayout->addWidget(horizontalLine);

        HL=new QHBoxLayout;
        label = new QLabel("k:", this);
        HL->addWidget(label);
        m_kWidget=new QLineEdit("1.0", this);
        HL->addWidget(m_kWidget);
        label = new QLabel("Ti:", this);
        HL->addWidget(label);
        m_TiWidget=new QLineEdit("1.0", this);
        HL->addWidget(m_TiWidget);
        label = new QLabel("Td:", this);
        HL->addWidget(label);
        m_TdWidget=new QLineEdit("1.0", this);
        HL->addWidget(m_TdWidget);
        m_interfaceLayout->addLayout(HL);

        HL=new QHBoxLayout;
        label = new QLabel("Диапазон позиции штока: от ", this);
        HL->addWidget(label);
        m_positionMinWidget=new QLineEdit("1", this);
        HL->addWidget(m_positionMinWidget);
        label = new QLabel(" до ", this);
        HL->addWidget(label);
        m_positionMaxWidget=new QLineEdit("49", this);
        HL->addWidget( m_positionMaxWidget);
        label = new QLabel("мм", this);
        HL->addWidget(label);
        m_interfaceLayout->addLayout(HL);

        HL=new QHBoxLayout;
        label = new QLabel("Длина штока: ", this);
        HL->addWidget(label);
        m_LWidget=new QLineEdit("50", this);
        HL->addWidget(m_LWidget);
        label = new QLabel("мм", this);
        HL->addWidget(label);
        m_interfaceLayout->addLayout(HL);

        HL=new QHBoxLayout;
        label = new QLabel("Скорость изм уст: ", this);
        HL->addWidget(label);
        m_ustChangeSpeedWidget=new QLineEdit("1", this);
        HL->addWidget(m_ustChangeSpeedWidget);
        label = new QLabel("мм. рт. ст./сек", this);
        HL->addWidget(label);
        m_plusButton=new QPushButton("+", this);
        HL->addWidget(m_plusButton);
        m_minusButton=new QPushButton("-", this);
        HL->addWidget(m_minusButton);
        m_interfaceLayout->addLayout(HL);

        HL=new QHBoxLayout;
        label = new QLabel("Дельта: ", this);
        HL->addWidget(label);
        m_deltaWidget=new QLineEdit("0", this);
        HL->addWidget(m_deltaWidget);
        m_useDeltaWidget=new QCheckBox("Использовать дельту", this);
        HL->addWidget(m_useDeltaWidget);
        m_interfaceLayout->addLayout(HL);

        connect(m_LWidget, &QLineEdit::editingFinished, this, &ERU_ECP::LChaneged);
        connect(m_positionMaxWidget, &QLineEdit::editingFinished, this, &ERU_ECP::positionMinMaxChaneged);
        connect(m_positionMinWidget, &QLineEdit::editingFinished, this, &ERU_ECP::positionMinMaxChaneged);
        connect(m_ustChangeSpeedWidget, &QLineEdit::editingFinished, this, &ERU_ECP::ustChangeSpeedChaneged);
        connect(m_kWidget, &QLineEdit::editingFinished, this, &ERU_ECP::setParams);
        connect(m_TiWidget, &QLineEdit::editingFinished, this, &ERU_ECP::setParams);
        connect(m_TdWidget, &QLineEdit::editingFinished, this, &ERU_ECP::setParams);
        //connect(Main, &MainWindow::timerSignal, this, &ERU_ECP::newCycle);
        LChaneged();
        ustChangeSpeedChaneged();
        this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

QJsonObject ERU_ECP::toJson()
{
    QJsonObject jmod = this->Module::toJson();
        QJsonObject obj;
        obj["value"]=m_value;
        obj["target"]=m_target;
        obj["position"]=m_position;
        obj["positionMin"]=m_positionMin;
        obj["positionMax"]=m_positionMax;
        obj["k"]=m_k;
        obj["Ti"]=m_Ti;
        obj["Td"]=m_Td;
        obj["L"]=m_L;
        obj["ustChangeSpeed"]=m_ustChangeSpeed;
        obj["ELast1"]=m_ELast1;
        obj["ELast2"]=m_ELast2;
        obj["delta"]=m_deltaWidget->text().toDouble();
        obj["useDelta"]=m_useDeltaWidget->isChecked();
    jmod.insert("options", obj);
    return jmod;
}

void ERU_ECP::fromJson(const QJsonObject &jmod)
{
    this->Module::fromJson(jmod);
        QJsonObject obj=jmod["options"].toObject();
        m_valueWidget->setText(QString::number(obj["value"].toDouble()));
        m_target=obj["target"].toDouble();
        m_targetWidget->setText(QString::number(m_target));
        m_positionWidget->setText(QString::number(obj["position"].toDouble()));
        m_positionMinWidget->setText(QString::number(obj["positionMin"].toDouble()));
        m_positionMaxWidget->setText(QString::number(obj["positionMax"].toDouble()));
        m_deltaWidget->setText(QString::number(obj["delta"].toDouble()));
        m_useDeltaWidget->setChecked(obj["useDelta"].toBool());
        m_kWidget->setText(QString::number(obj["k"].toDouble()));
        m_TiWidget->setText(QString::number(obj["Ti"].toDouble()));
        m_TdWidget->setText(QString::number(obj["Td"].toDouble()));
        m_LWidget->setText(QString::number(obj["L"].toDouble()));
        m_ustChangeSpeed=obj["ustChangeSpeed"].toDouble();
        m_ELast1=obj["ELast1"].toDouble();
        m_ELast2=obj["ELast2"].toDouble();
        LChaneged();
        ustChangeSpeedChaneged();
        setParams();
}

void ERU_ECP::LChaneged()
{
    m_L=m_LWidget->text().toDouble();
    positionMinMaxChaneged();
    m_analogOutputs.at(0)->setLimits(0, m_L, 0, m_L);
}

void ERU_ECP::positionMinMaxChaneged()
{
    m_positionMax=m_positionMaxWidget->text().toDouble();
    m_positionMin=m_positionMinWidget->text().toDouble();
    m_positionMax=Static::clamp(m_positionMax, 0.0, m_L);
    m_positionMin=Static::clamp(m_positionMin, 0.0, m_positionMax);
    m_positionMaxWidget->setText(QString::number(m_positionMax));
    m_positionMinWidget->setText(QString::number(m_positionMin));
}

void ERU_ECP::ustChangeSpeedChaneged()
{
    m_ustChangeSpeed=m_ustChangeSpeedWidget->text().toDouble();
}

void ERU_ECP::setParams()
{
    m_k=m_kWidget->text().toDouble();
    m_Ti=m_TiWidget->text().toDouble();
    m_Td=m_TdWidget->text().toDouble();
}

void ERU_ECP::newMessage(const QByteArray &text, bool check)
{
    //this->Module::newMessage(text, check);
    this->Module::blink();
    if((uchar(text[0])==*m_address) & check)
    {   //Нестандартное чтение регистров
        if(uchar(text[1])==C_READ)
            if(text.size()==8)
                {
                    QByteArray buf;
                    QByteArray buf1;

                    buf.append(*m_address);       //id модуля
                    buf.append(text[1]);              //id команды
                    buf.append(uchar(8));           //Количество байтов
                    buf.append(uchar(0));           //Старший байт status, всегда 0

                    uchar status = 0b00000000;
                    status|=(m_position==m_L)<<7;
                    status|=(m_position==0)<<6;
                    status|=(m_minusButton->isDown())<<1;
                    status|=(m_plusButton->isDown())<<0;
                    buf.append(status);        //Младший байт status

                    buf1=Static::toU8(m_value*100);
                    buf.append(uchar(buf1[1]));            //Старший байт давл
                    buf.append(uchar(buf1[0]));            //Младший байт давл

                    buf1=Static::toU8(m_target*100);
                    buf.append(uchar(buf1[1]));            //Старший байт уставки
                    buf.append(uchar(buf1[0]));            //Младший байт уставки

                    buf1=Static::toU8(m_position);
                    buf.append(uchar(buf1[0]));            //Старший байт положения затвора
                    buf.append(uchar(buf1[1]));            //Младший байт положения затвора

                    QByteArray buf2=Static::CRC(buf);
                    buf.append(buf2);
                    sendAnswer(buf);
                }
        if(uchar(text[1])==C_WRITE)
        {
                uchar first = text[3];
                uchar n = text[5];
                for(uchar i = 0; i<n; ++i)
                {
                    QByteArray buf1;
                    buf1.append(text[7+2*i]);
                    buf1.append(text[8+2*i]);
                    setReg(first+i, buf1);
                }

                QByteArray buf;
                QByteArray buf1;
                for(int i = 0; i<6; ++i)
                    buf.append(text.at(i));
                QByteArray buf2=Static::CRC(buf);
                buf.append(buf2);
                sendAnswer(buf);
        }
    }
}

void ERU_ECP::newCycle(int calcCycle)
{
    Module::newCycle(calcCycle);
    double T = m_timeout/1000.0;
    if(m_plusButton->isDown())
        m_target+=(m_ustChangeSpeed*T);
    if(m_minusButton->isDown())
        m_target-=(m_ustChangeSpeed*T);

    if(m_Ti==0)
        m_Ti=Q_INFINITY;
    double kp=m_k;
    double ki=(T/m_Ti)*kp;
    double kd=kp*m_Td/T;
    double e=0;
    if(m_useDeltaWidget->isChecked())
        m_value=m_target+m_deltaWidget->text().toDouble();
    else
        m_value=m_analogInputs.at(0)->value();
    e=(m_target-m_value);
    if(m_target<1)           //Так надо для проверки
        m_position=0;
    else
    {
    m_position=m_position+ki*e+kp*(e-m_ELast1)+kd*(e-2*m_ELast1+m_ELast2);
    //m_position=Static::clamp(m_position, (double)m_positionMin, (double)m_positionMax);   Эти границы не используются, так как реальный регулятор кривой
    m_position=Static::clamp(m_position, 0.0, m_L);
    }
    m_analogOutputs.at(0)->setValue(m_position);
    m_positionWidget->setText(QString::number(m_position));
    m_ELast2=m_ELast1;
    m_ELast1=e;
    m_valueWidget->setText(QString::number(m_value));
    m_targetWidget->setText(QString::number(m_target));
}

void ERU_ECP::setReg(uchar reg, QByteArray regText)
{
    if(reg==C_TARGET)
        m_target=Static::toU16(regText[0], regText[1])/100.0;
    else if(reg==C_STEP_OPEN)
        m_position+=Static::toU16(regText[0], regText[1])*0.5;
    else if(reg==C_STEP_CLOSE)
        m_position-=Static::toU16(regText[0], regText[1])*0.5;
    else if(reg==C_FAST_CLOSE)
    {
        m_target=0.2;       //Как в реальном устройстве
        m_position=0;
    }
}
