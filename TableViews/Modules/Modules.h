#ifndef MODULES_H
#define MODULES_H
#include <QStringList>
//#include "Module.h"
#include "DI_11.h"
#include "DIO_11.h"
#include "DO_11.h"
#include "AO_11.h"
#include "AI_12.h"
#include "ERU_ECP.h"
#include "DO_31A.h"
#include "DI_33A.h"
#include "DI_32A.h"
#include "AI_32A.h"
#include "AI_31A.h"
#include "AI_33A.h"
#include "AO_31A.h"
#include "AIO_31A.h"
#include "DIO_32A.h"
#include "DIA_31A.h"
#include "Custom.h"

class Modules
{
public:
    Modules();
    enum mod_types: unsigned        //Обязательно должен быть согласован по количеству и порядку элементов с typeNames
    {
        NO_TYPE= 0,
        DI_11,
        DO_11,
        DIO_11,
        AI_12,
        AO_11,
        DI_32A,
        DI_33A,
        DIA_31A,
        DO_31A,
        DIO_32A,
        AI_31A,
        AI_32A,
        AI_33A,
        AO_31A,
        AIO_31A,
        ERU,
        Custom
    };

    static QStringList getTypeNames();
    static QString getTypeName(int i);
    static Module *newModule(QWidget *parent=nullptr, int type=0, uchar addr=0);

private:
    static const QStringList typeNames;
};

#endif // MODULES_H
