#include "Module.h"
#include <QSpacerItem>
#include "mainwindow.h"
#include "Modules.h"
#include "led.h"
#include "static.h"
#include "IOs.h"

Module::Module(QWidget *parent, uchar addr) : Imitation(parent, QString::number((uint)addr))
{
    *m_address = addr;
    m_typeBox->addItems(Modules::getTypeNames());
    m_typeBox->addItems(Main->customBlocks());
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);
    m_useNickname = new QCheckBox(this);//////////////
    m_nicknameDesription->setText("Использовать псевдоним:");
    m_isConnected = new led(this,"Connected", 10);
    m_RXD = new led(this,"RXD");
    m_TXD = new led(this,"TXD");
    m_RUN = new led(this,"RUN");
    m_RUN->setValue(1);
    m_doAnswer = new QCheckBox(this);
    m_doAnswer->setChecked(true);
    m_doAnswerDesription = new QLabel("Отвечать на запросы CPU", this);
    m_addressBox = new QComboBox(this);
    m_addressDescription = new QLabel("Адрес: ", this);
    m_addressLayout = new QHBoxLayout;

    m_useNickname->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_isConnected->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    m_doAnswer->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_addressDescription->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    m_addressBox->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);

    m_addressLayout->addWidget(m_typeBox);
    //m_addressLayout->addStretch(1);
    m_addressLayout->addWidget(m_addressDescription);
    m_addressLayout->addWidget(m_addressBox);
    m_addressLayout->addWidget(m_delete);
    m_mainLayuot->addLayout(m_addressLayout);

     m_infoLayout->addWidget(m_nicknameDesription);
     m_infoLayout->addWidget(m_useNickname);
     m_infoLayout->addWidget(m_nicknameEdit);
     m_mainLayuot->addLayout(m_infoLayout);

     QHBoxLayout *L = new QHBoxLayout;
     L->addWidget(m_RXD);
     //L->addStretch(100);
     m_interfaceLayout->addLayout(L);
     L = new QHBoxLayout;
     L->addWidget(m_TXD);
     //L->addStretch(100);
     m_interfaceLayout->addLayout(L);
     L = new QHBoxLayout;
     L->addWidget(m_RUN);
     //L->addStretch(100);
     m_interfaceLayout->addLayout(L);
     //m_interfaceLayout->setSpacing(0);

     m_mainLayuot->addLayout(m_interfaceLayout);
     m_mainLayuot->addStretch(1);
     L = new QHBoxLayout;
     L->addWidget(m_isConnected);
     //L->addStretch(1);
     m_mainLayuot->addLayout(L);

     L = new QHBoxLayout;
     L->addWidget(m_doAnswer);
     L->addWidget(m_doAnswerDesription);
     //L->addStretch(1);
     m_mainLayuot->addLayout(L);

     this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

     if(Main->isConnected())    //Устанавливаем статус коннекта при инициализации
         connected(true);

     connect(Main, &MainWindow::newMessage, this, &Module::newMessage);
     connect(this, &Module::answer, Main, &MainWindow::answerSLOT);
     connect(m_addressBox, QOverload<const QString &>::of(&QComboBox::activated), this, &Module::addrChanegedSLOT);         //так как есть int  и Qstring сигналы activated необходим QOverload
     connect(this, &Module::addrChanged, Main, &MainWindow::moduleChangeAddr);
     connect(Main, &MainWindow::addrListUpdated, this, &Module::addrListUpdated);
     connect(this, &Module::changeTypePressed, Main, &MainWindow::moduleChangeType);
     connect(this, &Imitation::nameChanged, Main, &MainWindow::moduleChangeName);
     connect(this, &Imitation::deletePressed, Main, &MainWindow::deleteModule);
     connect(Main, &MainWindow::communicationProblem, this, &Module::communicationProblem);
     connect(m_useNickname, &QCheckBox::clicked, this, &Module::changeName);
     connect(this, &Module::addrChanged, this, &Module::changeName);

     addrChanegedSLOT(QString::number(addr));
}

Module::~Module()
{
    //emit addrChanged(m_address, 0);        //Освобождаем адрес
}

QJsonObject Module::toJson()
{
    QJsonObject jmod = this->Imitation::toJson();
    jmod["obj_type"]="module";
    jmod["addr"]=*m_address;
    jmod["nickname"]=m_nickname;
    jmod["useNickname"]=m_useNickname->isChecked();
    jmod["doAnswer"]=m_doAnswer->isChecked();
    return jmod;
}

void Module::fromJson(const QJsonObject &jsn)
{
    this->Imitation::fromJson(jsn);
    addrChanegedSLOT(QString::number(jsn["addr"].toInt()));
    m_nickname=jsn["nickname"].isString();
    m_useNickname->setChecked(jsn["useNickname"].toBool());
    changeName();
    m_doAnswer->setChecked(jsn["doAnswer"].toBool());
}

void Module::copyCoreData(Module *original)
{
    this->setParent(original->parentWidget());
    *m_address=original->address();
    m_name=original->name();
    m_nickname=original->nickname();
    m_nicknameEdit->setText(m_nickname);
    m_useNickname->setChecked(original->useNickname()->isChecked());
    m_doAnswer->setChecked(original->doAnswer()->isChecked());
}

void Module::connected(bool val)
{
    if(val==1)
        m_isConnected->setValue(1);
}

void Module::disconnected()
{
    m_isConnected->setValue(0);
}

void Module::blink()
{
    m_RXD->blink(1);
}

void Module::newMessage(const QByteArray &text, bool check)
{
    blink();
    if((uchar(text[0])==*m_address) & check)
    {
        uint16_t addr = Static::toU16(text[2], text[3]);
        uint16_t number = Static::toU16(text[4], text[5]);

        if(uchar(text[1])==C_WRITE)
         {
             uint16_t first = Static::toU16(text[2], text[3]);
             uint16_t n = Static::toU16(text[4], text[5]);
             for(uchar i = 0; i<n; ++i)
             {
                 uint16_t buf1 = Static::toU16(text[7+2*i], text[8+2*i]);
                 setRegister(first+i, buf1);
             }
             readOutputs();
         }
        sendAnswer(addr, number, text[1]);
    }
}



void Module::addrListUpdated(QVector<bool> addresses)
{
    addresses[*m_address]=false;                 //Разрешаем модулю его собственный адрес, указывая, что он не занят
    QStringList addressesList;
    for(int i=0; i<ADDR_AVALIABLE; ++i)
        if(addresses[i]==0)
            addressesList.append(QString::number(i));

    m_addressBox->clear();
    m_addressBox->addItems(addressesList);
    int j = addressesList.indexOf(QString::number(*m_address));
    m_addressBox->setCurrentIndex(j);
}

void Module::addrChanegedSLOT(const QString newA)
{
    int buf=*m_address;
    *m_address=newA.toInt();

    emit addrChanged(buf, *m_address);
}

void Module::changeName()
{
    m_nickname=m_nicknameEdit->text();
    if(m_useNickname->isChecked())
        m_name=m_nickname;
    else
        m_name=QString::number(*m_address);
    emit nameChanged(m_name);
}

void Module::communicationProblem(int p)
{
    if(p==0)
        m_isConnected->setValue(1);
    else if(p==1)
        m_isConnected->setValue(3);
    else if(p==2)
        m_isConnected->setValue(2);
}

void Module::newCycle(int calcCycle)
{
    Imitation::newCycle(calcCycle);
    *m_wrkInd+=calcCycle;
}

void Module::readOutputs()
{}

void Module::sendAnswer(QByteArray &text)
{
    if(m_doAnswer->isChecked())
    {
        emit answer(text);
        m_TXD->blink(1);
    }
}

void Module::sendAnswer(uint16_t addr, uint16_t number, uchar command)
{
    if(m_doAnswer->isChecked())
    {
            int end = addr+number;
                QByteArray buf;
                buf.append(*m_address);       //id модуля
                buf.append(command);              //id команды
                if(command==C_READ)
                {
                    buf.append(uchar(number*2));
                    for(int i = addr; i<end; ++i)
                    {
                        int t = getRegister(i);
                        QByteArray buf2=Static::toU8(t);
                        buf.append(buf2.at(1));
                        buf.append(buf2.at(0));
                    }
                }else
                if(command==C_WRITE)
                {
                    QByteArray buf2=Static::toU8(addr);
                    buf.append(buf2.at(1));
                    buf.append(buf2.at(0));
                    buf2=Static::toU8(number);
                    buf.append(buf2.at(1));
                    buf.append(buf2.at(0));
                }
                QByteArray buf2=Static::CRC(buf);
                buf.append(buf2);

                sendAnswer(buf);
                buf.clear();
            }
}

void Module::memoryInitialisation()
{
    m_memAddr=0;
    m_memEnd=0;
    m_mem.append(0);
    m_memNames.append("NONE");
}

int Module::memEnd() const
{
    return m_memEnd;
}

void Module::setMemEnd(int newMemEnd)
{
    m_memEnd = newMemEnd;
}

int Module::memAddr() const
{
    return m_memAddr;
}

void Module::setMemAddr(int newMemAddr)
{
    m_memAddr = newMemAddr;
}

const QStringList &Module::memNames() const
{
    return m_memNames;
}

QCheckBox *Module::doAnswer() const
{
    return m_doAnswer;
}

uint16_t Module::getRegister(int i) const
{
    if((i<m_memAddr) | (i>m_memEnd))
        return 0;
    else
        return m_mem.at(i-m_memAddr);
}

void Module::setRegister(int i, uint16_t val)
{
    if((i<m_memAddr) | (i>m_memEnd))
        return;
    else
    {
        if(m_mem[i-m_memAddr]!=val)
        {
            m_mem[i-m_memAddr]=val;
            emit registerChanged(i);
        }
    }
}

QString Module::getRegisterName(int i) const
{
    if((i<m_memAddr) | (i>m_memEnd))
        return "NONE";
    else
        return m_memNames.at(i-m_memAddr);
}

void Module::setRegisterName(int i, QString name)
{
    if((i<m_memAddr) | (i>m_memEnd))
        return;
    else
        m_memNames[i-m_memAddr]=name;
}

QCheckBox *Module::useNickname() const
{
    return m_useNickname;
}

const QString Module::nickname() const
{
    return m_nickname;
}

uchar Module::address() const
{
    return *m_address;
}

void Module::addIO(IOClass *IO)
{
    IO_type IOt = IO->IOType();
    data_type Dt = IO->dataType();
    if(IOt==IO_type::Input)
    {
        if(Dt==data_type::Discrete)
            m_discreteInputs.append(dynamic_cast<DiscreteInput*>(IO));
        else
            m_analogInputs.append(dynamic_cast<AnalogInput*>(IO));
    } else
    {
        if(Dt==data_type::Discrete)
            m_discreteOutputs.append(dynamic_cast<DiscreteOutput*>(IO));
        else
            m_analogOutputs.append(dynamic_cast<AnalogOutput*>(IO));
    }
    //m_Main->addContact(IO->contact());
}

void Module::removeIO(IOClass *IO)
{
    IO_type IOt = IO->IOType();
    data_type Dt = IO->dataType();
    if(IOt==IO_type::Input)
    {
        if(Dt==data_type::Discrete)
            m_discreteInputs.removeAll(dynamic_cast<DiscreteInput*>(IO));
        else
            m_analogInputs.removeAll(dynamic_cast<AnalogInput*>(IO));
    } else
    {
        if(Dt==data_type::Discrete)
            m_discreteOutputs.removeAll(dynamic_cast<DiscreteOutput*>(IO));
        else
            m_analogOutputs.removeAll(dynamic_cast<AnalogOutput*>(IO));
    }
    m_Main->removeContact(IO->contact());
}
