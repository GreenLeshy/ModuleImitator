#ifndef CUSTOM_H
#define CUSTOM_H

#include "Module.h"
#include <QScrollArea>

class MemoryView;
class IOConfigurator;
class IOClass;
class Custom : public Module    //Универсальный настраиваемый модуль
{
public:
    Custom(QWidget *parent, uchar addr);
    ~Custom();

    QLineEdit *addrWidget() const;
    void setAddrWidget(QLineEdit *newAddrWidget);
    virtual QJsonObject toJson(void) override;               //Р’РѕР·РІСЂР°С‰Р°РµС‚ РёРЅС„РѕСЂРјР°С†РёСЋ РѕР± СЌР»РµРјРµРЅС‚Рµ РІ РІРёРґРµ QJsonObject
    virtual void fromJson(const QJsonObject  &jmod) override;    //Р—Р°РіСЂСѓР¶Р°РµС‚ РёРЅС„РѕСЂРјР°С†РёСЋ РѕР± СЌР»РµРјРµРЅС‚Рµ РёР· QJsonObject
    virtual QVector<Contact *> depends(Contact *cont) override;

public slots:
    void memAddrChanged(void);          //Пользователь изменил начальный адрес
    void memEndChanged(void);            //Пользователь изменил конечный адрес
    void showMemory(void);                  //Пользователь нажал на кнопку, показывающуу память
    void addContact(void);                      //Пользователь нажал на кнопку добавления контакта
    void confsButtonPressed(void);         //Пользователь нажал на кнопку конфигурирования контакта
    void confsDelButtonPressed(void);    //Пользователь нажал на кнопку удаления контакта
    void IOCreated(IOClass *IO);             //Был создан новый контакт
    void save(void);                                  //Пользователь нажал на кнопку сохранения модуля

protected:
    virtual void memoryInitialisation() override;
    virtual void resizeEvent(QResizeEvent *event) override; //Переопределяет стандартную функцию для адекватного отображения области контактов

protected:
    QLineEdit *m_addrWidget;
    QLineEdit *m_endWidget;
    int m_newMemAddr;
    int m_newMemEnd;
    MemoryView *m_view;
    QPushButton *m_showMemory;
    QPushButton *m_saveModule;
    QScrollArea *m_scrollArea;
    QVBoxLayout *m_areaLayout;
    QVector<IOConfigurator *> m_confs;
    QVector<QPushButton *> m_confsButtons;
    QVector<QPushButton *> m_confsDelButtons;
};

#endif // CUSTOM_H
