#include "AddTabM.h"
#include "Modules.h"

AddTabM::AddTabM(QWidget *parent, QString name) : Module(parent)
{
    blockSignals(true);
    m_type=Modules::getTypeName(Modules::NO_TYPE);
    m_typeBox->setCurrentText(m_type);
    m_nicknameEdit->setText(name);
    m_useNickname->setChecked(true);
    m_name="+";
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}
