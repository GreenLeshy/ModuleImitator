#ifndef DI_33A_H
#define DI_33A_H

#include "Module.h"

class DI_33A : public Module
{
    Q_OBJECT
public:
    DI_33A(QWidget *parent, uchar addr);

public slots:
    virtual void inputsChaneged(void) override;
    virtual void readInputs(void) override;
    void writeHistory(void);

protected:
    virtual void memoryInitialisation() override;

protected:
 uint16_t *m_valueIN;
 uint16_t *m_frontUp;
 uint16_t *m_frontDown;
 QTimer *m_timer;                       //Таймер для 2,5-секундного интервала определения фронтов
 QQueue<uchar> m_history;      //История значений для определения фронтов
 uint16_t *m_moduleType;
 uint16_t *m_progVer;
};

#endif // DI_33A_H
