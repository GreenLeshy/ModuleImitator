#include "DIO_32A.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

DIO_32A::DIO_32A(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=8;
    m_type=Modules::getTypeName(Modules::DIO_32A);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);

    QString name;
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("IN").append(QString::number(i));
        DiscreteInput *DI = new DiscreteInput(this, name);
        m_interfaceLayout->addLayout(DI->layout());
        m_discreteInputs.append(DI);
    }
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("OUT").append(QString::number(i));
        DiscreteOutput *DO = new DiscreteOutput(this, name);
        m_interfaceLayout->addLayout(DO->layout());
        m_discreteOutputs.append(DO);
    }

    m_timer=new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &DIO_32A::writeHistory);
    m_timer->start(500);

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

void DIO_32A::fromJson(const QJsonObject &jsn)
{
    this->Module::fromJson(jsn);
    int buf=0;
    for(int i =0; i<m_n;++i)
            buf=buf+(m_discreteOutputs.at(i)->value()<<i);
    *m_valueOUT=buf;
}

void DIO_32A::inputsChaneged()
{
    readInputs();
}

void DIO_32A::readInputs()
{
    *m_valueIN=0;
    for(int i=0; i<m_n; ++i)
    {
        *m_valueIN+=int(m_discreteInputs.at(i)->value())<<i;
    }
}

void DIO_32A::readOutputs(void)
{
    bool buf1;
    int val = *m_valueOUT;
    for(int i=0; i<m_n; ++i)
    {
        buf1=(val&1);
            m_discreteOutputs.at(i)->setValue(buf1);
        val=val>>1;
    }
    *m_valueOUTCopy=*m_valueOUT;
}

void DIO_32A::writeHistory()
{
    m_history.enqueue(*m_valueIN);
    if(m_history.size()>5)
    {
        uint16_t buf = m_history.dequeue();
        uint16_t buf2 = *m_valueIN^buf;         //Поиск изменившихся битов
        *m_frontUp = (buf2)&(*m_valueIN);     //Определение направления изменения
        *m_frontDown = (buf2)&(buf);             //Определение направления изменения
    }
}

void DIO_32A::memoryInitialisation()
{
    //00 Тип модуля <m_moduleType = 15>
    //01 Индикатор ошибок <Не используетcя, =0>
    //02 Индикатор прогресса <m_wrkInd>
    //03 Отфильтрованное значение входов (8 бит)  <m_valueIn>
    //04 Копия состояния выходов (8 бит)  <Копия m_valueOUT>
    //05 Регистр состояния выходов <Не используетcя, =0>
    //06 Положительный фронт (8 бит, задержка 2,5 с) <m_frontUp>
    //07 Отрицательный фронт (8 бит, задержка 2,5 с) <m_frontDown>
    //08 Индикаторы КЗ на входах <Не используетcя, =0>
    //09 Индикаторы обрава линии на входах <Не используетcя, =0>
    //10 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //11 Счетчик сбросов по питанию <Не используетcя, =0>
    //12 Программная версия <m_progVer, =32 65>
    //13 Состояние выходов (8 бит) [m_valueOUT] <m_valueOUT>
    m_mem.clear();

    m_memAddr=0;
    m_memEnd=13;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_moduleType= &m_mem.data()[0];
    *m_moduleType=Static::toU16(0, 15);
    m_wrkInd=&m_mem.data()[2];
    m_progVer= &m_mem.data()[12];
    *m_progVer=Static::toU16(32, 65);
    m_valueOUT= &m_mem.data()[13];
    m_valueOUTCopy= &m_mem.data()[4];

    m_valueIN = &m_mem.data()[3];
    m_frontUp = &m_mem.data()[6];
    m_frontDown = &m_mem.data()[7];
}
