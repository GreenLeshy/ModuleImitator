#include "AI_32A.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

AI_32A::AI_32A(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=12;
    m_type=Modules::getTypeName(Modules::AI_32A);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);
    QString name;
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("IN").append(QString::number(i));
        AnalogInput *AI = new AnalogInput(this, name, 0, 20, 4, 20);
        m_interfaceLayout->addLayout(AI->layout());
        m_analogInputs.append(AI);
    }

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

}

void AI_32A::inputsChaneged()
{
    readInputs();
}

void AI_32A::readInputs()
{
    for(int i=0; i<m_n; ++i)
    {
        *m_inputValueList[i]=Static::scaleToInt(m_analogInputs.at(i)->value(), 0, 20, ANALOG14_MINIMUM, ANALOG14_MAXIMUM, true);
    }
}
void AI_32A::memoryInitialisation()
{
    //00 Тип модуля <m_moduleType = 11>
    //01 Индикатор ошибок <Не используетcя, =0>
    //02 Индикатор прогресса <m_wrkInd>
    //03 Отфильтрованное значение канала 0 (14 разрядов)
    //04 Отфильтрованное значение канала 1 (14 разрядов)
    //05 Отфильтрованное значение канала 2 (14 разрядов)
    //06 Отфильтрованное значение канала 3 (14 разрядов)
    //07 Отфильтрованное значение канала 4 (14 разрядов)
    //08 Отфильтрованное значение канала 5 (14 разрядов)
    //09 Отфильтрованное значение канала 6 (14 разрядов)
    //10 Отфильтрованное значение канала 7 (14 разрядов)
    //11 Отфильтрованное значение канала 8 (14 разрядов)
    //12 Отфильтрованное значение канала 9 (14 разрядов)
    //13 Отфильтрованное значение канала 10 (14 разрядов)
    //14 Отфильтрованное значение канала 11 (14 разрядов)
    //15 Регистр состояния входов <Не используетcя, =0>
    //16 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //17 Счетчик сбросов по питанию <Не используетcя, =0>
    //18 Программная версия <m_progVer, =6 22>
    //19 Значение реперного сигнала RP1 <325>
    //20 Значение реперного сигнала RP2 <8235>
    //21 Значение реперного сигнала RP3 <15955>
    //22 Значение реперного сигнала RP4 <0>
    m_mem.clear();

    m_memAddr=0;
    m_memEnd=22;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_moduleType= &m_mem.data()[0];
    *m_moduleType=Static::toU16(0, 11);
    m_wrkInd=&m_mem.data()[2];
    for(int i=0; i<m_n; ++i)
    {
        m_inputValueList.append(0);
        m_inputValueList[i]=&m_mem.data()[3+i];
    }
    m_progVer= &m_mem.data()[18];
    *m_progVer=Static::toU16(6, 22);
    m_mem.data()[19]=325;
    m_mem.data()[20]=8235;
    m_mem.data()[21]=15955;
}
