#include "AO_31A.h"
#include "Contact.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

AO_31A::AO_31A(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=4;
    m_type=Modules::getTypeName(Modules::AO_31A);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);

    QString name;
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("OUT").append(QString::number(i));
        AnalogOutput *AO = new AnalogOutput(this, name, 0, 20, 4, 20);
        m_interfaceLayout->addLayout(AO->layout());
        m_analogOutputs.append(AO);
    }
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

void AO_31A::readOutputs(void)
{
    for(int i=0; i<m_n; ++i)
    {
        double buf2 = Static::scaleToDouble(int(*m_outputValueList.at(i)), ANALOG12_MINIMUM, ANALOG12_MAXIMUM, 0, 20, true);
        m_analogOutputs.at(i)->setValue(buf2);
        *m_outputValueListCopy[i]=*m_outputValueList.at(i);
        *m_outputValueListMeasure[i]=*m_outputValueList.at(i);
    }
}

void AO_31A::memoryInitialisation()
{
    //00 Тип модуля <m_moduleType = 12>
    //01 Индикатор ошибок <Не используетcя, =0>
    //02 Индикатор прогресса <m_wrkInd>
    //03 Копия записанного значения канала 0 (12 разрядов)
    //04 Копия записанного значения канала 1 (12 разрядов)
    //05 Копия записанного значения канала 2 (12 разрядов)
    //06 Копия записанного значения канала 3 (12 разрядов)
    //07 Регистр состояния входов <Не используетcя, =0>
    //08 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //09 Счетчик сбросов по питанию <Не используетcя, =0>
    //10 Программная версия <m_progVer, =31 60>
    //11 Считанное значение канала 0 (12 разрядов)
    //12 Считанное значение канала 1 (12 разрядов)
    //13 Считанное значение канала 2 (12 разрядов)
    //14 Считанное значение канала 3 (12 разрядов)
    //15 Не исп
    //16 Записываемое значение выхода 0 (12 разрядов)
    //17 Записываемое значение выхода 1 (12 разрядов)
    //18 Записываемое значение выхода 2 (12 разрядов)
    //19 Записываемое значение выхода 3 (12 разрядов)
    m_mem.clear();

    m_memAddr=0;
    m_memEnd=19;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_moduleType= &m_mem.data()[0];
    *m_moduleType=Static::toU16(0, 12);
    m_wrkInd=&m_mem.data()[2];
    for(int i=0; i<m_n; ++i)
    {
        m_outputValueList.append(0);
        m_outputValueList[i]=&m_mem.data()[16+i];
        m_outputValueListCopy.append(0);
        m_outputValueListCopy[i]=&m_mem.data()[3+i];
        m_outputValueListMeasure.append(0);
        m_outputValueListMeasure[i]=&m_mem.data()[11+i];
    }
    m_progVer= &m_mem.data()[10];
    *m_progVer=Static::toU16(31, 60);
}
