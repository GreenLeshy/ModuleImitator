#ifndef AO_31A_H
#define AO_31A_H

#include "Module.h"

class AO_31A : public Module
{
public:
    AO_31A(QWidget *parent, uchar addr);

public slots:
    virtual void readOutputs(void) override; //Чтение требуемого состояния выходов

protected:
    virtual void memoryInitialisation() override;

protected:
    QVector<uint16_t*> m_outputValueList;
    QVector<uint16_t*> m_outputValueListCopy;
    QVector<uint16_t*> m_outputValueListMeasure;
    uint16_t *m_moduleType;
    uint16_t *m_progVer;
};

#endif // AO_31A_H
