#ifndef AI_33A_H
#define AI_33A_H

#include "Module.h"

class AI_33A : public Module
{
    Q_OBJECT
public:
    AI_33A(QWidget *parent, uchar addr);

public slots:
    virtual void inputsChaneged(void) override;
    virtual void readInputs(void) override;

protected:
    virtual void memoryInitialisation() override;

protected:
 QVector<uint16_t*> m_inputValueList;
 uint16_t *m_moduleType;
 uint16_t *m_progVer;
};

#endif // AI_33A_H
