#ifndef DO_31A_H
#define DO_31A_H

#include "Module.h"

class DO_31A : public Module
{
    Q_OBJECT
public:
    DO_31A(QWidget *parent, uchar addr);

public slots:
    virtual void readOutputs(void) override; //Чтение требуемого состояния выходов

protected:
    virtual void memoryInitialisation() override;

protected:
uint16_t *m_valueOUT;
uint16_t *m_valueOUTCopy;
uint16_t *m_moduleType;
uint16_t *m_progVer;
};

#endif // DO_31A_H
