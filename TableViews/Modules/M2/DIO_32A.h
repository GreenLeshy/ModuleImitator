#ifndef DIO_32A_H
#define DIO_32A_H

#include <Module.h>

class DIO_32A : public Module
{
    Q_OBJECT
public:
    DIO_32A(QWidget *parent, uchar addr);
    virtual void fromJson(const QJsonObject  &jsn) override;

public slots:
    virtual void inputsChaneged(void) override;
    virtual void readInputs(void) override;
    virtual void readOutputs(void) override; //Чтение требуемого состояния выходов
    //virtual void newMessage(QByteArray const &text, bool check) override;
    void writeHistory(void);

protected:
    virtual void memoryInitialisation() override;

protected:
 uint16_t  *m_valueIN;
 uint16_t  *m_valueOUT;
 uint16_t *m_valueOUTCopy;
 uint16_t *m_frontUp;
 uint16_t *m_frontDown;
 uint16_t *m_moduleType;
 uint16_t *m_progVer;
 QTimer *m_timer;                       //Таймер для 2,5-секундного интервала определения фронтов
 QQueue<uchar> m_history;      //История значений для определения фронтов
};


#endif // DIO_32A_H
