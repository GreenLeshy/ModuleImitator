#include "AI_31A.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

AI_31A::AI_31A(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=6;
    m_type=Modules::getTypeName(Modules::AI_31A);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);
    QString name;
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("IN").append(QString::number(i));
        AnalogInput *AI = new AnalogInput(this, name, 0, 20, 4, 20);
        m_interfaceLayout->addLayout(AI->layout());
        m_analogInputs.append(AI);
    }

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

}

void AI_31A::inputsChaneged()
{
    readInputs();
}

void AI_31A::readInputs()
{
    for(int i=0; i<m_n; ++i)
    {
        *m_inputValueList[i]=Static::scaleToInt(m_analogInputs.at(i)->value(), 0, 20, ANALOG12_MINIMUM, ANALOG12_MAXIMUM, true);
    }
}
void AI_31A::memoryInitialisation()
{
    //00 Тип модуля <m_moduleType = 10>
    //01 Индикатор ошибок <Не используетcя, =0>
    //02 Индикатор прогресса <m_wrkInd>
    //03 Отфильтрованное значение канала 0 (12 разрядов)
    //04 Отфильтрованное значение канала 1 (12 разрядов)
    //05 Отфильтрованное значение канала 2 (12 разрядов)
    //06 Отфильтрованное значение канала 3 (12 разрядов)
    //07 Отфильтрованное значение канала 4 (12 разрядов)
    //08 Отфильтрованное значение канала 5 (12 разрядов)
    //9 Регистр состояния входов <Не используетcя, =0>
    //10 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //11 Счетчик сбросов по питанию <Не используетcя, =0>
    //12 Программная версия <m_progVer, =31 08>
    //13 Значение реперного сигнала RP1 <1000>
    //14 Значение реперного сигнала RP2 <3000>
    m_mem.clear();

    m_memAddr=0;
    m_memEnd=14;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_moduleType= &m_mem.data()[0];
    *m_moduleType=Static::toU16(0, 10);
    m_wrkInd=&m_mem.data()[2];
    for(int i=0; i<m_n; ++i)
    {
        m_inputValueList.append(0);
        m_inputValueList[i]=&m_mem.data()[3+i];
    }
    m_progVer= &m_mem.data()[12];
    *m_progVer=Static::toU16(31, 8);
    m_mem.data()[13]=1000;
    m_mem.data()[14]=3000;
}
