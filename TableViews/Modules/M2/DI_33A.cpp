#include "DI_33A.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

DI_33A::DI_33A(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=16;
    m_type=Modules::getTypeName(Modules::DI_33A);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);
    QString name;
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("IN").append(QString::number(i));
        DiscreteInput *DI = new DiscreteInput(this, name);
        m_interfaceLayout->addLayout(DI->layout());
        m_discreteInputs.append(DI);
    }

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

    m_timer=new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &DI_33A::writeHistory);
    m_timer->start(500);
}

void DI_33A::inputsChaneged()
{
    readInputs();
}

void DI_33A::readInputs()
{
    *m_valueIN=0;
    for(int i=0; i<m_n; ++i)
    {
        *m_valueIN+=uint16_t(m_discreteInputs.at(i)->value())<<i;
    }
}

void DI_33A::writeHistory()
{
    m_history.enqueue(*m_valueIN);
    if(m_history.size()>5)
    {
        uint16_t buf = m_history.dequeue();
        uint16_t buf2 = *m_valueIN^buf;         //Поиск изменившихся битов
        *m_frontUp = (buf2)&(*m_valueIN);     //Определение направления изменения
        *m_frontDown = (buf2)&(buf);             //Определение направления изменения
    }
}

void DI_33A::memoryInitialisation()
{
    //00 Тип модуля <m_moduleType = 20>
    //01 Индикатор ошибок <Не используетcя, =0>
    //02 Индикатор прогресса <m_wrkInd>
    //03 Отфильтрованное значение входов (16 бит)  <m_valueIn>
    //04 Положительный фронт (16 бит, задержка 2,5 с) <m_frontUp>
    //05 Отрицательный фронт (16 бит, задержка 2,5 с) <m_frontDown>
    //06 Регистр состояния входов <Не используетcя, =0>
    //07 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //08 Счетчик сбросов по питанию <Не используетcя, =0>
    //09 Программная версия <m_progVer, =33 41>
    m_mem.clear();
    m_memAddr=0;
    m_memEnd=9;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_moduleType= &m_mem.data()[0];
    *m_moduleType=Static::toU16(0, 20);
    m_wrkInd=&m_mem.data()[2];
    m_valueIN = &m_mem.data()[3];
    m_frontUp = &m_mem.data()[4];
    m_frontDown = &m_mem.data()[5];
    m_progVer= &m_mem.data()[9];
    *m_progVer=Static::toU16(33, 41);
}
