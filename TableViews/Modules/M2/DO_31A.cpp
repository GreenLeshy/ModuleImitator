#include "DO_31A.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

DO_31A::DO_31A(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=16;
    m_type=Modules::getTypeName(Modules::DO_31A);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);
    QString name;

    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("OUT").append(QString::number(i));
        DiscreteOutput *DO = new DiscreteOutput(this, name);
        m_interfaceLayout->addLayout(DO->layout());
        m_discreteOutputs.append(DO);
    }
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

void DO_31A::readOutputs(void)
{
    bool buf1;
    uint val = *m_valueOUT;
    for(int i=0; i<m_n; ++i)
    {
        buf1=(val&1);
            m_discreteOutputs.at(i)->setValue(buf1);
        val=val>>1;
    }
    *m_valueOUTCopy=*m_valueOUT;
}

void DO_31A::memoryInitialisation()
{
    //00 Тип модуля <m_moduleType = 13>
    //01 Индикатор ошибок <Не используетcя, =0>
    //02 Индикатор прогресса <m_wrkInd>
    //03 Копия состояния выходов (16 бит)  <Копия m_valueOUT>
    //04 Регистр состояния выходов <Не используетcя, =0>
    //05 Аварии нагрузок на выходах
    //06 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //07 Счетчик сбросов по питанию <Не используетcя, =0>
    //08 Программная версия <m_progVer, =31 50>
    //09 Не используетcя
    //10 Не используетcя
    //11 Состояние выходов (16 бит) [m_valueOUT] <m_valueOUT>
    m_mem.clear();
    m_memAddr=0;
    m_memEnd=11;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_moduleType= &m_mem.data()[0];
    *m_moduleType=Static::toU16(0, 13);
    m_wrkInd=&m_mem.data()[2];
    m_progVer= &m_mem.data()[8];
    *m_progVer=Static::toU16(31, 50);
    m_valueOUT= &m_mem.data()[11];
    m_valueOUTCopy= &m_mem.data()[3];
}
