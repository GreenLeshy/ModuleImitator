#include "AIO_31A.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

AIO_31A::AIO_31A(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=3;
    m_type=Modules::getTypeName(Modules::AIO_31A);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);
    QString name;
    for(int i=1; i<m_n*2+1; ++i)
    {
        name=QString("IN").append(QString::number(i));
        AnalogInput *AI = new AnalogInput(this, name, 0, 20, 4, 20);
        m_interfaceLayout->addLayout(AI->layout());
        m_analogInputs.append(AI);
    }
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("OUT").append(QString::number(i));
        AnalogOutput *AO = new AnalogOutput(this, name, 0, 20, 4, 20);
        m_interfaceLayout->addLayout(AO->layout());
        m_analogOutputs.append(AO);
    }

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

}

void AIO_31A::inputsChaneged()
{
    readInputs();
}

void AIO_31A::readInputs()
{
    for(int i=0; i<m_n*2; ++i)
    {
        *m_inputValueList[i]=Static::scaleToInt(m_analogInputs.at(i)->value(), 0, 20, ANALOG14_MINIMUM, ANALOG14_MAXIMUM, true);
    }
}

void AIO_31A::readOutputs()
{
    for(int i=0; i<m_n; ++i)
    {
        double buf2 = Static::scaleToDouble(int(*m_outputValueList.at(i)), ANALOG12_MINIMUM, ANALOG12_MAXIMUM, 0, 20, true);
        m_analogOutputs.at(i)->setValue(buf2);
        *m_outputValueListCopy[i]=*m_outputValueList.at(i);
        *m_outputValueListMeasure[i]=*m_outputValueList.at(i);
    }
}
void AIO_31A::memoryInitialisation()
{
    //00 Тип модуля <m_moduleType = 18>
    //01 Индикатор ошибок <Не используетcя, =0>
    //02 Индикатор прогресса <m_wrkInd>
    //03 Отфильтрованное значение канала 0 (14 разрядов)
    //04 Отфильтрованное значение канала 1 (14 разрядов)
    //05 Отфильтрованное значение канала 2 (14 разрядов)
    //06 Отфильтрованное значение канала 3 (14 разрядов)
    //07 Отфильтрованное значение канала 4 (14 разрядов)
    //08 Отфильтрованное значение канала 5 (14 разрядов)
    //09 Копия записанного значения канала 0 (12 разрядов)
    //10 Копия записанного значения канала 1 (12 разрядов)
    //11 Копия записанного значения канала 2 (12 разрядов)
    //12 Регистр состояния выходов <Не используетcя, =0>
    //13 Регистр состояния входов <Не используетcя, =0>
    //14 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //15 Счетчик сбросов по питанию <Не используетcя, =0>
    //16 Программная версия <m_progVer, =31 41>
    //17 Значение реперного сигнала REP <Неизвестно>
    //18 Считанное значение канала вывода 0 (12 разрядов)
    //19 Считанное значение канала вывода 1 (12 разрядов)
    //20 Считанное значение канала вывода 2 (12 разрядов)
    //21 Записываемое значение выхода 0 (12 разрядов)
    //22 Записываемое значение выхода 1 (12 разрядов)
    //23 Записываемое значение выхода 2 (12 разрядов)
    m_mem.clear();

    m_memAddr=0;
    m_memEnd=23;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_moduleType= &m_mem.data()[0];
    *m_moduleType=Static::toU16(0, 18);
    m_wrkInd=&m_mem.data()[2];
    for(int i=0; i<m_n*2; ++i)
    {
        m_inputValueList.append(0);
        m_inputValueList[i]=&m_mem.data()[3+i];
    }
    for(int i=0; i<m_n; ++i)
    {
        m_outputValueList.append(0);
        m_outputValueList[i]=&m_mem.data()[21+i];
        m_outputValueListCopy.append(0);
        m_outputValueListCopy[i]=&m_mem.data()[9+i];
        m_outputValueListMeasure.append(0);
        m_outputValueListMeasure[i]=&m_mem.data()[18+i];
    }
    m_progVer= &m_mem.data()[16];
    *m_progVer=Static::toU16(31, 41);
    m_mem.data()[17]=ANALOG14_MAXIMUM/2;
}

