#ifndef AIO_31A_H
#define AIO_31A_H

#include <Module.h>

class AIO_31A : public Module
{
    Q_OBJECT
public:
    AIO_31A(QWidget *parent, uchar addr);

public slots:
    virtual void inputsChaneged(void) override;
    virtual void readInputs(void) override;
    virtual void readOutputs(void) override; //Чтение требуемого состояния выходов

protected:
    virtual void memoryInitialisation() override;

protected:
 QVector<uint16_t*> m_inputValueList;
 QVector<uint16_t*> m_outputValueList;
 QVector<uint16_t*> m_outputValueListCopy;
 QVector<uint16_t*> m_outputValueListMeasure;
 uint16_t *m_moduleType;
 uint16_t *m_progVer;
};

#endif // AIO_31A_H
