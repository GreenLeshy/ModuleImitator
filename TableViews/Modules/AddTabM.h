#ifndef ADDTABM_H
#define ADDTABM_H

#include "Module.h"

class AddTabM : public Module //Модуль, который ничего не делает, но нужен для работы логики добавления модулей
{
    Q_OBJECT
public:
    AddTabM(QWidget *parent, QString name = "+");    //Вкладка, при переходе на которую добавляется новый модуль
};

#endif // ADDTABM_H
