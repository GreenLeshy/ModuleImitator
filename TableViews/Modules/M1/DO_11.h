#ifndef DO_11_H
#define DO_11_H

#include "Module.h"

class DiscreteOutput;
class DO_11 : public Module
{
public:
    DO_11(QWidget *parent, uchar addr);

public slots:
    virtual void readOutputs(void) override; //Чтение требуемого состояния выходов
    //virtual void newMessage(QByteArray const &text, bool check) override;

protected:
    virtual void memoryInitialisation() override;

protected:
uint16_t *m_valueOUT;
uint16_t *m_valueOUTCopy;
};

#endif // DO_11_H
