#ifndef DI_11_H
#define DI_11_H

#include "Module.h"

class DiscreteInput;
class DI_11 : public Module
{
    Q_OBJECT

public:
    DI_11(QWidget *parent, uchar addr);

public slots:
    virtual void inputsChaneged(void) override;
    virtual void readInputs(void) override;
    //virtual void newMessage(QByteArray const &text, bool check) override;
    void writeHistory(void);

protected:
    virtual void memoryInitialisation() override;

protected:
 uint16_t *m_valueIN;
 uint16_t *m_frontUp;
 uint16_t *m_frontDown;
 QTimer *m_timer;                       //Таймер для 2,5-секундного интервала определения фронтов
 QQueue<uchar> m_history;      //История значений для определения фронтов
};

#endif // DI_11_H
