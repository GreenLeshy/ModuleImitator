#include "AO_11.h"
#include "Contact.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

AO_11::AO_11(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=2;
    m_type=Modules::getTypeName(Modules::AO_11);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);

    QString name;
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("OUT").append(QString::number(i));
        AnalogOutput *AO = new AnalogOutput(this, name, 0, 20, 4, 20);
        m_interfaceLayout->addLayout(AO->layout());
        m_analogOutputs.append(AO);
    }
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

void AO_11::readOutputs(void)
{
    for(int i=0; i<m_n; ++i)
    {
        double buf2 = Static::scaleToDouble(int(*m_outputValueList.at(i)), ANALOG12_MINIMUM, ANALOG12_MAXIMUM, 0, 20, true);
        m_analogOutputs.at(i)->setValue(buf2);
        *m_outputValueListCopy[i]=*m_outputValueList.at(i);
    }
}

void AO_11::memoryInitialisation()
{
    //00 Копия регистра 09 (12 разрядов)
    //01 Копия регистра 10 (12 разрядов)
    //02 Не исп
    //03 Не исп
    //04 Индикатор ошибок (2: ошибка Flash, 3: ошибка SRAM, 4: ошибка EEPROM) <Не используется, =0>
    //05 Не исп
    //06 Сетевой адрес, считанный с джамперов <m_address>
    //07 Тип модуля <Не используетcя, =0>
    //08 Программная версия <Не используетcя, =0>
    //09 Уровень тока в канале 1 (12 бит)
    //10 Уровень тока в канале 2 (12 бит)
    m_mem.clear();
    m_memAddr=0;
    m_memEnd=10;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_address= &m_mem.data()[6];
    *m_address=rez_address;

    for(int i=0; i<m_n; ++i)
    {
        m_outputValueList.append(0);
        m_outputValueList[i]=&m_mem.data()[9+i];
        m_outputValueListCopy.append(0);
        m_outputValueListCopy[i]=&m_mem.data()[i];
    }
}
