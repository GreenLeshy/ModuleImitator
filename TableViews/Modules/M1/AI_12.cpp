#include "AI_12.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

AI_12::AI_12(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=4;
    m_type=Modules::getTypeName(Modules::AI_12);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);
    QString name;
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("IN").append(QString::number(i));
        AnalogInput *AI = new AnalogInput(this, name, 0, 20, 4, 20);
        m_interfaceLayout->addLayout(AI->layout());
        m_analogInputs.append(AI);
    }

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна

}

void AI_12::inputsChaneged()
{
    readInputs();
}

void AI_12::readInputs()
{
    for(int i=0; i<m_n; ++i)
    {
        *m_inputValueList[i]=Static::scaleToInt(m_analogInputs.at(i)->value(), 0, 20, ANALOG12_MINIMUM, ANALOG12_MAXIMUM, true);
    }
}

void AI_12::memoryInitialisation()
{
    //00 Отфильтрованное значение канала 0 (12 разрядов)
    //01 Отфильтрованное значение канала 1 (12 разрядов)
    //02 Отфильтрованное значение канала 2 (12 разрядов)
    //03 Отфильтрованное значение канала 3 (12 разрядов)
    //04 Не исп
    //05 Не исп
    //06 Не исп
    //07 Не исп
    //08 Индикатор прогресса <m_wrkInd>
    //09 Не исп
    //10 Счетчик внешних сбросов (по охранному таймеру) <Не используется, =0>
    //11 Индикатор ошибок (2: ошибка Flash, 3: ошибка SRAM, 4: ошибка EEPROM) <Не используется, =0>
    //12 Счетчик сбросов по питанию  <Не используется, =0>
    //13 Сетевой адрес, считанный с джамперов <m_address>
    //14 Тип модуля <Не используетcя, =0>
    //15 Программная версия <Не используетcя, =0>
    m_mem.clear();
    m_memAddr=0;
    m_memEnd=15;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_address= &m_mem.data()[13];
    *m_address=rez_address;
    for(int i=0; i<m_n; ++i)
    {
        m_inputValueList.append(0);
        m_inputValueList[i]=&m_mem.data()[i];
    }
    m_wrkInd=&m_mem.data()[8];
}
