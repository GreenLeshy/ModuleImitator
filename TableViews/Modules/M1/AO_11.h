#ifndef AO_11_H
#define AO_11_H

#include "Module.h"
class AO_11 : public Module
{
public:
    AO_11(QWidget *parent, uchar addr);

public slots:
    virtual void readOutputs(void) override; //Чтение требуемого состояния выходов
    //virtual void newMessage(QByteArray const &text, bool check) override;

protected:
    virtual void memoryInitialisation() override;

protected:
    QVector<uint16_t*> m_outputValueList;
    QVector<uint16_t*> m_outputValueListCopy;
};

#endif // AO_11_H
