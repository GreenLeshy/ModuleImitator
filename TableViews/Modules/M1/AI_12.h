#ifndef AI_12_H
#define AI_12_H

#include "Module.h"

class AI_12 : public Module
{
public:
    AI_12(QWidget *parent, uchar addr);

public slots:
    virtual void inputsChaneged(void) override;
    virtual void readInputs(void) override;

protected:
    virtual void memoryInitialisation() override;

protected:
 QVector<uint16_t*> m_inputValueList;
};

#endif // AI_12_H
