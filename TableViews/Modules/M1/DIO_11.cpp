#include "DIO_11.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

DIO_11::DIO_11(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=4;
    m_type=Modules::getTypeName(Modules::DIO_11);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);

    QString name;
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("IN").append(QString::number(i));
        DiscreteInput *DI = new DiscreteInput(this, name);
        m_interfaceLayout->addLayout(DI->layout());
        m_discreteInputs.append(DI);
    }
    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("OUT").append(QString::number(i));
        DiscreteOutput *DO = new DiscreteOutput(this, name);
        m_interfaceLayout->addLayout(DO->layout());
        m_discreteOutputs.append(DO);
    }

    m_timer=new QTimer(this);
    connect(m_timer, &QTimer::timeout, this, &DIO_11::writeHistory);
    m_timer->start(500);

    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

void DIO_11::fromJson(const QJsonObject &jsn)
{
    this->Module::fromJson(jsn);
    int buf=0;
    for(int i =0; i<m_n;++i)
            buf=buf+(m_discreteOutputs.at(i)->value()<<i);
    *m_valueOUT=buf;
}

void DIO_11::inputsChaneged()
{
    readInputs();
}

void DIO_11::readInputs()
{
    *m_valueIN=0;
    for(int i=0; i<m_n; ++i)
    {
        *m_valueIN+=int(m_discreteInputs.at(i)->value())<<i;
    }
}

void DIO_11::readOutputs(void)
{
    bool buf1;
    int val = *m_valueOUT;
    for(int i=0; i<m_n; ++i)
    {
        buf1=(val&1);
            m_discreteOutputs.at(i)->setValue(buf1);
        val=val>>1;
    }
    *m_valueOUTCopy=*m_valueOUT;
}

void DIO_11::writeHistory()
{
    m_history.enqueue(*m_valueIN);
    if(m_history.size()>5)
    {
        uint16_t buf = m_history.dequeue();
        uint16_t buf2 = *m_valueIN^buf;         //Поиск изменившихся битов
        *m_frontUp = (buf2)&(*m_valueIN);     //Определение направления изменения
        *m_frontDown = (buf2)&(buf);             //Определение направления изменения
    }
}

void DIO_11::memoryInitialisation()
{
    //00 Отфильтрованное значение входов (4 бита)  <m_valueIn>
    //01 Положительный фронт (4 бита, задержка 2,5 с) <m_frontUp>
    //02 Отрицательный фронт (4 бита, задержка 2,5 с) <m_frontDown>
    //03 Копия состояния выходов (4 бит)  <Копия m_valueOUT>
    //04 Статус модуля (5 и 7 биты) <Не используетcя, =0>
    //05 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //06 Индикатор ошибок (2: ошибка Flash, 3: ошибка SRAM, 4: ошибка EEPROM) <Не используетcя, =0>
    //07 Счетчик сбросов по питанию <Не используетcя, =0>
    //08 Сетевой адрес, считанный с джамперов <m_address>
    //09 Тип модуля <Не используетcя, =0>
    //10 Программная версия <Не используетcя, =0>
    //11 Состояние выходов (4 бит) [m_valueOUT] <m_valueOUT>
    m_mem.clear();
    m_memAddr=0;
    m_memEnd=11;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_address= &m_mem.data()[8];
    *m_address=rez_address;
    m_valueIN = &m_mem.data()[0];
    m_frontUp = &m_mem.data()[1];
    m_frontDown = &m_mem.data()[2];
    m_valueOUT= &m_mem.data()[11];
    m_valueOUTCopy= &m_mem.data()[3];
}
