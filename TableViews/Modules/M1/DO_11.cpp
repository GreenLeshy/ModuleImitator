#include "DO_11.h"
#include "Modules.h"
#include "static.h"
#include "IOs.h"
#include "led.h"

DO_11::DO_11(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_n=8;
    m_type=Modules::getTypeName(Modules::DO_11);
    memoryInitialisation();
    m_typeBox->setCurrentText(m_type);
    QString name;

    for(int i=1; i<m_n+1; ++i)
    {
        name=QString("OUT").append(QString::number(i));
        DiscreteOutput *DO = new DiscreteOutput(this, name);
        m_interfaceLayout->addLayout(DO->layout());
        m_discreteOutputs.append(DO);
    }
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

void DO_11::readOutputs(void)
{
    bool buf1;
    uint val = *m_valueOUT;
    for(int i=0; i<m_n; ++i)
    {
        buf1=(val&1);
            m_discreteOutputs.at(i)->setValue(buf1);
        val=val>>1;
    }
    *m_valueOUTCopy=*m_valueOUT;
}

void DO_11::memoryInitialisation()
{
    //03 Копия состояния выходов (8 бит)  <Копия m_valueOUT>
    //04 Статус модуля (5 и 7 биты) <Не используетcя, =0>
    //05 Счетчик внешних сбросов (по охранному таймеру) <Не используетcя, =0>
    //06 Индикатор ошибок (2: ошибка Flash, 3: ошибка SRAM, 4: ошибка EEPROM) <Не используетcя, =0>
    //07 Счетчик сбросов по питанию <Не используетcя, =0>
    //08 Сетевой адрес, считанный с джамперов <m_address>
    //09 Тип модуля <Не используетcя, =0>
    //10 Программная версия <Не используетcя, =0>
    //11 Состояние выходов (8 бит) [m_valueOUT] <m_valueOUT>
    m_mem.clear();
    m_memAddr=3;
    m_memEnd=11;
    for(int i=m_memAddr; i<m_memEnd+1; ++i)
        m_mem.append(uint16_t(0));

    m_valueOUT= &m_mem.data()[8];
    m_valueOUTCopy= &m_mem.data()[0];
    m_address= &m_mem.data()[5];
    *m_address=rez_address;

}
