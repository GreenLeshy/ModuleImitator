#ifndef DIO_11_H
#define DIO_11_H

#include "Module.h"

class DiscreteInput;
class DiscreteOutput;
class DIO_11 : public Module
{
    Q_OBJECT

public:
    DIO_11(QWidget *parent, uchar addr);
    virtual void fromJson(const QJsonObject & jsn) override;

public slots:
    virtual void inputsChaneged(void) override;
    virtual void readInputs(void) override;
    virtual void readOutputs(void) override; //Чтение требуемого состояния выходов
    //virtual void newMessage(QByteArray const &text, bool check) override;
    void writeHistory(void);

protected:
    virtual void memoryInitialisation() override;

protected:
 uint16_t  *m_valueIN;
 uint16_t  *m_valueOUT;
 uint16_t *m_valueOUTCopy;
 uint16_t *m_frontUp;
 uint16_t *m_frontDown;
 QTimer *m_timer;                       //Таймер для 2,5-секундного интервала определения фронтов
 QQueue<uchar> m_history;      //История значений для определения фронтов
};

#endif // DIO_11_H
