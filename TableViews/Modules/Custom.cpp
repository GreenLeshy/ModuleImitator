#include "Custom.h"
#include "Modules.h"
#include "MemoryView.h"
#include "mainwindow.h"
#include "IOConfigurator.h"
#include "IOConfiguratorWindow.h"
#include "IOs.h"
#include "Contacts.h"

Custom::Custom(QWidget *parent, uchar addr) : Module(parent, addr)
{
    m_type=Modules::getTypeName(Modules::Custom);
    m_typeBox->setCurrentText(m_type);

    QHBoxLayout *HL;
    QLabel *label;

    HL=new QHBoxLayout;
    m_saveModule = new QPushButton("Сохранить модуль", this);
    HL->addWidget(m_saveModule);
    m_addressLayout->addLayout(HL);

    HL=new QHBoxLayout;
    m_showMemory = new QPushButton("Показать память", this);
    HL->addWidget(m_showMemory);
    m_interfaceLayout->addLayout(HL);

    HL=new QHBoxLayout;
    label = new QLabel("Первый адрес: ", this);
    HL->addWidget(label);
    m_addrWidget = new QLineEdit("0", this);
    HL->addWidget(m_addrWidget);
    m_interfaceLayout->addLayout(HL);

    HL=new QHBoxLayout;
    label = new QLabel("Последний адрес: ", this);
    HL->addWidget(label);
    m_endWidget = new QLineEdit("500", this);
    HL->addWidget(m_endWidget);
    m_interfaceLayout->addLayout(HL);

    m_newMemAddr=0;
    m_newMemEnd=500;
    memoryInitialisation();

    m_scrollArea = new QScrollArea(this);
    QWidget *w = new QWidget(m_scrollArea);
    m_scrollArea->setWidget(w);
    m_areaLayout = new QVBoxLayout(w);
    m_interfaceLayout->addWidget(m_scrollArea);
    m_scrollArea->setBackgroundRole(QPalette::Light);


    QPushButton *B = new QPushButton("+", m_scrollArea);
    m_areaLayout->addWidget(B);
    connect(B, &QPushButton::clicked, this, &Custom::addContact);
    B->setStyleSheet("QPushButton { background: rgb(200, 255, 200); selection-background-color: rgb(100, 155, 100); }");
    m_scrollArea->setMinimumWidth(450);
    m_scrollArea->setMinimumHeight(300);
    m_scrollArea->setWidgetResizable(true);

    connect(m_showMemory, &QPushButton::pressed, this, &Custom::showMemory);
    connect(m_saveModule, &QPushButton::pressed, this, &Custom::save);
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

Custom::~Custom()
{
    int size = m_confsDelButtons.size();
    for(int i=size-1; i>-1; --i)
        m_confsDelButtons.at(i)->click();
    /*size = m_discreteInputs.size();
    for(int i = size-1; i>=0; --i)
        delete m_discreteInputs.at(i);
    size = m_discreteOutputs.size();
    for(int i = size-1; i>=0; --i)
        delete m_discreteOutputs.at(i);
    size = m_analogInputs.size();
    for(int i = size-1; i>=0; --i)
        delete m_analogInputs.at(i);
    size = m_analogOutputs.size();
    for(int i = size-1; i>=0; --i)
        delete m_analogOutputs.at(i);*/
}

QJsonObject Custom::toJson()
{
    QJsonObject jmod = this->Module::toJson();
        QJsonObject obj;
        obj["start"]=m_memAddr;
        obj["end"]=m_memEnd;
        for(int i=m_memAddr; i<m_memEnd+1; ++i)
        {
            obj["reg"+QString::number(i)]=getRegister(i);
            obj["regName"+QString::number(i)]=getRegisterName(i);
        }
        jmod.insert("memory", obj);
        int size = m_confs.size();
        QJsonArray IOCs;
        for(int i =0; i<size; ++i)
        {
            QJsonObject IOC = m_confs.at(i)->toJson();
            IOCs.append(IOC);
        }
        jmod.insert("IOCs", IOCs);
    return jmod;
}

void Custom::fromJson(const QJsonObject &jmod)
{
    QJsonObject jsn = jmod;
    QString oldName=jsn["name"].toString();
    QString newName=jsn["newName"].toString();
    QString nameBuffer;
    QString newPair;
    QJsonArray newArr;
    QJsonObject IO;
    QJsonObject cont;
    if(newName!="" && newName!=oldName)
    {
        //Заранее переименовываем контакты, так как потом это будет выполнятся вне загрузки и вызовет зависание
        QJsonArray oldArr=jsn["IOCs"].toArray();
        int n = oldArr.size();
        for(int i=0; i<n; ++i)
        {
            IO=oldArr.at(i).toObject();
            cont=IO["contact"].toObject();
            nameBuffer=Static::writeTag(cont["name"].toString(), newName, '<', '>', ':');
            newPair=cont["pair"].toString();
            if(newPair.contains('{'+oldName+'}'))
            {
                newPair=Static::writeTag(cont["pair"].toString(), newName, '{', '}', ':');
            }
            else
            {
            if(newPair.contains('<'+oldName+'>'))
                newPair=Static::writeTag(newPair, newName, '<', '>', ':');
            }

            cont["name"]=nameBuffer;
            cont["pair"]=newPair;
            IO["contact"]=cont;
            newArr.append(IO);
        }
        jsn["IOCs"]=newArr;
        jsn["name"]=newName;
    }

    this->Module::fromJson(jsn);
    this->blockSignals(true);
        QJsonObject obj=jsn["memory"].toObject();
        m_newMemAddr=obj["start"].toInt();
        m_newMemEnd=obj["end"].toInt();
        memoryInitialisation();
        for(int i=m_memAddr; i<m_memEnd+1; ++i)
        {
            setRegister(i, obj["reg"+QString::number(i)].toInt());
            setRegisterName(i, obj["regName"+QString::number(i)].toString());
        }
        QJsonArray IOCs=jsn["IOCs"].toArray();
        int size = IOCs.size();
        for(int i =0; i<size; ++i)
        {
            addContact();
            m_confs.at(i)->fromJson(IOCs.at(i).toObject());
        }
        m_endWidget->setText(QString::number(m_memEnd));
        m_addrWidget->setText(QString::number(m_memAddr));

        this->blockSignals(false);
}

QVector<Contact *> Custom::depends(Contact *cont)
{
    QVector<Contact *> vect;
    IOClass *IO=cont->getIO();
    int size=m_confs.size();
    IOConfigurator *conf;
    for(int i=0; i<size;++i)
    {
        conf=m_confs.at(i);
        if(conf->IO()==IO)
        {
            IOConfigurator *conf2;
            for(int j=0; j<size;++j)
            {
                if(i!=j)
                {
                    conf2=m_confs.at(j);
                    if(conf->regAddr()==conf2->regAddr())
                        if(conf2->IOType()==IO_type::Output)        //Если два входа будут перезаписывать один и тот же регистр, это бессмысленно, но безопасно
                        {
                            if(conf->dataType()==data_type::Analog || conf2->dataType()==data_type::Analog)
                                vect.append(conf2->IO()->contact());
                            else
                                if(conf->bit()==conf2->bit())   //Два дискретных контакта могут находиться на разных битах одного байта
                                    vect.append(conf2->IO()->contact());
                        }
                }
            }
       }
    }
    return vect;
}

void Custom::memAddrChanged()
{
    int buf = m_addrWidget->text().toInt();
    if(buf<0 || buf>m_memEnd)
        m_addrWidget->setText(QString::number(m_memAddr));
    else
    {
        m_newMemAddr=buf;
        memoryInitialisation();
        m_Main->setSaved(false);
    }
}

void Custom::memEndChanged()
{
    int buf = m_endWidget->text().toInt();
    if(buf<m_memAddr)
        m_endWidget->setText(QString::number(m_memEnd));
    else
    {
        m_newMemEnd=buf;
        memoryInitialisation();
    }
}

void Custom::showMemory()
{
    m_view = new MemoryView(this);
    m_view->show();
}

void Custom::addContact()
{
    QHBoxLayout *HL;
    HL=new QHBoxLayout;
    //QPushButton *B = new QPushButton("IOC", m_scrollArea);
    QPushButton *B = new QPushButton(m_scrollArea);
    B->setIcon(QIcon("Шестеренка.png"));
    m_confsButtons.append(B);
    B->setMaximumWidth(40);
    HL->addWidget(B);
    connect(B, &QPushButton::clicked, this, &Custom::confsButtonPressed);
    IOConfigurator *IOC = new IOConfigurator(this);
    m_confs.append(IOC);
    B = new QPushButton("-", m_scrollArea);
    B->setMaximumWidth(20);
    B->setStyleSheet("QPushButton { background: rgb(255, 200, 200); selection-background-color: rgb(155, 100, 100); }");
    m_confsDelButtons.append(B);
    HL->addWidget(B);
    connect(B, &QPushButton::clicked, this, &Custom::confsDelButtonPressed);
    m_areaLayout->insertLayout(1, HL);
    connect(IOC, &IOConfigurator::IOCreated, this, &Custom::IOCreated);
    auto IO = IOC->createIO();
    m_scrollArea->setMinimumWidth(qMax(m_scrollArea->minimumWidth(), IO->width()+360));
    int width = qMax(m_scrollArea->minimumWidth(), m_mainLayuot->minimumSize().width());
    this->setMinimumSize(width, this->minimumHeight());     //Для правильного определения размера окна
}

void Custom::confsButtonPressed()
{
    int i = m_confsButtons.indexOf(static_cast<QPushButton *>(sender()));
    IOConfiguratorWindow *wind = new IOConfiguratorWindow(m_confs.at(i),this);
    wind->exec();
}

void Custom::confsDelButtonPressed()
{
    int i = m_confsDelButtons.indexOf(static_cast<QPushButton *>(sender()));
    auto b1 = m_confs.at(i);
    m_confs.removeAt(i);
    auto b2 = m_confsButtons.at(i);
    m_confsButtons.removeAt(i);
    auto b3 = m_confsDelButtons.at(i);
    m_confsDelButtons.removeAt(i);
    /*b1->deleteLater();
    b2->deleteLater();
    b3->deleteLater();*/
    delete b1;
    delete b2;
    delete b3;
}

void Custom::IOCreated(IOClass *IO)
{
    int i =m_confs.indexOf(static_cast<IOConfigurator *>(sender()));
    int size = m_confs.size();
    auto b = dynamic_cast<QHBoxLayout *>(m_areaLayout->itemAt(size-i));
    addIO(IO);
    b->insertLayout(1, IO->layout());
    this->setMinimumSize(m_mainLayuot->minimumSize());     //Для правильного определения размера окна
}

void Custom::save()
{
    m_useNickname->setChecked(true);
    int size = contacts().size();
    for(int i=0;i<size;++i)
    {
        QString name = contacts().at(i)->name();
        if(!(name.contains("<") && name.contains(">")))
            name.prepend("<>");
        Static::writeTag(name, "0", '<', '>', ':');      //Для того, чтобы соединения сохранились для нулевого адреса
        contacts().at(i)->getIO()->updateName(name);
    }
    Main->saveCustom(this);
    nameChanged(m_name);    //Возвращаем контактам правильные имена
}

void Custom::memoryInitialisation()
{
    QVector<uint16_t> buf;
    QStringList bufName;
    for(int i=m_newMemAddr; i<m_newMemEnd+1; ++i)
    {
        buf.append(getRegister(i)); //Так старые значения памяти сохранятся
        bufName.append(getRegisterName(i));
    }
    m_mem=buf;
    m_memNames=bufName;
    m_memAddr=m_newMemAddr;
    m_memEnd=m_newMemEnd;
}

void Custom::resizeEvent(QResizeEvent *event)
{
    Module::resizeEvent(event);
    m_scrollArea->resize(m_scrollArea->width(), this->height()-300);
}
