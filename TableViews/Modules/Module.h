#ifndef MODULE_H
#define MODULE_H

#include "Imitation.h"
#include <QTimer>
#include <QMap>
#include <QQueue>

#define C_READ 3        //Стандартная команда чтения
#define C_WRITE 16    //Стандартная команда записи

class led;
class Module : public Imitation                        //Класс, являющийся родительским для модулей
{
    Q_OBJECT

public:
    Module(QWidget *parent, uchar addr=0);
    virtual ~Module();


    virtual QJsonObject toJson(void) override;
    virtual void fromJson(const QJsonObject  &jsn) override;
    void copyCoreData(Module *original);        //Копирует общие для модулей данные у другого модуля

    uchar address() const;                                    //Возвращает адрес модуля
    const QString nickname() const;                   //Возвращает псевдоним модуля
    QCheckBox *useNickname() const;               //Возвращает указатель на виджет
    QCheckBox *doAnswer() const;                    //Возвращает указатель на виджет
    uint16_t getRegister(int i) const;                   //Получить значение регистра i в памяти контроллера (если такой регистр отсутствует, возвращает 0)
    void setRegister(int i, uint16_t val);                //Установить значение регистра i в памяти контроллера
    QString getRegisterName(int i) const;                   //Получить значение регистра i в памяти контроллера (если такой регистр отсутствует, возвращает 0)
    void setRegisterName(int i, QString name);                //Установить значение регистра i в памяти контроллера

    int memAddr() const;
    int memEnd() const;

    const QStringList &memNames() const;
    void addIO(IOClass *IO);                //Функция для добавления в списки контакта под управлением IOConfigurator
    void removeIO(IOClass *IO);          //Функция для удаления в списки контакта под управлением IOConfigurator

public slots:
    virtual void connected(bool val) override;                        //Установлено COM соединение
    virtual void disconnected(void) override;                         //Разорвано COM соединение
    void blink();                                                          //Пришло новое сообщение, стандартная реакция (мигание лампочкой)
    virtual void newMessage(QByteArray const &text, bool check);   //Пришло новое сообщение, стандартная реакция (стандартный ответ)
    void addrListUpdated(QVector<bool>);                              //Обновлен список доступных адресов
    void addrChanegedSLOT(const QString newA);              //Пользователь изменил адрес модуля
    virtual void changeName() override;                               //Имя модуля изменилось
    virtual void communicationProblem(int p) override;       //Статус коммуникации изменился (без проблем, с проблемами, нет связи)
    void newCycle(int calcCycle) override;                            //Сработал таймер
    virtual void readOutputs(void);                                     //Чтение требуемого состояния выходов

signals:
    void answer(QByteArray const &text);             //Ответить по COM соединению (сигнал)
    void addrChanged(int oldA, int newA);           //Адрес модуля изменился с oldA на newA
    void registerChanged(int reg);

protected:
    void sendAnswer(QByteArray &text);                //Ответить по COM соединению заготовленным ответом text
    void sendAnswer(uint16_t addr, uint16_t number, uchar command);                //Ответить по COM соединению стандатным ответом
    virtual void memoryInitialisation();                    //Инициализация памяти модуля
    void setMemEnd(int newMemEnd);
    void setMemAddr(int newMemAddr);


QString m_nickname;                                            //Псевдоним
QCheckBox *m_useNickname;                              //Виджет выбора: использовать ли псевдоним?
led *m_isConnected;                                              //Индикатор состояния COM подключения
led *m_RXD;                                                            //Индикатор чтения сообщения
led *m_TXD;                                                            //Индикатор ответа
led *m_RUN;                                                           //Индикатор работы модуля
QCheckBox *m_doAnswer;                                    //Виджет выбора: отвечать ли процессору?
QLabel *m_doAnswerDesription;                          //QLabel с пояснением к m_doAnswer
uint16_t rez_address;                                          //Переменная на случай, если модуль не переопределит место адреса в памяти
uint16_t *m_address=&rez_address;                  //Адрес модуля
uint16_t rez_wrkInd;                                          //Переменная на случай, если модуль не переопределит место bylbrfwbb hf,jns в памяти
uint16_t *m_wrkInd=&rez_wrkInd;                      //Индикация работы
QHBoxLayout *m_addressLayout;                         //QLayout, содержащий в себе информацию об адресе
QLabel *m_addressDescription;                            //QLabel с пояснением к m_addressBox
QComboBox *m_addressBox;                                //Виджет для выбора адреса модуля
QVector<uint16_t> m_mem;                                 //Доступная для чтения и записи память модуля (имитация регистров реального устройства)
int m_memAddr;                                                   //Начальный адрес памяти
int m_memEnd;                                                     //Конечный адрес памяти
QStringList m_memNames;
};

#endif // MODULE_H
