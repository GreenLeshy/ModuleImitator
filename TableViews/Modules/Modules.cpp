#include "Modules.h"

Modules::Modules()
{ 

}

const QStringList Modules::typeNames =         //Обязательно должен быть согласован по количеству и порядку элементов с mod_types
{
    "NO DEFINED",
        "DI-11",
        "DO-11",
        "DIO-11",
        "AI-12",
        "AO-11",
        "DI-32A",
        "DI-33A",
        "DIA-31A",
        "DO-31A",
        "DIO-32A",
        "AI-31A",
        "AI-32A",
        "AI-33A",
        "AO-31A",
        "AIO-31A",
        "ERU",
        "Custom"
};

QStringList Modules::getTypeNames()
{
    return typeNames;
}

QString Modules::getTypeName(int i)
{
    return typeNames.at(i);
}

Module *Modules::newModule(QWidget *parent, int type, uchar addr)
{
    Module* newTab;
    switch (type)
    {
    case int(Modules::AI_12):
        newTab = new class AI_12(parent, addr);
    break;
    case int(Modules::AO_11):
        newTab = new class AO_11(parent, addr);
    break;
    case int(Modules::DI_11):
        newTab = new class DI_11(parent, addr);
    break;
    case int(Modules::DIO_11):
        newTab = new class DIO_11(parent, addr);
    break;
    case int(Modules::DO_11):
        newTab = new class DO_11(parent, addr);
    break;
    case int(Modules::ERU):
        newTab = new class ERU_ECP(parent, addr);
    break;
    case int(Modules::DO_31A):
        newTab = new class DO_31A(parent, addr);
    break;
    case int(Modules::DI_33A):
        newTab = new class DI_33A(parent, addr);
    break;
    case int(Modules::AI_32A):
        newTab = new class AI_32A(parent, addr);
    break;
    case int(Modules::AI_31A):
        newTab = new class AI_31A(parent, addr);
    break;
    case int(Modules::AI_33A):
        newTab = new class AI_33A(parent, addr);
    break;
    case int(Modules::DI_32A):
        newTab = new class DI_32A(parent, addr);
    break;
    case int(Modules::AO_31A):
        newTab = new class AO_31A(parent, addr);
    break;
    case int(Modules::AIO_31A):
        newTab = new class AIO_31A(parent, addr);
    break;
    case int(Modules::DIO_32A):
        newTab = new class DIO_32A(parent, addr);
    break;
    case int(Modules::DIA_31A):
        newTab = new class DIA_31A(parent, addr);
    break;
    case int(Modules::Custom):
        newTab = new class Custom(parent, addr);
    break;
    default:
        newTab = new Module(parent, addr);
    break;
    }
    return newTab;
}
