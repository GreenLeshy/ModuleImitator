#ifndef ERU_ECP_H
#define ERU_ECP_H

#include "Module.h"

class ERU_ECP : public Module       //Модуль, имитирующий поведение регулятора с ЭРУ
{
    Q_OBJECT
public:
    ERU_ECP(QWidget *parent, uchar addr);
    virtual QJsonObject toJson(void) override;               //Возвращает информацию об элементе в виде QJsonObject
    virtual void fromJson(const QJsonObject  &jmod) override;    //Загружает информацию об элементе из QJsonObject

public slots:
    void LChaneged(void);
    void positionMinMaxChaneged(void);
    void ustChangeSpeedChaneged(void);
    void setParams(void);
    virtual void newMessage(QByteArray const &text, bool check) override;
    void newCycle(int calcCycle) override;                  //Расчет модели объекта

protected:
    double m_value=0;
    double m_target=0;
    double m_position=0;
    double m_positionMin=0;
    double m_positionMax=50;
    double m_k=1;
    double m_Ti=1;
    double m_Td=1;
    double m_L=50;
    double m_ELast1=0;
    double m_ELast2=0;
    double m_ustChangeSpeed=1;
    QLabel *m_targetWidget;
    QLabel *m_valueWidget;
    QLabel *m_positionWidget;
    QLineEdit *m_kWidget;
    QLineEdit *m_TiWidget;
    QLineEdit *m_TdWidget;
    QLineEdit *m_LWidget;
    QLineEdit *m_positionMinWidget;
    QLineEdit *m_positionMaxWidget;
    QPushButton *m_plusButton;
    QPushButton *m_minusButton;
    QLineEdit *m_ustChangeSpeedWidget;
    QLineEdit *m_deltaWidget;
    QCheckBox *m_useDeltaWidget;


    void setReg(uchar reg, QByteArray regText);

    const uchar C_TARGET=2;
    const uchar C_STEP_OPEN=3;
    const uchar C_STEP_CLOSE=4;
    const uchar C_FORBID_CONSOLE=5;
    const uchar C_ALLOW_CONSOLE=6;
    const uchar C_INCREASE_TARGET=7;
    const uchar C_DECREASE_TARGET=8;
    const uchar C_STOP_TARGET=9;
    const uchar C_FAST_CLOSE=11;
};

#endif // ERU_ECP_H
