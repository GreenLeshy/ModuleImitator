#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "AddTabM.h"
#include "AddTabO.h"
#include "Contacts.h"
#include "Objects.h"
#include "Modules.h"
#include "SystemTabs.h"
#include "static.h"
#include "IOWidgets.h"
#include "IOs.h"
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include "options.h"
#include <QDebug>
#include <QDialogButtonBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_modulesTabWidget = ui->moduls;
    m_modulesTabWidget->removeTab(0);//Удаление Tabов, которые автоматически создаются ui
    m_modulesTabWidget->removeTab(0);

    m_objectsTabWidget = ui->objects;
    m_objectsTabWidget->removeTab(0);//Удаление Tabов, которые автоматически создаются ui
    m_objectsTabWidget->removeTab(0);

    m_systemTabWidget = ui->interfaces;
    m_systemTabWidget->removeTab(0);//Удаление Tabов, которые автоматически создаются ui
    m_systemTabWidget->removeTab(0);

    updateIndex();
    readConfig();
    writeConfig();
    updateCustomList();

    for (int i=0; i<ADDR_AVALIABLE; ++i)
        m_addresses.append(false);

    addSystemTab(SystemTabs::WidgetsTab, "Управление");
    addSystemTab(SystemTabs::TabAllModules, "Модули");

    AddTabM* newTab5 = new AddTabM(this);
    addTabModule(newTab5);

    auto* newTab0 = new AddTabO(this);
    addTabobject(newTab0);

    m_modulesTabWidget->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    m_modulesTabWidget->show();

    iniPort();

    connect(m_modulesTabWidget, &QTabWidget::currentChanged, this, &MainWindow::moduleOpened);
    connect(m_objectsTabWidget, &QTabWidget::currentChanged, this, &MainWindow::objectOpened);
    connect(ui->ActionSave, &QAction::triggered, this, &MainWindow::savePressed);
    connect(ui->actionSaveAs, &QAction::triggered, this, &MainWindow::saveAsPressed);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::openFilePressed);
    connect(ui->actionNew, &QAction::triggered, this, &MainWindow::newProjectPressed);
    connect(ui->actionParameters, &QAction::triggered, this, &MainWindow::optionsPressed);

    connect(m_problemTimer, &QTimer::timeout, this, &MainWindow::problemTimerTimeout);

    if(m_fileName!="")
        load(m_fileName);
    else
    {
        addModule();
        addObject();
    }
    keyCtrlS = new QShortcut(this);
    keyCtrlS->setKey(Qt::CTRL + Qt::Key_S);
    connect(keyCtrlS, &QShortcut::activated, this, &MainWindow::savePressed);

    keyShiftCtrlS = new QShortcut(this);
    keyShiftCtrlS->setKey(Qt::CTRL+ Qt::SHIFT + Qt::Key_S);
    connect(keyShiftCtrlS, &QShortcut::activated, this, &MainWindow::saveAsPressed);

    keyCtrlO = new QShortcut(this);
    keyCtrlO->setKey(Qt::CTRL + Qt::Key_O);
    connect(keyCtrlO, &QShortcut::activated, this, &MainWindow::openFilePressed);

    keyCtrlN = new QShortcut(this);
    keyCtrlN->setKey(Qt::CTRL + Qt::Key_N);
    connect(keyCtrlN, &QShortcut::activated, this, &MainWindow::newProjectPressed);

    QString buf(QDir::currentPath());
    buf = Static::changeFolderBranch(buf, "", "sounds/");

    sound_OnOff = new QMediaPlayer(this);
    sound_OnOff->setMedia(QUrl::fromLocalFile(buf+"GateOnOff.wma"));
    sound_inWork = new QMediaPlayer(this);
    sound_inWork->setMedia(QUrl::fromLocalFile(buf+"GateInWork.mp3"));
    sound_inWork->setVolume(10);

    m_lastTime=QTime::currentTime().msecsSinceStartOfDay();
    timerSlot();                                                                            //Запуск таймера

    ui->namesIsReapeated->setStyleSheet("QLabel {color : red; }");
    ui->namesIsReapeated->hide();
    ui->badBoundTry->setStyleSheet("QLabel {font-weight: bold; color : red; }");
    ui->badBoundTry->hide();
}

MainWindow::~MainWindow()
{
    if(m_saveConfig)
        writeConfig();
    if(m_multithread)
    {
        m_pThread->quit();
        m_pThread->wait(1000);
    }
    delete ui;
    deleteIndex();
}

void MainWindow::newMessageSLOT(const QByteArray &text, bool check)
{

    QString buf("->");
    for(int i=0; i<text.size(); ++i)
    {
        int p = uchar(text[i]);
        buf.append(QString::number(p)).append(" ");
    }
    COMTab()->write(buf);
    if(!check)
    {
        COMTab()->write("CRC wrong!");
        m_communicationProblem=1;
        emit communicationProblem(1);
    }
    else
        if(m_communicationProblem)
        {
            m_communicationProblem=0;
            emit communicationProblem(0);
        }
    m_problemTimer->start(2500);
    m_dedablerAnswer=1;             //Можно отвечать
    emit newMessage(text, check);
}

void MainWindow::answerSLOT(const QByteArray &text)
{
    QString buf("<-");
    for(int i=0; i<text.size(); ++i)
    {
        int p = uchar(text[i]);
        buf.append(QString::number(p)).append(" ");
    }
    if(m_dedablerAnswer)    //Данное условие гарантирует, что не будет отправлено два ответа подряд на "слипшийся" запрос
    {
        COMTab()->write(buf);
        emit answer(text);
        m_dedablerAnswer=0;
    }
}

void MainWindow::connectedSLOT(const bool val)
{
    if(val)
    {
        if(m_isConnected==false)
            COMTab()->write(QString("Connected to ").append(m_currentComm));
        else
            COMTab()->write(QString("Restarted"));
    }
    else
        COMTab()->write(QString("Failed connected to ").append(m_currentComm));
    m_isConnected = val;
    m_problemTimer->start(2500);
    emit connected(val);
}

void MainWindow::disconnectedSLOT()
{
    COMTab()->write(QString("Disconnected from ").append(m_currentComm));
    m_isConnected = false;
    m_problemTimer->stop();
    emit disconnected();
}

void MainWindow::BConnectPressed()
{
    m_currentComm=COMTab()->CurrentCOM();
    emit connectToPort(m_currentComm);
}

void MainWindow::BDisconnectPressed()
{
    emit disconnectFromPort();
}

void MainWindow::moduleChangeAddr(int oldA, int newA)
{
    m_addresses[oldA]=false;
    if(newA!=0)
        m_addresses[newA]=true;
    emit addrListUpdated(m_addresses);
    setSaved(false);
}

Module *MainWindow::addModule(int type, int addr, int pos)
{
    auto *newTab=Modules::newModule(this, type, addr);
    addTabModule(newTab, pos);
    return newTab;
}

objectModel *MainWindow::addObject(int type, QString name, int pos)
{
    auto newTab = objects::newobject(this, type, name);
    addTabobject(newTab, pos);
    return newTab;
}

MI_TableView *MainWindow::addSystemTab(int type, QString name, int pos)
{
    MI_TableView* newTab;
    newTab = SystemTabs::newSysTab(this, type, name);
    addTabSystem(newTab, pos);
    return newTab;
}

void MainWindow::moduleOpened(int i)
{
    if(!m_loadFlag)
        if(i==m_modulesTabWidget->count()-1)//Если открыта вкладка "+"
        {
            addModule(0, 0, m_modulesTabWidget->count()-1); //Добавляем новую вкладку..
            m_modulesTabWidget->setCurrentIndex(m_modulesTabWidget->count()-2); //.. и переключаемся на нее
        }
    resize();
}

void MainWindow::objectOpened(int i)
{
    if(!m_loadFlag)
        if(i==m_objectsTabWidget->count()-1)//Если открыта вкладка "+"
        {
            addObject(0, 0, m_objectsTabWidget->count()-1);
            m_objectsTabWidget->setCurrentIndex(m_objectsTabWidget->count()-2);
        }
    resize();
}

void MainWindow::moduleChangeName(QString &name)
{
    Module *module = dynamic_cast<Module*>(sender());
    int i=m_modules.indexOf(module);
    m_modulesTabWidget->setTabText(i, name);
    QString oldName = m_modulesNameList.key(i);
    m_modulesNameList.remove(oldName);
    m_modulesNameList.insert(name, i);
}

void MainWindow::objectChangeName(QString &name)
{
    objectModel *OM = dynamic_cast<objectModel*>(sender());
    int i=m_objects.indexOf(OM);
    m_objectsTabWidget->setTabText(i, name);
    QString oldName = m_objectsNameList.key(i);
    m_objectsNameList.remove(oldName);
    m_objectsNameList.insert(name, i);
}

void MainWindow::deleteModule(void)
{
    Module *im = dynamic_cast<Module *>(sender());
    int i=m_modules.indexOf(im);
    deleteModuleFromLists(i);
    moduleChangeAddr(im->address(), 0);                 //Освобождаем адрес
    if(i==m_modules.size()-1)
        m_modulesTabWidget->setCurrentIndex(i-1);
    m_modulesTabWidget->removeTab(i);
    im->deleteLater();
    setSaved(false);
}

void MainWindow::deleteobject()
{
    auto *im = dynamic_cast<objectModel *>(sender());
    int i=m_objects.indexOf(im);
    deleteobjectFromLists(i);
    if(i==m_objects.size()-1)
        m_objectsTabWidget->setCurrentIndex(i-1);
    m_objectsTabWidget->removeTab(i);
    im->deleteLater();
    setSaved(false);
}

void MainWindow::deleteSystem(MI_TableView * sys)
{
    int i=m_systemTabs.indexOf(sys);
    deleteSystemFromLists(i);
    m_systemTabWidget->removeTab(i);
    sys->deleteLater();
}

void MainWindow::moduleChangeType(int type)
{
    Module *module = dynamic_cast<Module*>(sender());
    removeContactsImit(module);                    //Чтобы не было наложения контактов
    int i=m_modules.indexOf(module);
    module->deletePressedSLOT();
    Module* newMod;
    int size = Modules::getTypeNames().size();
    if(type<size)
        newMod=addModule(type, 0, i);
    else
        newMod=loadCustomBlock(m_customBlocks.at(type-size), i, module->name());
    if(newMod==nullptr)
        return;
    newMod->copyCoreData(module);
    newMod->changeName();
    moduleChangeAddr(0, newMod->address());  //Снова занять адрес, который освободил модуль-предшественник
    m_modulesTabWidget->setCurrentIndex(i);
    setSaved(false);
}

void MainWindow::objectChangeType(int type)
{
    objectModel *obj = dynamic_cast<objectModel*>(sender());
    QString type1 = Static::cut(obj->type(), "_");
    QString type2 = Static::cut(objects::getTypeName(type), "_");
    QJsonObject jsn{obj->toJson()};          //Сохраняем информацию на случай, если выбран схожий тип объекта
    removeContactsImit(obj);                    //Чтобы не было наложения контактов
    int i=m_objects.indexOf(obj);
    addObject(type, obj->name(), i);
    obj->deletePressedSLOT();
    if(type1==type2)                                //Если необходимо только поменять количество подобъектов в составном объекте (если выбран схожий тип объекта)
        m_objects.at(i)->fromJson(jsn);
    m_objects.at(i)->changeName();
    m_objectsTabWidget->setCurrentIndex(i);
    setSaved(false);
}

void MainWindow::contactChanged(Contact *con)
{
    //qDebug() <<"MW contactChanged "+con->name();
    if(!m_loadFlag)
    {
        if(con->dataType()==data_type::Discrete)    //Обновление списков дискретных и аналоговых контактов разделены для оптимизации
            updateDiscreteContacts();
        else
            updateAnalogContacts();

        WidgetsTab()->updateIOmini(con);
        setSaved(false);
    }
    resize();
}

void MainWindow::contactRenamed(Contact *con)
{
    //qDebug() <<"MW contactRenamed "+con->name();
        updateNameList();
        if(m_contacts.size()!=m_contactsNameList.size()+m_reservedContactsCount)
            ui->namesIsReapeated->show();
        else
            ui->namesIsReapeated->hide();
        contactChanged(con);
}

void MainWindow::timerSlot()
{
    int t = QTime::currentTime().msecsSinceStartOfDay();
    int d= t-m_lastTime;
    if(d<1) //На случай, если программа работает в полночь и системное время перешло с 23:59:59:999 на 0
        d=m_calcCycle;
    m_lastTime=t;

    ++m_cycleIndex;
    int i =m_cycleIndex%CYCLE_N;
    m_cycleTimes[i]=d;
    if(m_cycleIndex%CYCLE_N==0)
    {
        int buf = m_cycleTimes[0];
        int min=buf;
        int max=buf;
        double s = buf;
        for(int i=1; i<CYCLE_N; ++i)
        {
            buf=m_cycleTimes[i];
            if(buf>max)
                max=buf;
            if(buf<min)
                min=buf;
            s+=buf;
        }
        int medium = qIntCast(s/CYCLE_N);
        QString timeString("Реальное время цикла: {");
        timeString.append(QString::number(min)).append("...").append(QString::number(medium)).append("...").append(QString::number(max)).append("} мс.");
        ui->calcTime->setText(timeString);
    }
    QTimer::singleShot(m_calcCycle, this, &MainWindow::timerSlot);      //SingleShot для лучшего контроля циклов расчета
    emit timerSignal(d, m_cycleIndex);
}

void MainWindow::savePressed()
{
    if(m_fileName!="")
        save(m_fileName);
    else
        saveAsPressed();
}

void MainWindow::saveAsPressed()
{
    QString path = Static::changeFolderBranch(QDir::currentPath(), "release", "Projects");
    QString fileName = QFileDialog::getSaveFileName(this, tr("Выберите файл сохранения"), path, tr("Json files (*.json)"));
    if(fileName!="")
        save(fileName);
}

void MainWindow::openFilePressed()
{
    QString path = Static::changeFolderBranch(QDir::currentPath(), "release", "Projects");
    QString fileName = QFileDialog::getOpenFileName(this, tr("Выберите файл загрузки"), path, tr("Json files (*.json)"));
    if(fileName!="")
        load(fileName);
}

void MainWindow::newProjectPressed()
{
    m_fileName="";
    this->setWindowTitle(QString("ModuleImitator - ").append("*New project*"));
    deleteAll();
    deletePort();
    iniPort();
    emit addrListUpdated(m_addresses);
}

void MainWindow::problemTimerTimeout()
{
    m_communicationProblem=true;
    emit communicationProblem(2);
}

void MainWindow::baudRateChangedSLOT(QString text)
{
    emit baudRateChanged(text);
}

void MainWindow::dataBitsChangedSLOT(QString text)
{
    emit dataBitsChanged(text);
}

void MainWindow::parityChangedSLOT(QString text)
{
    emit parityChanged(text);
}

void MainWindow::stopBitsChangedSLOT(QString text)
{
    emit stopBitsChanged(text);
}

void MainWindow::readTimeoutChangedSLOT(QString text)
{
    int t=text.toInt();
    if(t<0)
        t=1;
    emit readTimeoutChanged(t);
}

void MainWindow::writeTimeoutChangedSLOT(QString text)
{
    int t=text.toInt();
    if(t<0)
        t=1;
    emit writeTimeoutChanged(t);
}

void MainWindow::optionsPressed()
{
    Options opt(this);
    opt.setModal(true);
    opt.exec();
}

void MainWindow::currentCOMChaneged()
{
    bool buf = m_isConnected;
    BDisconnectPressed();
    if(buf)
        BConnectPressed();
}

void MainWindow::playOnOff()
{
    if(m_soundsOn)
        if(sound_OnOff!=nullptr)
            sound_OnOff->play();
}

void MainWindow::gateStartWorking(int n)
{
    m_gateInWork+=n;
        if(sound_inWork!=nullptr)
        {
            if((m_gateInWork>0) & m_soundsOn)
                sound_inWork->play();
            else
                sound_inWork->stop();
        }
}

void MainWindow::closeEvent(QCloseEvent *ev)
{
    if(!m_saved)
    {
        QMessageBox msgBox(this);
        msgBox.setText(QString("Проект был изменен."));
        msgBox.setInformativeText(QString("Хотите сохранить изменения?"));
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::No | QMessageBox::Cancel);
        int ret=msgBox.exec();
        if(ret==QMessageBox::Save)
            savePressed();
        else
            if(ret==QMessageBox::Cancel)
                ev->ignore();
        if(ret==QMessageBox::Save || ret==QMessageBox::No)
            deleteAll();
    }
    else
        deleteAll();
}

void MainWindow::showBadBoundTry()
{
    ui->badBoundTry->show();
    QTimer::singleShot(10000, this, &MainWindow::hideBadBoundTry);
}

void MainWindow::hideBadBoundTry()
{
    ui->badBoundTry->hide();
}

TabCOM* MainWindow::COMTab()
{
    //return dynamic_cast<TabCOM*>(systemTab("COM"));
    return m_COM;
}

WidgetsTab *MainWindow::WidgetsTab()
{
    return dynamic_cast<class WidgetsTab*>(systemTab("Управление"));
}

void MainWindow::addContact(Contact *newContact)
{
    m_contacts.append(newContact);
    m_contactsNameList.insert(newContact->name(), m_contacts.size()-1);
}

void MainWindow::removeContact(Contact *delContact)
{
    if(m_contacts.contains(delContact))
    {
        delContact->disbound();
        m_contacts.removeOne(delContact);
        Static::shiftRemove(&m_contactsNameList, delContact->name());
        contactChanged(delContact);
        WidgetsTab()->deleteContact(delContact);
    }
}

void MainWindow::removeContactsImit(Imitation *Imit)
{
    QList<Contact *> contacts = Imit->contacts();
    int n =contacts.size();
    for(int i=0; i<n; ++i)
    {
        Contact *contact = contacts.at(i);
        contact->setName(QString("Reserved name: this contact will be deleted").append(QString::number(i))); //Костыль, предохраняющий от сбоев индексации при удалении элементов при совпадении имен
        this->removeContact(contact);
    }
}

void MainWindow::updateDiscreteContacts()
{
    QStringList inputs;
    QStringList outputs;
    int n = m_contacts.size();
    for(int i=0; i<n; ++i)
    {
        auto contact = m_contacts.at(i);
        if(contact->dataType()==data_type::Discrete)     //Если контакт дискретный
             if(contact->IsBusy()==false)                                           //Если контакт не занят
                 if(contact->getIO()->name().toLower()!="резерв")      //Если контакт используется
                     {
                        if(contact->IOType()==IO_type::Input)         //Добавляем его в нужный список
                            inputs.append(contact->name());
                        else
                            outputs.append(contact->name());
                     }
    }
    if(m_loadFlag)
    {
        if(Static::StringListsIsEqual(inputs,m_lastDIs))
        {
            emit DIContactsUpdated(inputs);
            m_lastDIs=inputs;
       }
        if(Static::StringListsIsEqual(outputs,m_lastDOs))
        {
            emit DOContactsUpdated(outputs);
            m_lastDOs=outputs;
        }
    }
    else
    {
        emit DIContactsUpdated(inputs);
        emit DOContactsUpdated(outputs);
    }
}

void MainWindow::updateAnalogContacts()
{
    QStringList inputs;
    QStringList outputs;
    int n = m_contacts.size();
    for(int i=0; i<n; ++i)
    {
        auto contact = m_contacts.at(i);
        if(contact->dataType()==data_type::Analog)     //Если контакт аналоговый
             if(contact->IsBusy()==false)                                           //Если контакт не занят
                 if(contact->getIO()->name().toLower()!="резерв")      //Если контакт используется
                     {
                        if(contact->IOType()==IO_type::Input)         //Добавляем его в нужный список
                            inputs.append(contact->name());
                        else
                            outputs.append(contact->name());
                     }
    }

    if(m_loadFlag)
    {
        if(Static::StringListsIsEqual(inputs,m_lastAIs))
        {
            emit AIContactsUpdated(inputs);
            m_lastAIs=inputs;
        }
        if(Static::StringListsIsEqual(outputs,m_lastAOs))
        {
            emit AOContactsUpdated(outputs);
            m_lastAOs=outputs;
        }
    }
    else
    {
        emit AIContactsUpdated(inputs);
        emit AOContactsUpdated(outputs);
    }
}

Module *MainWindow::module(int i)
{
    return m_modules.at(i);
}

objectModel *MainWindow::object(int i)
{
    return m_objects.at(i);
}

MI_TableView *MainWindow::systemTab(int i)
{
    return m_systemTabs.at(i);
}

MI_TableView *MainWindow::systemTab(QString name)
{
    if(m_systemTabsNameList.contains(name))
            return systemTab(m_systemTabsNameList[name]);
        return nullptr;
}

MI_TableView *MainWindow::tab(QString &name)
{
    if(m_modulesNameList.contains(name))
        return module(m_modulesNameList[name]);
    else if(m_objectsNameList.contains(name))
        return object(m_objectsNameList[name]);
    else if(m_systemTabsNameList.contains(name))
        return systemTab(m_systemTabsNameList[name]);
    return nullptr;
}

Module *MainWindow::module(QString &name)
{
    if(m_modulesNameList.contains(name))
        return module(m_modulesNameList[name]);
    return nullptr;
}

int MainWindow::elSize(int i)
{
    return m_sizeList.at(i);
}

void MainWindow::addTabModule(Module *newTab, int pos)
{
    if(pos<0)
    {
        if(m_modules.size()>0)
        {
            if(m_modules.last()->name()=="+")
            {
                int p=m_modules.size()-1;
                m_modules.insert(p, newTab);
                m_modulesTabWidget->insertTab(p, newTab, newTab->name());
                m_modulesTabWidget->setCurrentIndex(p);
            }
            else
            {
                m_modules.append(newTab);
                m_modulesTabWidget->addTab(newTab, newTab->name());
            }
        }
        else
        {
            m_modulesTabWidget->addTab(newTab, newTab->name());
            m_modules.append(newTab);
        }
        m_modulesNameList.insert(newTab->name(), m_modules.size());
    }
    else
    {
        m_modules.insert(pos, newTab);
        m_modulesTabWidget->insertTab(pos, newTab, newTab->name());

        Static::shiftInsert(&m_modulesNameList, newTab->name(), pos);
    }
    setSaved(false);
}

void MainWindow::addTabobject(objectModel *newTab, int pos)
{
    if(pos<0)
    {
        if(m_objects.size()>0)
        {
            if(m_objects.last()->name()=="+")
            {
                int p=m_objects.size()-1;
                m_objects.insert(p, newTab);
                m_objectsTabWidget->insertTab(p, newTab, newTab->name());
                m_objectsTabWidget->setCurrentIndex(p);
            }
            else
            {
                m_objects.append(newTab);
                m_objectsTabWidget->addTab(newTab, newTab->name());
            }
        }
        else
        {
            m_objects.append(newTab);
            m_objectsTabWidget->addTab(newTab, newTab->name());
        }
        m_objectsNameList.insert(newTab->name(), m_objects.size());
    }
    else
    {
        m_objects.insert(pos, newTab);
        m_objectsTabWidget->insertTab(pos, newTab, newTab->name());
        Static::shiftInsert(&m_objectsNameList, newTab->name(), pos);
    }
    setSaved(false);
}

void MainWindow::addTabSystem(MI_TableView *newTab, int pos)
{
    if(pos<0)
    {
        m_systemTabs.append(newTab);
        m_systemTabWidget->addTab(newTab, newTab->name());
        m_systemTabsNameList.insert(newTab->name(), m_systemTabs.size());
    }
    else
    {
        m_systemTabs.insert(pos, newTab);
        m_systemTabWidget->insertTab(pos, newTab, newTab->name());
        Static::shiftInsert(&m_systemTabsNameList, newTab->name(), pos);
    }
}

void MainWindow::deleteModuleFromLists(int i)
{
    QString oldName = m_modulesNameList.key(i);
    removeContactsImit(m_modules.at(i));
    m_modules.removeAt(i);
    m_modulesNameList.remove(oldName);
}

void MainWindow::deleteobjectFromLists(int i)
{
    QString oldName = m_objectsNameList.key(i);
    removeContactsImit(m_objects.at(i));
    m_objects.removeAt(i);
    m_objectsNameList.remove(oldName);
}

void MainWindow::deleteSystemFromLists(int i)
{
    QString oldName = m_systemTabsNameList.key(i);
    m_systemTabs.removeAt(i);
    m_systemTabsNameList.remove(oldName);
}

void MainWindow::save(QString &fileName)
{
    QJsonObject Mainobject;
    Mainobject["ver"]=PROGRAM_VERSION;
    Mainobject["confirmation"]="It's Module Imitator project file";
    Mainobject["connected"]=m_isConnected;
    Mainobject["currentModule"]=m_modulesTabWidget->currentIndex();
    Mainobject["currentobject"]=m_objectsTabWidget->currentIndex();
    Mainobject["currentSys"]=m_systemTabWidget->currentIndex();
    QJsonArray modules;
    for(int i=0; i<m_modules.size()-1; ++i)
    {
        modules.append(m_modules.at(i)->toJson());
    }
    Mainobject.insert("modules", modules);

    QJsonArray objects;
    for(int i=0; i<m_objects.size()-1; ++i)
    {
        objects.append(m_objects.at(i)->toJson());
    }
    Mainobject.insert("objects", objects);

    QJsonArray systemTabs;
    for(int i=0; i<m_systemTabs.size(); ++i)
    {
        systemTabs.append(m_systemTabs.at(i)->toJson());
    }
    Mainobject.insert("systemTabs", systemTabs);

    QJsonDocument doc(Mainobject);

    auto calcCRC=Static::CRC(doc.toJson(QJsonDocument::Indented));
    Mainobject["CRC"]=Static::toU16(calcCRC[0], calcCRC[1]);
    doc.setObject(Mainobject);

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    file.write(doc.toJson(QJsonDocument::Indented));
    file.close();
    m_fileName = fileName;
    updateLastProject(fileName);
    QMessageBox msgBox(this);
    msgBox.setText("Проект успешно сохранен");
    QTimer::singleShot(1200, &msgBox, &QMessageBox::close);
    msgBox.exec();
    setSaved(true);
}

void MainWindow::load(QString &fileName)
{
    emit BDisconnectPressed();
    m_loadFlag=1;
    WidgetsTab()->clear();
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QString buf=file.readAll();
    QJsonDocument doc=QJsonDocument::fromJson(buf.toUtf8());
    QJsonObject Mainobject = doc.object();

    if(Mainobject["confirmation"]!="It's Module Imitator project file")
    {
        QMessageBox msgBox(this);
        msgBox.setText(QString("Файл \"%1\" поврежден или не является файлом проекта ModuleImitator").arg(fileName));
        msgBox.exec();
        m_loadFlag=0;
        addModule();
        addObject();
        return;
    }

    int readCRC=Mainobject["CRC"].toInt();
    Mainobject.remove("CRC");
    doc.setObject(Mainobject);
    auto calcCRC=Static::CRC(doc.toJson(QJsonDocument::Indented));
    if(readCRC!=Static::toU16(calcCRC[0], calcCRC[1]))
    {
        QMessageBox msgBox(this);
        msgBox.setText(QString("Проект был отредактирован или поврежден."));
        msgBox.setInformativeText(QString("Желаете продолжить загрузку?"));
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        int ret=msgBox.exec();
        if(ret==QMessageBox::Cancel)
        {
            m_loadFlag=0;
            addModule();
            addObject();
            return;
        }
    }

    double a = double(int(Mainobject["ver"].toDouble()*100)/100.0);
    double b = double(int(PROGRAM_VERSION*100)/100.0);
    if(a!=b)   //Отличия версий больше или равно 0.01
    {
        QMessageBox msgBox(this);
        msgBox.setText(QString("Данный проект был создан в другой версии программы, это может привести к ошибкам"));
        msgBox.exec();
    }

    deleteAll();

    this->blockSignals(true);   //Для оптимизации, иначе время загрузки экспоненциально растет от количества модулей
    QJsonArray modules=Mainobject["modules"].toArray();
    for(int i=0; i<modules.size(); ++i)
    {
        QString type = modules.at(i).toObject()["type"].toString();
        int typeInt = Modules::getTypeNames().indexOf(type);
        int addr = modules.at(i).toObject()["addr"].toInt();
        Imitation* newTab=addModule(typeInt, addr, m_modules.size()-1);
        newTab->fromJson(modules.at(i).toObject());
    }

    QJsonArray objects=Mainobject["objects"].toArray();
    for(int i=0; i<objects.size(); ++i)
    {
        QString type = objects.at(i).toObject()["type"].toString();
        int typeInt = objects::getTypeNames().indexOf(type);
        QString name = objects.at(i).toObject()["name"].toString();
        Imitation* newTab=addObject(typeInt, name, m_objects.size()-1);
        newTab->fromJson(objects.at(i).toObject());
    }

    this->blockSignals(false);

    QJsonArray systemTabs=Mainobject["systemTabs"].toArray();
    for(int i=0; i<systemTabs.size(); ++i)
    {
        QString name = systemTabs.at(i).toObject()["name"].toString();
        systemTab(name)->fromJson(systemTabs.at(i).toObject());
    }

    m_fileName = fileName;
    this->setWindowTitle(QString("ModuleImitator - ").append(fileName));

    file.close();

    updateLastProject(fileName);
    m_loadFlag=0;

    emit addrListUpdated(m_addresses);  //Обновить список адресов (так как сигналы были отключены)
    bool ADFlags[2]={0, 0};
    int n = m_contacts.size();
    if(n)
        contactRenamed(m_contacts.at(0));
    for(int i=0; i<n; ++i)  //Обновить список контактов (так как сигналы были отключены)
    {
        if(m_contacts.at(i)->pair()==nullptr)
        {
            contactChanged(m_contacts.at(i));
            ADFlags[m_contacts.at(i)->dataType()]=1;
        }
        if(!ADFlags[data_type::Analog])
            if(m_contacts.at(i)->dataType()==data_type::Analog)
            {
                contactChanged(m_contacts.at(i));
                ADFlags[data_type::Analog]=1;
            }
        if(!ADFlags[data_type::Discrete])
            if(m_contacts.at(i)->dataType()==data_type::Discrete)
            {
                contactChanged(m_contacts.at(i));
                ADFlags[data_type::Discrete]=1;
            }
    }
    if(Mainobject["connected"].toBool())
        BConnectPressed();
    m_modulesTabWidget->setCurrentIndex(Mainobject["currentModule"].toInt());
    m_objectsTabWidget->setCurrentIndex(Mainobject["currentobject"].toInt());
    m_systemTabWidget->setCurrentIndex(Mainobject["currentSys"].toInt());
    sendAllContacts();
    setSaved(true);
    QTimer::singleShot(10, this, &MainWindow::resize);
}

void MainWindow::readConfig()
{
    QFile confFile(m_config);
    confFile.open(QIODevice::ReadOnly);
    QString buf=confFile.readAll();
    QJsonDocument doc=QJsonDocument::fromJson(buf.toUtf8());
    QJsonObject Mainobject = doc.object();
    QJsonObject sizes = Mainobject["sizes"].toObject();
    QStringList keys = sizes.keys();
    QMap<QString, int> m_sizeMap;
    for(int i=0; i<keys.size(); ++i)
    {
        m_sizeMap.insert(keys.at(i), sizes[keys.at(i)].toInt());
    }
    m_sizeList.insert(s_choise, m_sizeMap.value("s_choise", 125));
    m_sizeList.insert(s_name, m_sizeMap.value("s_name", 115));
    m_sizeList.insert(s_ledD, m_sizeMap.value("s_ledD", 60));
    m_sizeList.insert(s_ledA, m_sizeMap.value("s_ledA", 35));
    m_sizeList.insert(s_visualA, m_sizeMap.value("s_visualA", 50));
    m_sizeList.insert(s_AIWidget, m_sizeMap.value("s_AIwidth", 50));
    m_sizeList.insert(s_AOwidth, m_sizeMap.value("s_choise", 1));
    m_sizeList.insert(s_DIWidget, m_sizeMap.value("s_DIwidth", 25));
    m_sizeList.insert(s_DOwidth, m_sizeMap.value("s_DOwidth", 1));
    m_sizeList.insert(s_timeout, m_sizeMap.value("s_timeout", 30));
    m_sizeList.insert(s_TAM_left, m_sizeMap.value("s_TAM_left", 50));
    m_sizeList.insert(s_TAM_midle, m_sizeMap.value("s_TAM_midle", 70));
    m_sizeList.insert(s_TAM_right, m_sizeMap.value("s_TAM_right", 50));


    m_fileName =Mainobject.value("last_project").toString();

    QJsonValue jbuf;
    if((jbuf=Mainobject.value("multithread"))!=QJsonValue::Undefined)
        m_multithread =jbuf.toBool();
    if((jbuf=Mainobject.value("sounds"))!=QJsonValue::Undefined)
        m_soundsOn = jbuf.toBool();
    if((jbuf=Mainobject.value("autoRestart"))!=QJsonValue::Undefined)
        m_autoRestart = jbuf.toBool();
    if((jbuf=Mainobject.value("nMessageLimited"))!=QJsonValue::Undefined)
        m_nMessageLimited = jbuf.toBool();
    if((jbuf=Mainobject.value("ncalcCycle"))!=QJsonValue::Undefined)
        m_calcCycle = jbuf.toInt();
    confFile.close();
}

void MainWindow::updateLastProject(QString &fileName)
{
    Static::replaceInJSON(m_config, "last_project", fileName);
}

void MainWindow::deleteAll()
{
    this->blockSignals(true);
    for(int i=m_modules.size()-1; i>-1; --i)
    {
        m_modules.at(i)->deletePressedSLOT();
    }
    for(int i=m_objects.size()-1; i>-1; --i)
    {
        m_objects.at(i)->deletePressedSLOT();
    }
    this->blockSignals(false);
    updateNameList();
}

void MainWindow::iniPort()
{
    m_COM=dynamic_cast<TabCOM*>(addSystemTab(SystemTabs::TabCOM, "COM", 0));

    if(m_multithread) //В одном потоке работает стабильнее, в двух быстрее
    {
        m_pThread = new QThread(this);
        m_port = new Port();
        connect(m_pThread, &QThread::finished, m_port, &Port::deleteLater);
        m_port->moveToThread(m_pThread);
        m_pThread->start();
    }
    else
        m_port = new Port(this);

    m_port->setBaudRate(COMTab()->baudRate());
    m_port->setDataBits(COMTab()->dataBits());
    m_port->setParity(COMTab()->parity());
    m_port->setStopBits(COMTab()->stopBits());

    COMTab()->write(QString("Started").append("\n"));
    m_problemTimer=new QTimer(this);

    connect(this, &MainWindow::connectToPort, m_port, &Port::connectToPort);
    connect(this, &MainWindow::disconnectFromPort, m_port, &Port::disconnectFromPort);
    connect(this, &MainWindow::baudRateChanged, m_port, &Port::setBaudRate);
    connect(this, &MainWindow::dataBitsChanged, m_port, &Port::setDataBits);
    connect(this, &MainWindow::parityChanged, m_port, &Port::setParity);
    connect(this, &MainWindow::stopBitsChanged, m_port, &Port::setStopBits);
    connect(this, &MainWindow::readTimeoutChanged, m_port, &Port::setReadTimeout);
    connect(this, &MainWindow::writeTimeoutChanged, m_port, &Port::setWriteTimeout);
    connect(m_port, &Port::connected, this, &MainWindow::connectedSLOT);
    connect(m_port, &Port::disconnected, this, &MainWindow::disconnectedSLOT);
    connect(m_port, &Port::newMessage, this, &MainWindow::newMessageSLOT);
    connect(this, &MainWindow::answer, m_port, &Port::write);
    m_port->setAutoRestart(m_autoRestart);
    m_systemTabWidget->setCurrentIndex(0);
}

void MainWindow::deletePort()
{
    BDisconnectPressed();
    if(m_port!=nullptr)
    {
        if(m_multithread)
            m_port->deleteLater(); //Нельзя delete объект в другом потоке, поэтому deleteLater, хоть он и медленнее
        else
            delete m_port;          //deleteLater не успевает удалить объект так, чтобы он освободил COM-порт для следующего
    }
    if(m_problemTimer!=nullptr)
    {
        m_problemTimer->deleteLater();
    }
    if(m_pThread!=nullptr)
    {
        m_pThread->quit();
        m_pThread->wait(1000);
    }
    deleteSystem(COMTab());
}

void MainWindow::writeConfig()
{
    QFile confFile(m_config);
    confFile.open(QIODevice::ReadOnly);
    QString buf=confFile.readAll();
    QJsonDocument doc=QJsonDocument::fromJson(buf.toUtf8());
    QJsonObject Mainobject = doc.object();
    QJsonObject sizes = Mainobject["sizes"].toObject();
    sizes["s_choise"]=elSize(s_choise);
    sizes["s_name"]=elSize(s_name);
    sizes["s_ledD"]=elSize(s_ledD);
    sizes["s_ledA"]=elSize(s_ledA);
    sizes["s_visualA"]=elSize(s_visualA);
    sizes["s_AIWidget"]=elSize(s_AIWidget);
    sizes["s_AOwidth"]=elSize(s_AOwidth);
    sizes["s_DIWidget"]=elSize(s_DIWidget);
    sizes["s_DOwidth"]=elSize(s_DOwidth);
    sizes["s_timeout"]=elSize(s_timeout);
    sizes["s_TAM_left"]=elSize(s_TAM_left);
    sizes["s_TAM_midle"]=elSize(s_TAM_midle);
    sizes["s_TAM_right"]=elSize(s_TAM_right);
    Mainobject.insert("sizes", sizes);
    Mainobject["multithread"]=m_multithread;
    Mainobject["calcCycle"]=m_calcCycle;
    Mainobject["sounds"] = m_soundsOn;
    Mainobject["autoRestart"] = m_autoRestart;
    Mainobject["nMessageLimited"] = m_nMessageLimited;
    QJsonDocument doc2(Mainobject);
    QString jsonString = doc2.toJson(QJsonDocument::Indented);
    confFile.close();                                       //Нормальное удаление содержимого файла не работает
    confFile.open(QIODevice::WriteOnly);
    confFile.write(jsonString.toUtf8());
    confFile.close();
}

void MainWindow::updateIndex()
{
    QFile confFile("indexes.txt");
    confFile.open(QIODevice::ReadOnly);
    QString buf=confFile.readAll();
    int i=0;
    while(buf.contains(QString::number(i)))
       ++i;
    if(i<9)
    {
        m_index=i;
        buf.append(QString::number(i));
    }
    else
    {
        m_index=0;
        buf="";
    }
    confFile.close();                                       //Нормальное удаление содержимого файла не работает
    confFile.open(QIODevice::WriteOnly);
    confFile.write(buf.toUtf8());
    confFile.close();
    m_config=QString("MI_config").append(QString::number(m_index)).append(".txt");
}

void MainWindow::deleteIndex()
{
    QFile confFile("indexes.txt");
    confFile.open(QIODevice::ReadOnly);
    QString buf=confFile.readAll();
    buf.remove(QString::number(m_index));
    confFile.close();                                       //Нормальное удаление содержимого файла не работает
    confFile.open(QIODevice::WriteOnly);
    confFile.write(buf.toUtf8());
    confFile.close();
}

void MainWindow::updateNameList()
{
    m_contactsNameList.clear();
    m_reservedContactsCount = 0;
    int n = m_contacts.size();
    for(int i=0; i<n; ++i)
    {
        QString name = m_contacts.at(i)->name();
        if(name.toLower()=="резерв")
            ++m_reservedContactsCount;
        else
            m_contactsNameList.insert(name, i);
    }
}

void MainWindow::updateCustomList()
{
    m_customBlocks.clear();
    QString path = Static::changeFolderBranch(QDir::currentPath(), "release", "Custom modules");
    QDir dir(path);
    auto files = dir.entryInfoList();
    for(int i=0; i<files.size(); ++i)
    {
        QString name = files.at(i).fileName();
        if(name.contains(".json"))
        {
            QFile file(name);
            file.open(QIODevice::ReadOnly);
            QString buf=file.readAll();
            QJsonDocument doc=QJsonDocument::fromJson(buf.toUtf8());
            QJsonObject Mainobject = doc.object();

            if(Mainobject["confirmation"]!="It's Custom module file")
            {
                int n = name.lastIndexOf("\\");
                QString shName = name.right(n);
                n=shName.lastIndexOf(".");
                shName = shName.left(n);
                m_customBlocks.append("[C]"+shName);
            }
        }
    }
}

Custom *MainWindow::loadCustomBlock(QString className, int pos, QString name)
{
    m_loadFlag=1;
    QString path = Static::changeFolderBranch(QDir::currentPath(), "release", "Custom modules");
    className = className.remove("[C]");
    path.append("\\" + className+".json");
    QFile file(path);
    file.open(QIODevice::ReadOnly);
    QString buf=file.readAll();
    QJsonDocument doc=QJsonDocument::fromJson(buf.toUtf8());
    QJsonObject Mainobject = doc.object();

    if(Mainobject["confirmation"]!="It's Custom module file")
    {
        QMessageBox msgBox(this);
        msgBox.setText(QString("Файл \"%1\" поврежден или не является файлом custom-модуля ModuleImitator").arg(path));
        msgBox.exec();
        m_loadFlag=0;
        addModule();
        return nullptr;
    }

    int readCRC=Mainobject["CRC"].toInt();
    Mainobject.remove("CRC");
    doc.setObject(Mainobject);
    auto calcCRC=Static::CRC(doc.toJson(QJsonDocument::Indented));
    if(readCRC!=Static::toU16(calcCRC[0], calcCRC[1]))
    {
        QMessageBox msgBox(this);
        msgBox.setText(QString("Файл был отредактирован или поврежден."));
        msgBox.setInformativeText(QString("Желаете продолжить загрузку?"));
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        int ret=msgBox.exec();
        if(ret==QMessageBox::Cancel)
        {
            m_loadFlag=0;
            addModule();
            return nullptr;
        }
    }

    double a = double(int(Mainobject["ver"].toDouble()*100)/100.0);
    double b = double(int(PROGRAM_VERSION*100)/100.0);
    if(a!=b)   //Отличия версий больше или равно 0.01
    {
        QMessageBox msgBox(this);
        msgBox.setText(QString("Данный модуль был создан в другой версии программы, это может привести к ошибкам"));
        msgBox.exec();
    }

    this->blockSignals(true);   //Для оптимизации, иначе время загрузки экспоненциально растет от количества модулей

    auto customJSN = Mainobject["custom"].toObject();
    QString oldName = customJSN["name"].toString();
    customJSN["newName"]=name;
    if(pos<0)
        pos = m_modules.size()-1;
    auto custom = dynamic_cast<Custom*>(addModule(Modules::Custom, 0, pos));
    custom->fromJson(customJSN);

    QJsonObject object;
    QJsonArray objects=Mainobject["objects"].toArray();
    QString newName=custom->name();
    for(int i=0; i<objects.size(); ++i)
    {
        QString type = objects.at(i).toObject()["type"].toString();
        int typeInt = objects::getTypeNames().indexOf(type);
        QString name = objects.at(i).toObject()["name"].toString();
        name = Static::writeTag(name, newName, '{', '}');
        auto newTab=addObject(typeInt, name, m_objects.size()-1);

        object=objects.at(i).toObject();
        object["name"]=name;

        QString nameBuffer;
        QString newPair;
        QJsonObject cont;
        //Заранее переименовываем контакты, так как потом это будет выполнятся вне загрузки и вызовет зависание
        QStringList formats={"AnalogInputs", "AnalogOutputs", "DiscreteInputs", "DisctreteOutputs"};
        for(int j=0; j<4; ++j)
        {
            QJsonArray newArr;
            QJsonArray oldArr=object[formats.at(j)].toArray();
            int n = oldArr.size();
            for(int i=0; i<n; ++i)
            {
                cont=oldArr.at(i).toObject();
                newPair=cont["pair"].toString();
                if(newPair.contains('{'+oldName+'}'))
                {
                    newPair=Static::writeTag(cont["pair"].toString(), newName, '{', '}', ':');
                }
                else
                {
                if(newPair.contains('<'+oldName+'>'))
                    newPair=Static::writeTag(newPair, newName, '<', '>', ':');
                }
                nameBuffer=Static::writeTag(cont["name"].toString(), newName, '{', '}', ':');
                cont["name"]=nameBuffer;
                cont["pair"]=newPair;
                newArr.append(cont);
            }
            object[formats.at(j)]=newArr;
        }

        newTab->fromJson(object);
        newTab->setParentCustom(custom);
    }

    file.close();
    this->blockSignals(false);
    m_loadFlag=0;
    sendAllContacts();
    bool ADFlags[2]={0, 0};
    int n = m_contacts.size();
    if(n)
        contactRenamed(m_contacts.at(0));
    for(int i=0; i<n; ++i)  //Обновить список контактов (так как сигналы были отключены)
    {
        if(m_contacts.at(i)->pair()==nullptr)
        {
            contactChanged(m_contacts.at(i));
            ADFlags[m_contacts.at(i)->dataType()]=1;
        }
        if(!ADFlags[data_type::Analog])
            if(m_contacts.at(i)->dataType()==data_type::Analog)
            {
                contactChanged(m_contacts.at(i));
                ADFlags[data_type::Analog]=1;
            }
        if(!ADFlags[data_type::Discrete])
            if(m_contacts.at(i)->dataType()==data_type::Discrete)
            {
                contactChanged(m_contacts.at(i));
                ADFlags[data_type::Discrete]=1;
            }
    }
    sendAllContacts();
    QTimer::singleShot(10, this, &MainWindow::resize);
    return custom;
}

void MainWindow::sendAllContacts()
{
    emit DIContactsUpdated(m_lastDIs);
    emit DOContactsUpdated(m_lastDOs);
    emit AIContactsUpdated(m_lastAIs);
    emit AOContactsUpdated(m_lastAOs);
}

const QStringList &MainWindow::customBlocks() const
{
    return m_customBlocks;
}

bool MainWindow::saved() const
{
    return m_saved;
}

void MainWindow::setSaved(bool newSaved)
{
    m_saved = newSaved;
}

bool MainWindow::autoRestart() const
{
    return m_autoRestart;
}

void MainWindow::setAutoRestart(bool newAutoRestart)
{
    m_autoRestart = newAutoRestart;
    m_port->setAutoRestart(m_autoRestart);
}

bool MainWindow::nMessageLimited() const
{
    return m_nMessageLimited;
}

void MainWindow::setNMessageLimited(bool newNMessageLimited)
{
    m_nMessageLimited = newNMessageLimited;
    COMTab()->setIsLimited(m_nMessageLimited);
}

bool MainWindow::sounds() const
{
    return m_soundsOn;
}

void MainWindow::setSounds(bool newSoundsOn)
{
    m_soundsOn = newSoundsOn;
    gateStartWorking(0);
}

void MainWindow::resize()
{
    this->setMinimumWidth(qMax(ui->horizontalLayout->minimumSize().width()+50, this->minimumSize().width()));
}

void MainWindow::saveCustom(Custom *module)
{
    QDialog dialog(this);
    dialog.setWindowTitle("Выберите объекты, входящие в состав модуля:");
    dialog.setWindowFlags(dialog.windowFlags()&~Qt::WindowContextHelpButtonHint);
    QDialogButtonBox buttonBox(QDialogButtonBox::StandardButton::Ok | QDialogButtonBox::Cancel, &dialog);
    connect(&buttonBox, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
    connect(&buttonBox, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);
    QVBoxLayout *M = new QVBoxLayout;
    dialog.setLayout(M);

    QList<QCheckBox *> checkList;
    int size=m_objects.size()-1;
    QHBoxLayout *H;

    auto m_scrollArea = new QScrollArea(&dialog);
    QWidget *w = new QWidget(m_scrollArea);
    m_scrollArea->setWidget(w);
    auto m_areaLayout = new QVBoxLayout(w);
    M->addWidget(m_scrollArea);
    m_scrollArea->setBackgroundRole(QPalette::Light);
    m_scrollArea->setMinimumHeight(300);
    m_scrollArea->setMaximumHeight(900);
    m_scrollArea->setWidgetResizable(true);

    for(int i=0; i<size; ++i)
    {
        H=new QHBoxLayout;
        auto label = new QLabel(m_objects.at(i)->name(), m_scrollArea);
        auto checkBox = new QCheckBox(m_scrollArea);
        H->addWidget(label);
        H->addStretch(10);
        H->addWidget(checkBox);
        checkList.append(checkBox);
        //M->addLayout(H);
        m_areaLayout->addLayout(H);
        if(m_objects.at(i)->parentCustom()==module)
            checkBox->setChecked(true);
    }
    m_scrollArea->resize(m_scrollArea->width(), 900);

    M->addWidget(&buttonBox);
    dialog.setMinimumSize(M->minimumSize());
    dialog.setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

    if(dialog.exec())
    {
        QString path = Static::changeFolderBranch(QDir::currentPath(), "release", "Custom modules");
        if(!QDir(path).exists())
            QDir().mkdir(path);
        QString fileName = QFileDialog::getSaveFileName(this, tr("Выберите файл сохранения"), path, tr("Json files (*.json)"));
        if(fileName!="")
        {
            QJsonObject Mainobject;
            Mainobject["ver"]=PROGRAM_VERSION;
            Mainobject["confirmation"]="It's Custom module file";
            for(int i=0; i<size; ++i)
            {
                if(checkList.at(i)->isChecked())
                    m_objects.at(i)->setParentCustom(module);
            }

            auto custom = module->toJson();
            Mainobject.insert("custom", custom);

            QJsonArray objects;
            for(int i=0; i<size; ++i)
            {
                if(checkList.at(i)->isChecked())
                    objects.append(m_objects.at(i)->toJson());
            }
            Mainobject.insert("objects", objects);

            QJsonDocument doc(Mainobject);

            auto calcCRC=Static::CRC(doc.toJson(QJsonDocument::Indented));
            Mainobject["CRC"]=Static::toU16(calcCRC[0], calcCRC[1]);
            doc.setObject(Mainobject);

            QFile file(fileName);
            file.open(QIODevice::WriteOnly);
            file.write(doc.toJson(QJsonDocument::Indented));
            file.close();
            QMessageBox msgBox(this);
            msgBox.setText("Модуль успешно сохранен");
            QTimer::singleShot(1200, &msgBox, &QMessageBox::close);
            msgBox.exec();
            updateCustomList();
        }
    }
}



int MainWindow::cycleIndex() const
{
    return m_cycleIndex;
}

void MainWindow::setCalcCycle(int newCalcCycle)
{
    if(newCalcCycle>2)
        m_calcCycle = newCalcCycle;
}

bool MainWindow::multithread() const
{
    return m_multithread;
}

void MainWindow::setMultithread(bool newMultithread)
{
    if(m_multithread!=newMultithread)
    {   //Пересоздаем порт с новой конфигурацией
        QJsonObject jsn = COMTab()->toJson();
        bool buf = m_isConnected;
        deletePort();
        m_multithread = newMultithread;
        iniPort();
        COMTab()->fromJson(jsn);
        if(buf)
            BConnectPressed();
    }
}

bool MainWindow::isSaveConfig() const
{
    return m_saveConfig;
}

void MainWindow::setSaveConfig(bool newSaveConf)
{
    m_saveConfig = newSaveConf;
}

bool MainWindow::loadFlag() const
{
    return m_loadFlag;
}

const QVector<int> &MainWindow::sizeList() const
{
    return m_sizeList;
}

const QVector<Contact *> &MainWindow::contacts()
{
    return m_contacts;
}

const QList<Module *> &MainWindow::modules() const
{
    return m_modules;
}

int MainWindow::calcCycle() const
{
    return m_calcCycle;
}

bool MainWindow::isConnected() const
{
    return m_isConnected;
}
